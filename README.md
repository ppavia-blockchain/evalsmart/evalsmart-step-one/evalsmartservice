<link rel="stylesheet" type="text/css" media="all" href="README.css" />

# Evalsmart Service REST

## Presentation

This project is a REST Service implemented with Spring Boot.
The services provide eae resources with json representation format. 

## Installation

Need to install mongodb and import form struct and user data.
We can run the postman request located in /src/main/resources/request to do this.

## REST Architecture description

"REST's client–server separation of concerns simplifies component implementation, reduces the complexity of connector semantics, improves the effectiveness of performance tuning, and increases the scalability of pure server components. Layered system constraints allow intermediaries—proxies, gateways, and firewalls—to be introduced at various points in the communication without changing the interfaces between components, thus allowing them to assist in communication translation or improve performance via large-scale, shared caching. REST enables intermediate processing by constraining messages to be self-descriptive: interaction is stateless between requests, standard methods and media types are used to indicate semantics and exchange information, and responses explicitly indicate cacheability."

_Roy Fielding, Architectural Styles and the Design of Network-based Software Architectures_

### Architectural properties
* Performance in component interactions
* Scalability
* Simplicity of a uniform interface
* Modifiability of components
* Visibility of communication between components by service agents
* Portability of components by moving program code with the data
* Reliability in the resistance to failure at the system level in the presence of failures within components, connectors, or data

### Architectural constraints
* Client-server architecture
* Statelessness
* Cacheability
* Layered system
* Code on demand (optional) or client-side scripting
* Uniform interface
	* Resource identification in requests
	* Resource manipulation through representations
	* Self-descriptive messages
	* Hypermedia as the engine of application state (HATEOAS)
	
### HTTP methods
GET, POST, PUT, PATCH, DELETE.

* GET is safe, meaning that the method does not change the state of the resource.
* GET, PUT, DELETE are idempotent, meaning that applying them multiple times to a resource result in the same state change of the resource as applying them once, though the response might differ.
* GET and POST are cacheable.