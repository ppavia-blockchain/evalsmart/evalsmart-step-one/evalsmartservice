alias		-> evalsmart
pwd			-> evalsmart

--------------------------------
Enabling HTTPS in Spring Boot

Spring Boot provide a set of a declarative server.ssl.* properties

exemple :
# The format used for the keystore. It could be set to JKS in case it is a JKS file
server.ssl.key-store-type=PKCS12
# The path to the keystore containing the certificate
server.ssl.key-store=classpath:keystore/evalsmart.p12
# The password used to generate the certificate
server.ssl.key-store-password=evalsmart
# The alias mapped to the certificate
server.ssl.key-alias=evalsmart