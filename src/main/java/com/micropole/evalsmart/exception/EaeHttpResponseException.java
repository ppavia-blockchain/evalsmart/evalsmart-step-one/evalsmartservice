package com.micropole.evalsmart.exception;

import org.springframework.http.HttpStatus;

public class EaeHttpResponseException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2140908949234313677L;
	private HttpStatus httpCodeStatus;
	
	public EaeHttpResponseException(String message, HttpStatus httpCodeStatus) {
		super(message);
		this.httpCodeStatus	= httpCodeStatus;
	}

	public HttpStatus getHttpCodeStatus() {
		return httpCodeStatus;
	}

	public void setHttpCodeStatus(HttpStatus httpCodeStatus) {
		this.httpCodeStatus = httpCodeStatus;
	}
}
