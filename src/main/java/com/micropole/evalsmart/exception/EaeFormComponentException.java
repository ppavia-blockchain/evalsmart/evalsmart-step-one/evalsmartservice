package com.micropole.evalsmart.exception;

public class EaeFormComponentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5942351606541315357L;

	public EaeFormComponentException(String errorMessage) {
        super(errorMessage);
    }
	
	public EaeFormComponentException(String errorMessage, Throwable err) {
	    super(errorMessage, err);
	}
}
