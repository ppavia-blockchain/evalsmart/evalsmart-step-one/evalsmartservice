package com.micropole.evalsmart.exception.handler;

import java.util.Date;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.micropole.evalsmart.exception.EaeHttpResponseException;
import com.micropole.evalsmart.model.EaeHttpResponseBean;

@ControllerAdvice
@RestController
public class EaeHttpResponseExceptionHandler<T> extends ResponseEntityExceptionHandler {
	@ExceptionHandler(EaeHttpResponseException.class)

	public final ResponseEntity<EaeHttpResponseBean<T>> handleNotFoundException(EaeHttpResponseException ex, WebRequest request) {
		EaeHttpResponseBean<T> exceptionResponse = new EaeHttpResponseBean<T>(
				new Date(),
				ex.getMessage(),
				request.getDescription(false),
				ex.getHttpCodeStatus().getReasonPhrase()
				);
		ResponseEntity<EaeHttpResponseBean<T>> responseEntity	= new ResponseEntity<EaeHttpResponseBean<T>>(exceptionResponse, ex.getHttpCodeStatus());
		return responseEntity;

	}
}
