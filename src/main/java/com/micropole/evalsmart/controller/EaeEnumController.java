package com.micropole.evalsmart.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.micropole.evalsmart.component.form.service.EaeEnumService;
import com.micropole.evalsmart.component.form.service.EaeUserService;
import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.exception.EaeHttpResponseException;
import com.micropole.evalsmart.model.EaeHttpResponseBean;
import com.micropole.evalsmart.model.component.form.bean.EaeStatusFormBean;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/eaeenum")
public class EaeEnumController {
	private static final Logger logger = LoggerFactory.getLogger(EaeEnumController.class);

	@Autowired
	private EaeUserService eaeFormUserService;
	@Autowired
	private EaeEnumService eaeEnumService;

	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/statusform/{statusCode}",
			//consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeStatusFormBean>> getStatusForm (@PathVariable String statusCode) {
		
		String responseMsg		= "user is not authorized";
		HttpStatus httpStatus	= HttpStatus.UNAUTHORIZED;
		EaeStatusFormBean statusForm	= null;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					statusForm				= eaeEnumService.getStatusFormByCode(statusCode);
					if ( statusForm == null ) {
						responseMsg			= "no status found with code [" + statusCode + "]";
						httpStatus 			= HttpStatus.OK;
					}
					else {
						responseMsg			= "get status form succeed";
						httpStatus 			= HttpStatus.OK;					
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getStatusForm" + "]" + "unable to retrieve status form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve status form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeStatusFormBean> httpResponse = new EaeHttpResponseBean<EaeStatusFormBean>(
				new Date(),
				responseMsg,
				"/statusform/" + statusCode,
				httpStatus.getReasonPhrase(),
				statusForm);			
		return new ResponseEntity<EaeHttpResponseBean<EaeStatusFormBean>>(httpResponse, httpStatus);
	}

	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/all-statusform",
			//consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeStatusFormBean>>> getAllStatusForm () {
		
		String responseMsg		= "user is not authorized";
		HttpStatus httpStatus	= HttpStatus.UNAUTHORIZED;
		List<EaeStatusFormBean> allStatusForm	= null;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					allStatusForm				= eaeEnumService.getAllStatusForm();
					if ( allStatusForm == null ) {
						responseMsg			= "no status found with code [" + allStatusForm + "]";
						httpStatus 			= HttpStatus.OK;
					}
					else {
						responseMsg			= "get status form succeed";
						httpStatus 			= HttpStatus.OK;					
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getAllStatusForm" + "]" + "unable to retrieve all status form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve all status form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeStatusFormBean>> httpResponse = new EaeHttpResponseBean<List<EaeStatusFormBean>>(
				new Date(),
				responseMsg,
				"/statusform",
				httpStatus.getReasonPhrase(),
				allStatusForm);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeStatusFormBean>>>(httpResponse, httpStatus);
	}
}
