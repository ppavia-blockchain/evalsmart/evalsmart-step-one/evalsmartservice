package com.micropole.evalsmart.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.micropole.evalsmart.component.form.service.EaeFormStructService;
import com.micropole.evalsmart.component.form.service.EaeUserService;
import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.exception.EaeHttpResponseException;
import com.micropole.evalsmart.model.EaeHttpResponseBean;
import com.micropole.evalsmart.model.EaeStatusFormEnum;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructComponentsBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructRequestMapping;
import com.micropole.evalsmart.utils.EvalSmartUtils;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/eaeformstruct")
public class EaeFormStructController {
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructController.class);

	@Autowired
	private EaeUserService eaeFormUserService;

	@Autowired
	private EaeFormStructService eaeFormStructService;
	
	
	/** RETRIEVE FORM STRUCT **/
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/{idForm}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>> getFormStruct ( @PathVariable String idForm ) {
		EaeFormStructBean eaeForm		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeForm = eaeFormStructService.getFormStructById(idForm);
					if ( eaeForm == null ) {
						responseMsg	="no form with id [" + idForm + "]";
					}
					else {
						responseMsg	="form [" + idForm + "] recovery was successful";					
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getForm" + "]" + "unable to retrieve form with id [" + idForm + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve form with id [" + idForm + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeFormStructBean> httpResponse = new EaeHttpResponseBean<EaeFormStructBean>(
				new Date(),
				responseMsg,
				"/eaeformstruct/forms/" + idForm,
				httpStatus.getReasonPhrase(),
				eaeForm);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/lastactive/since/{dtSince}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>> getLastActiveFormStruct ( @PathVariable String dtSince ) {
		EaeFormStructBean eaeForm		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					Date since	= EvalSmartUtils.convertDateStringToDate(dtSince);
					List<String> activeStatus	= new ArrayList<String>();
					activeStatus.add(EaeStatusFormEnum.getCode("active"));
					eaeForm = eaeFormStructService.getLastActiceFormStruct(activeStatus, since);
					if ( eaeForm == null ) {
						responseMsg	="no last active form struct";
					}
					else {
						responseMsg	="last active form [" + eaeForm.getIdForm() + "] recovery was successful";
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getForm" + "]" + "unable to retrieve last active form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve last active form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeFormStructBean> httpResponse = new EaeHttpResponseBean<EaeFormStructBean>(
				new Date(),
				responseMsg,
				"/eaeformstruct/forms/" + eaeForm.getIdForm(),
				httpStatus.getReasonPhrase(),
				eaeForm);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>>(httpResponse, httpStatus);
	}

	
	/** CREATE FORM STRUCT **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/addform",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>> addFormStruct ( @RequestBody EaeFormStructRequestMapping eaeFormStructRequestMapping ) {
		EaeFormStructBean eaeForm 	= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			EaeFormStructComponentsBean eaeComponents;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					// check and map form data
					eaeComponents = eaeFormStructService.mapComponentsFormStruct(eaeFormStructRequestMapping);
					if ( eaeComponents != null ) {
						// save form data
						eaeForm = eaeFormStructService.addNewFormComponents(eaeComponents);
						responseMsg	="the form with id : [" + eaeForm.getIdForm() + "] has been created";
						httpStatus	= HttpStatus.OK;
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "addForm" + "]" + "unable to create form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to create form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeFormStructBean> httpResponse = new EaeHttpResponseBean<EaeFormStructBean>(
				new Date(),
				responseMsg,
				"/eaeformstruct/forms/" + eaeForm.getIdForm(),
				httpStatus.getReasonPhrase(),
				eaeForm);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>>(httpResponse, httpStatus);
	}
	
	/** UPDATE FORM STRUCT **/
	
	@RequestMapping(
			method = {RequestMethod.PUT},
			path = "/forms/{idForm}",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>> updateFormStruct (
			@PathVariable String idForm,
			@RequestBody EaeFormStructRequestMapping eaeFormStructRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					EaeFormStructBean eaeForm	= eaeFormStructService.mapDataFormStruct(eaeFormStructRequestMapping);
					if ( eaeForm == null ) {
						logger.error("[" + "updateForm" + "]" + "unable to map request data parameters\n");
						throw new EaeHttpResponseException("unable to map request data parameters", HttpStatus.INTERNAL_SERVER_ERROR );
					}
					eaeFormStructService.modifyDataFormStruct(eaeForm);
					responseMsg			= "update form succeed";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateForm" + "]" + "unable to perform this request\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform this request", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructBean> httpResponse = new EaeHttpResponseBean<EaeFormStructBean>(
				new Date(),
				responseMsg,
				"/eaeformstruct/forms/" + idForm,
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/close",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>> updateDtClosedFormStruct (
			@PathVariable String idForm,
			@RequestBody EaeFormStructRequestMapping eaeFormStructRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructService.closeFormStruct(idForm, eaeFormStructRequestMapping.getDtClosed());
					responseMsg			= "form closed successfully";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateDtClosedFormStruct" + "]" + "unable to close form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to close form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructBean> httpResponse = new EaeHttpResponseBean<EaeFormStructBean>(
				new Date(),
				responseMsg,
				"/eaeformstruct/forms/" + idForm + "/close",
				httpStatus.getReasonPhrase(),
				null);	
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/label",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>> updateLabelFormStruct (
			@PathVariable String idForm, 
			@RequestBody EaeFormStructRequestMapping eaeFormStructRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructService.modifyLabelFormStruct(idForm, eaeFormStructRequestMapping.getLabel());
					responseMsg			= "form label modification is successful";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateLabelFormStruct" + "]" + "unable to modify label form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to modify label form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructBean> httpResponse = new EaeHttpResponseBean<EaeFormStructBean>(
				new Date(),
				responseMsg,
				"/eaeformstruct/forms/" + idForm + "/label",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/historize",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>> updateDtHistorizedFormStruct (
			@PathVariable String idForm,
			@RequestBody EaeFormStructRequestMapping eaeFormStructRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructService.historizeFormStruct(idForm, eaeFormStructRequestMapping.getDtHistorized());
					responseMsg			= "form historized successfully";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateDtClosedForm" + "]" + "unable to historize form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to historize form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructBean> httpResponse = new EaeHttpResponseBean<EaeFormStructBean>(
				new Date(),
				responseMsg,
				"/eaeformstruct/forms/" + idForm + "/historize",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/status",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>> updateStatusFormStruct (
			@PathVariable String idForm,
			@RequestBody EaeFormStructRequestMapping eaeFormStructRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructService.modifyStatusFormStruct(idForm, eaeFormStructRequestMapping.getStatus());
					responseMsg			= "update status form succeed";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateStatusFormStruct" + "]" + "unable to perform update status form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update status form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructBean> httpResponse = new EaeHttpResponseBean<EaeFormStructBean>(
				new Date(),
				responseMsg,
				"/eaeformstruct/forms/" + idForm + "/status/" + eaeFormStructRequestMapping.getStatus(),
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructBean>>(httpResponse, httpStatus);
	}
}
