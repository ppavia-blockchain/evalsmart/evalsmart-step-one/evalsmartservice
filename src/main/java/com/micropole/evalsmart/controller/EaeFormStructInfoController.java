package com.micropole.evalsmart.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.micropole.evalsmart.component.form.service.EaeFormStructInfoService;
import com.micropole.evalsmart.component.form.service.EaeUserService;
import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.exception.EaeHttpResponseException;
import com.micropole.evalsmart.model.EaeHttpResponseBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructInfoBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructInfoRequestMapping;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/eaeformstructinfo")
public class EaeFormStructInfoController {
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructInfoController.class);
	
	@Autowired
	private EaeUserService eaeFormUserService;
	@Autowired
	private EaeFormStructInfoService eaeFormStructInfoService;
	
	/** RETRIEVE INFO **/
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/{idForm}/infos",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructInfoBean>>> getInfosByForm ( @PathVariable String idForm) {
		List<EaeFormStructInfoBean> eaeInfos		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeInfos = eaeFormStructInfoService.getAllInfosByForm(idForm);
					if ( eaeInfos == null ) {
						responseMsg	="no info found for the form with id [" + idForm + "]";
					}
					else {
						responseMsg	=eaeInfos.size() + " info(s) found for the form with id [" + idForm + "]";					
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getUserFormByIds" + "]" + "unable to retrieve info for the form with id [" + idForm + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve info for the form with id [" + idForm + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructInfoBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructInfoBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructinfo/forms/" + idForm + "/infos",
				httpStatus.getReasonPhrase(),
				eaeInfos);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructInfoBean>>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/domains/{idDomain}/infos",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructInfoBean>>> getInfosByDomain ( @PathVariable String idDomain) {
		List<EaeFormStructInfoBean> eaeInfos		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeInfos = eaeFormStructInfoService.getAllInfosByDomain(idDomain);
					if ( eaeInfos == null ) {
						responseMsg	="no info found for the domain with id [" + idDomain + "]";
					}
					else {
						responseMsg	= eaeInfos.size() + " info(s) found for the domain with id [" + idDomain + "]";					
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getUserFormByIds" + "]" + "unable to retrieve info for the domain with id [" + idDomain + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve info for the domain with id [" + idDomain + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructInfoBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructInfoBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructinfo/domains/" + idDomain + "/infos",
				httpStatus.getReasonPhrase(),
				eaeInfos);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructInfoBean>>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/infos/{idInfo}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructInfoBean>> getInfoById ( @PathVariable String idInfo) {
		EaeFormStructInfoBean eaeInfo		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeInfo = eaeFormStructInfoService.getInfoById(idInfo);
					if ( eaeInfo == null ) {
						responseMsg	="no info found with id [" + idInfo + "]";
					}
					else {
						responseMsg	=" info with id [" + idInfo + "] recovered successfully";					
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getUserFormByIds" + "]" + "unable to retrieve info with id [" + idInfo + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve info with id [" + idInfo + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeFormStructInfoBean> httpResponse = new EaeHttpResponseBean<EaeFormStructInfoBean>(
				new Date(),
				responseMsg,
				"/eaeformstructinfo/infos/" + idInfo,
				httpStatus.getReasonPhrase(),
				eaeInfo);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructInfoBean>>(httpResponse, httpStatus);
	}
	
	/** CREATE INFO **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/domains/{idDomain}/addInfos",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructInfoBean>>> addNewInfos (
			 @PathVariable String idForm,
			 @PathVariable String idDomain,
			@RequestBody EaeFormStructInfoRequestMapping[] eaeFormStructInfosRequestMapping ) {
		List<EaeFormStructInfoBean> eaeInfos 	= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					// check and map infos data
					eaeInfos = eaeFormStructInfoService.mapAllDataInfos(idForm, idDomain, eaeFormStructInfosRequestMapping);
					if ( eaeInfos != null ) {
						// save infos data
						eaeInfos = eaeFormStructInfoService.addNewFormStructInfos(eaeInfos);
						responseMsg	= eaeInfos.size() + " info(s) have been created for the domain with id [" + idDomain + "]";
						httpStatus	= HttpStatus.OK;
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "addUserForm" + "]" + "unable to create infos for the form [" + idForm + "] and the domain [" + idDomain + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to create infos for the form [" + idForm + "] and the domain [" + idDomain + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructInfoBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructInfoBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructinfo/forms/" + idForm + "/domains/" + idDomain + "/infos",
				httpStatus.getReasonPhrase(),
				eaeInfos);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructInfoBean>>>(httpResponse, httpStatus);
	}
	
	/** UPDATE INFO **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/infos/{idInfo}/content",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructInfoBean>> changeInfoContent (
			@PathVariable String idInfo,
			@RequestBody EaeFormStructInfoRequestMapping eaeFormStructInfoRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructInfoService.changeContentInfo(idInfo, eaeFormStructInfoRequestMapping.getContent());
					responseMsg			= "change info [" + idInfo + "] content successfull";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "changeInfoContent" + "]" + "unable to change content of the info [" + idInfo + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to change content of the info [" + idInfo + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructInfoBean> httpResponse = new EaeHttpResponseBean<EaeFormStructInfoBean>(
				new Date(),
				responseMsg,
				"/eaeformstructinfo/infos/" + idInfo,
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructInfoBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/infos/{idInfo}/order",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructInfoBean>> changeInfoOrder (
			@PathVariable String idInfo,
			@RequestBody EaeFormStructInfoRequestMapping eaeFormStructInfoRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructInfoService.changeOrderInfo(idInfo, eaeFormStructInfoRequestMapping.getOrder());
					responseMsg			= "change info [" + idInfo + "] order successfull";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "changeInfoContent" + "]" + "unable to change order of the info [" + idInfo + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to change order of the info [" + idInfo + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructInfoBean> httpResponse = new EaeHttpResponseBean<EaeFormStructInfoBean>(
				new Date(),
				responseMsg,
				"/eaeformstructinfo/infos/" + idInfo,
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructInfoBean>>(httpResponse, httpStatus);
	}
}
