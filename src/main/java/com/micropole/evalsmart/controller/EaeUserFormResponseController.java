package com.micropole.evalsmart.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.micropole.evalsmart.component.form.service.EaeUserFormResponseService;
import com.micropole.evalsmart.component.form.service.EaeUserService;
import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.exception.EaeHttpResponseException;
import com.micropole.evalsmart.model.EaeHttpResponseBean;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormResponseBean;
import com.micropole.evalsmart.model.requestmapping.EaeUserResponseRequestMapping;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/eaeuserformresponse")
public class EaeUserFormResponseController {
	private static final Logger logger = LoggerFactory.getLogger(EaeUserFormResponseController.class);
	@Autowired
	private EaeUserService eaeFormUserService;
	@Autowired
	private EaeUserFormResponseService eaeUserFormResponseService;
	
	/** RETRIEVE USER FORM RESPONSE **/
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/responses/{idForm}/{idUser}/{idResponse}/{idOwner}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormResponseBean>> getUserFormResponseByIds ( 
			@PathVariable String idResponse,
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@PathVariable String idOwner
			) {
		EaeUserFormResponseBean eaeUserResponse		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idResponse = ").append(idResponse).append(", ");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idUser = ").append(idUser).append(", ");
		msg.append("idOwner = ").append(idOwner);
		msg.append("]");
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserResponse = eaeUserFormResponseService.getUserResponseByIds(idResponse, idForm, idUser, idOwner);
					if ( eaeUserResponse == null ) {
						responseMsg	="no user response with id " + msg;
					}
					else {
						responseMsg	="user response " + msg + " recovery was successful";					
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getUserFormResponseByIds" + "]" + "unable to retrieve user form response " + msg + "\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve user form response " + msg, HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserFormResponseBean> httpResponse = new EaeHttpResponseBean<EaeUserFormResponseBean>(
				new Date(),
				responseMsg,
				"/responses/" + idForm + idUser + idResponse + idOwner,
				httpStatus.getReasonPhrase(),
				eaeUserResponse);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormResponseBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/{idForm}/users/{idUser}/responses",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeUserFormResponseBean>>> getUserFormResponseByForm (
			@PathVariable String idForm, 
			@PathVariable String idUser
			) {
		List<EaeUserFormResponseBean> eaeUserResponses		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idUser = ").append(idUser);
		msg.append("]");
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserResponses = eaeUserFormResponseService.getUserResponsesByForm(idForm, idUser);
					if ( eaeUserResponses == null ) {
						responseMsg	="no user response with id " + msg;
					}
					else {
						responseMsg	= eaeUserResponses.size() + " user responses found with ids " + msg;
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getUserFormResponseByIds" + "]" + "unable to retrieve user form responses for the form " + msg + "\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve user form responses for the form " + msg, HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeUserFormResponseBean>> httpResponse = new EaeHttpResponseBean<List<EaeUserFormResponseBean>>(
				new Date(),
				responseMsg,
				"/forms/" + idForm + "/users/" + idUser,
				httpStatus.getReasonPhrase(),
				eaeUserResponses);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeUserFormResponseBean>>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/{idForm}/users/{idUser}/responses/ownedBy/{idOwner}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeUserFormResponseBean>>> getUserFormResponseOwnedByUser (
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@PathVariable String idOwner
			) {
		List<EaeUserFormResponseBean> eaeUserResponses		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idUser = ").append(idUser).append(", ");
		msg.append("idOwner = ").append(idOwner);
		msg.append("]");
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserResponses = eaeUserFormResponseService.getUserResponsesOwnedByUser(idForm, idUser, idOwner);
					if ( eaeUserResponses == null ) {
						responseMsg	="no user response with id " + msg;
					}
					else {
						responseMsg	= eaeUserResponses.size() + " user responses found for owner " + msg;					
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getUserFormResponseByIds" + "]" + "unable to retrieve user form responses for the form " + msg + "\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve user form responses for the form " + msg, HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeUserFormResponseBean>> httpResponse = new EaeHttpResponseBean<List<EaeUserFormResponseBean>>(
				new Date(),
				responseMsg,
				"/forms/" + idForm + "/users/" + idUser + "/owners/" + idOwner,
				httpStatus.getReasonPhrase(),
				eaeUserResponses);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeUserFormResponseBean>>>(httpResponse, httpStatus);
	}

	/** CREATE USER FORM RESPONSE **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/responses/adduserresponses",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeUserFormResponseBean>>> addUserFormResponses (
			@RequestBody EaeUserResponseRequestMapping[] eaeUserResponsesRequestMapping 
			) {
		List<EaeUserFormResponseBean> eaeUserResponses		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("]");
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserResponses		= eaeUserFormResponseService.mapDataUserResponses(eaeUserResponsesRequestMapping);
					eaeUserResponses = eaeUserFormResponseService.addUserResponses(eaeUserResponses);
					responseMsg			= eaeUserResponses.size() + " user form responses have been created";
					httpStatus 			= HttpStatus.OK;
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "modifyContentUserFormResponse" + "]" + "unable to create user form responses " + msg + "\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to create user form responses " + msg, HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeUserFormResponseBean>> httpResponse = new EaeHttpResponseBean<List<EaeUserFormResponseBean>>(
				new Date(),
				responseMsg,
				"/responses/adduserresponses",
				httpStatus.getReasonPhrase(),
				eaeUserResponses);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeUserFormResponseBean>>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/responses/adduserresponse",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormResponseBean>> addUserFormResponse (
			@RequestBody EaeUserResponseRequestMapping eaeUserResponseRequestMapping 
			) {
		EaeUserFormResponseBean eaeUserResponse		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idResponse = ").append(eaeUserResponseRequestMapping.getIdResponse()).append(", ");
		msg.append("idForm = ").append(eaeUserResponseRequestMapping.getIdForm()).append(", ");
		msg.append("idUser = ").append(eaeUserResponseRequestMapping.getIdUser()).append(", ");
		msg.append("idOwner = ").append(eaeUserResponseRequestMapping.getIdOwner());
		msg.append("]");
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserResponse		= eaeUserFormResponseService.mapDataUserResponse(eaeUserResponseRequestMapping);
					eaeUserResponse 	= eaeUserFormResponseService.addNewUserResponse(
							eaeUserResponse.getId(),
							eaeUserResponse.getIdForm(),
							eaeUserResponse.getIdUser(),
							eaeUserResponse.getIdDomain(),
							eaeUserResponse.getIdQuery(),
							eaeUserResponse.getIdResponse(),
							eaeUserResponse.getIdOwner(),
							eaeUserResponse.getContent(),
							eaeUserResponse.getDtCompletion());
					responseMsg			= "the user form response with id : " + msg + " has been created";
					httpStatus 			= HttpStatus.OK;
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "modifyContentUserFormResponse" + "]" + "unable to create user form response " + msg + "\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to create user form response " + msg, HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserFormResponseBean> httpResponse = new EaeHttpResponseBean<EaeUserFormResponseBean>(
				new Date(),
				responseMsg,
				"/forms/" + eaeUserResponse.getIdForm() 
				+ "/users/" + eaeUserResponse.getIdUser() 
				+ "/responses/" + eaeUserResponse.getIdResponse() 
				+ "/owners/" + eaeUserResponse.getIdOwner(),
				httpStatus.getReasonPhrase(),
				eaeUserResponse);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormResponseBean>>(httpResponse, httpStatus);
	}
	
	/** DELETE **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/responses/removeuserresponses",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeUserFormResponseBean>>> deleteUserFormResponses (
			@RequestBody EaeUserResponseRequestMapping[] eaeUserResponsesRequestMapping 
			) {
		List<EaeUserFormResponseBean> eaeUserResponses		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("]");
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserResponses		= eaeUserFormResponseService.mapDataUserResponses(eaeUserResponsesRequestMapping);
					this.eaeUserFormResponseService.removeUserFormResponses(eaeUserResponses);
					responseMsg			= eaeUserResponses.size() + " user form responses have been removed";
					httpStatus 			= HttpStatus.OK;
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "modifyContentUserFormResponse" + "]" + "unable to remove user form responses " + msg + "\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to remove user form responses " + msg, HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeUserFormResponseBean>> httpResponse = new EaeHttpResponseBean<List<EaeUserFormResponseBean>>(
				new Date(),
				responseMsg,
				"/responses/removeuserresponses",
				httpStatus.getReasonPhrase(),
				eaeUserResponses);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeUserFormResponseBean>>>(httpResponse, httpStatus);
	}
	
	
	@RequestMapping(
			method = {RequestMethod.DELETE},
			path = "/responses/remove/{idForm}/{idUser}/{idResponse}/{idOwner}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormResponseBean>> deleteUserFormResponse (
			@PathVariable String idResponse,
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@PathVariable String idOwner
			) {
		EaeUserFormResponseBean eaeUserResponse		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idResponse = ").append(idResponse).append(", ");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idUser = ").append(idUser).append(", ");
		msg.append("idOwner = ").append(idOwner);
		msg.append("]");
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					this.eaeUserFormResponseService.removeUserFormResponse(idResponse, idForm, idUser, idOwner);
					responseMsg			= "the user form response with id : " + msg + " has been removed";
					httpStatus 			= HttpStatus.OK;
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "deleteUserFormResponse" + "]" + "unable to remove user form response " + msg + "\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to remove user form response " + msg, HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserFormResponseBean> httpResponse = new EaeHttpResponseBean<EaeUserFormResponseBean>(
				new Date(),
				responseMsg,
				"no more location - ressource has been deleted",
				httpStatus.getReasonPhrase(),
				eaeUserResponse);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormResponseBean>>(httpResponse, httpStatus);
	}
	
	/** MODIFY USER FORM RESPONSE **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/users/{idUser}/responses/{idResponse}/owners/{idOwner}/content",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormResponseBean>> modifyContentUserFormResponse ( 
			@PathVariable String idResponse,
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@PathVariable String idOwner,
			@RequestBody EaeUserResponseRequestMapping eaeUserResponseRequestMapping 
			) {
		EaeUserFormResponseBean eaeUserResponse		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idResponse = ").append(idResponse).append(", ");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idUser = ").append(idUser).append(", ");
		msg.append("idOwner = ").append(idOwner);
		msg.append("]");
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserFormResponseService.modifyContentUserResponse(idResponse, idForm, idUser, idOwner, eaeUserResponseRequestMapping.getContent());
					responseMsg			= "modification of user form response contents was successful";
					httpStatus 			= HttpStatus.OK;
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "modifyContentUserFormResponse" + "]" + "unable to change content of the user form response " + msg + "\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to change content of the user form response " + msg, HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserFormResponseBean> httpResponse = new EaeHttpResponseBean<EaeUserFormResponseBean>(
				new Date(),
				responseMsg,
				"/forms/" + idForm + "/users/" + idUser + "/responses/" + idResponse + "/owners/" + idOwner,
				httpStatus.getReasonPhrase(),
				eaeUserResponse);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormResponseBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/users/{idUser}/responses/{idResponse}/owners/{idOwner}/datecompletion",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormResponseBean>> modifyDtCompletionUserFormResponse ( 
			@PathVariable String idResponse,
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@PathVariable String idOwner,
			@RequestBody EaeUserResponseRequestMapping eaeUserResponseRequestMapping 
			) {
		EaeUserFormResponseBean eaeUserResponse		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idResponse = ").append(idResponse).append(", ");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idUser = ").append(idUser).append(", ");
		msg.append("idOwner = ").append(idOwner);
		msg.append("]");
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserFormResponseService.modifyDtCompletionUserResponse(idResponse, idForm, idUser, idOwner, eaeUserResponseRequestMapping.getDtCompletion());
					responseMsg			= "modification of user form response contents was successful";
					httpStatus 			= HttpStatus.OK;
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "modifyContentUserFormResponse" + "]" + "unable to change completion date of the user form response " + msg + "\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to change completion date of the user form response " + msg, HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserFormResponseBean> httpResponse = new EaeHttpResponseBean<EaeUserFormResponseBean>(
				new Date(),
				responseMsg,
				"/forms/" + idForm + "/users/" + idUser + "/responses/" + idResponse + "/owners/" + idOwner,
				httpStatus.getReasonPhrase(),
				eaeUserResponse);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormResponseBean>>(httpResponse, httpStatus);
	}
}
