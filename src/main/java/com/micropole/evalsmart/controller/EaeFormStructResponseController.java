package com.micropole.evalsmart.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.micropole.evalsmart.component.form.service.EaeFormStructResponseService;
import com.micropole.evalsmart.component.form.service.EaeUserService;
import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.exception.EaeHttpResponseException;
import com.micropole.evalsmart.model.EaeHttpResponseBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructResponseBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructResponseRequestMapping;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/eaeformstructresponse")
public class EaeFormStructResponseController {
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructResponseController.class);
	@Autowired
	private EaeUserService eaeFormUserService;
	@Autowired
	private EaeFormStructResponseService eaeFormStructResponseService;
	
	
	
	/** RETRIEVE RESPONSE **/
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/{idForm}/responses",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructResponseBean>>> getAllFormStructResponsesByForm ( @PathVariable String idForm ) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
		List<EaeFormStructResponseBean> eaeResponses = null;
			// retrieve responses
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeResponses = eaeFormStructResponseService.getAllFormStructResponsesByForm(idForm);
					if ( eaeResponses == null ) {
						throw new EaeHttpResponseException("An error occured retrieving responses from form : [" + idForm + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					if ( eaeResponses.isEmpty() ) {
						throw new EaeHttpResponseException("No response founded for form : [" + idForm + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					responseMsg	= eaeResponses.size() + " responses found for the form : [" + idForm + "]";
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getAllResponsesFromForm" + "]" + "unable to retrieve responses\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to responses form\n" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructResponseBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructResponseBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructresponse/forms/" + idForm + "/responses",
				httpStatus.getReasonPhrase(),
				eaeResponses);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructResponseBean>>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/domains/{idDomain}/responses",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructResponseBean>>> getAllResponsesByDomain ( @PathVariable String idDomain ) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
		List<EaeFormStructResponseBean> eaeResponses = null;
			// retrieve responses
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeResponses = eaeFormStructResponseService.getAllFormStructResponsesByDomain(idDomain);
					if ( eaeResponses == null ) {
						throw new EaeHttpResponseException("An error occured retrieving responses from domain : [" + idDomain + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					if ( eaeResponses.isEmpty() ) {
						throw new EaeHttpResponseException("No response founded for domain : [" + idDomain + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					responseMsg	= eaeResponses.size() + " responses found for the domain : [" + idDomain + "]";
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getActiveFormByOwner" + "]" + "unable to retrieve responses\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve responses\n" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructResponseBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructResponseBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructresponse/domains/" + idDomain + "/responses",
				httpStatus.getReasonPhrase(),
				eaeResponses);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructResponseBean>>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/queries/{idQuery}/responses",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructResponseBean>>> getAllFormStructResponsesByQuery ( @PathVariable String idQuery ) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
		List<EaeFormStructResponseBean> eaeResponses = null;
			// retrieve responses
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeResponses = eaeFormStructResponseService.getAllFormStructResponsesByQuery(idQuery);
					if ( eaeResponses == null ) {
						throw new EaeHttpResponseException("An error occured retrieving responses from query : [" + idQuery + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					if ( eaeResponses.isEmpty() ) {
						throw new EaeHttpResponseException("No response founded for query : [" + idQuery + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					responseMsg	= eaeResponses.size() + " responses found for the query : [" + idQuery + "]";
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getActiveFormByOwner" + "]" + "unable to retrieve responses\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve responses\n" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructResponseBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructResponseBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructresponse/queries/" + idQuery + "/responses",
				httpStatus.getReasonPhrase(),
				eaeResponses);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructResponseBean>>>(httpResponse, httpStatus);
	}
	
	/** CREATE RESPONSE **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/domains/{idDomain}/queries/{idQuery}/addResponses",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructResponseBean>>> addNewQueries (
			 @PathVariable String idForm,
			 @PathVariable String idDomain,
			 @PathVariable String idQuery,
			@RequestBody EaeFormStructResponseRequestMapping[] eaeFormStructResponseRequestMapping ) {
		List<EaeFormStructResponseBean> eaeResponses 	= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					// check and map queries data
					eaeResponses = eaeFormStructResponseService.mapAllDataResponses(idForm, idDomain, idQuery, eaeFormStructResponseRequestMapping);
					if ( eaeResponses != null ) {
						// save queries data
						eaeResponses = eaeFormStructResponseService.addNewEaeFormStructResponses(eaeResponses);
						responseMsg	= eaeResponses.size() + " responses have been created for the query with id [" + idQuery + "]";
						httpStatus	= HttpStatus.OK;
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "addUserForm" + "]" + "unable to create resonses for the form [" + idForm + "] and the query [" + idQuery + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to create resonses for the form [" + idForm + "] and the query [" + idQuery + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructResponseBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructResponseBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructquery/forms/" + idForm + "/domains/" + idDomain + "/queries/" + idQuery + "/responses",
				httpStatus.getReasonPhrase(),
				eaeResponses);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructResponseBean>>>(httpResponse, httpStatus);
	}
	
	/** UPDATE RESPONSE **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/responses/{idResponse}/label",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructResponseBean>> updateLabelFormStructResponse (
			@PathVariable String idResponse,
			@RequestParam String label) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructResponseService.changeLabel(idResponse, label);
					responseMsg			= "update label response succeed";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateLabelFormStructResponse" + "]" + "unable to perform update label response\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update label response", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructResponseBean> httpResponse = new EaeHttpResponseBean<EaeFormStructResponseBean>(
				new Date(),
				responseMsg,
				"/eaeformstructresponse/responses/" + idResponse + "/label",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructResponseBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/responses/{idResponse}/responseType",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructResponseBean>> updateResponseTypeFormStructResponse (
			@PathVariable String idResponse,
			@RequestParam String responseType) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructResponseService.changeResponseType(idResponse, responseType);
					responseMsg			= "update response type succeed";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateResponseTypeFormStructResponse" + "]" + "unable to perform update response type\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update response type", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructResponseBean> httpResponse = new EaeHttpResponseBean<EaeFormStructResponseBean>(
				new Date(),
				responseMsg,
				"/eaeformstructresponse/responses/" + idResponse + "/responseType",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructResponseBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/responses/{idResponse}/level",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructResponseBean>> updateLevelFormStructResponse (
			@PathVariable String idResponse,
			@RequestParam String level) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructResponseService.changeLevel(idResponse, level);
					responseMsg			= "update level response succeed";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateLevelFormStructResponse" + "]" + "unable to perform update level response\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update level response", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructResponseBean> httpResponse = new EaeHttpResponseBean<EaeFormStructResponseBean>(
				new Date(),
				responseMsg,
				"/eaeformstructresponse/responses/" + idResponse + "/level",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructResponseBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/responses/{idResponse}/content",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructResponseBean>> updateContentFormStructResponse (
			@PathVariable String idResponse,
			@RequestParam String content) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructResponseService.changeContent(idResponse, content);
					responseMsg			= "update content response succeed";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateContentFormStructResponse" + "]" + "unable to perform update content response\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update content response", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructResponseBean> httpResponse = new EaeHttpResponseBean<EaeFormStructResponseBean>(
				new Date(),
				responseMsg,
				"/eaeformstructresponse/responses/" + idResponse + "/content",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructResponseBean>>(httpResponse, httpStatus);
	}
}
