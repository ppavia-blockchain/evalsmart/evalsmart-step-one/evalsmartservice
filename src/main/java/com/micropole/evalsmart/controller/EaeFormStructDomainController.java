package com.micropole.evalsmart.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.micropole.evalsmart.component.form.service.EaeFormStructDomainService;
import com.micropole.evalsmart.component.form.service.EaeUserService;
import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.exception.EaeHttpResponseException;
import com.micropole.evalsmart.model.EaeHttpResponseBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructDomainBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructDomainRequestMapping;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/eaeformstructdomain")
public class EaeFormStructDomainController {
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructDomainController.class);

	@Autowired
	private EaeUserService eaeFormUserService;
	@Autowired
	private EaeFormStructDomainService eaeFormStructDomainService;
	
	/** RETRIEVE DOMAIN **/
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/{idForm}/domains",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructDomainBean>>> getAllFormStructDomainsByForm ( @PathVariable String idForm ) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
		List<EaeFormStructDomainBean> eaeDomains = null;
			// retrieve queries
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeDomains = eaeFormStructDomainService.getAllDomainsByForm(idForm);
					if ( eaeDomains == null ) {
						throw new EaeHttpResponseException("An error occured retrieving domains from form : [" + idForm + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					if ( eaeDomains.isEmpty() ) {
						throw new EaeHttpResponseException("No domain founded for form : [" + idForm + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					responseMsg	= eaeDomains.size() + " domains found for the form : [" + idForm + "]";
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getAllFormStructDomainsByForm" + "]" + "unable to retrieve queries\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve queries form\n" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructDomainBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructDomainBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructdomain/forms/" + idForm + "/domains",
				httpStatus.getReasonPhrase(),
				eaeDomains);
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructDomainBean>>>(httpResponse, httpStatus);
	}
	
	/** CREATE DOMAIN **/
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/domains/adddomains",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructDomainBean>>> addNewFormStructDomains (
			 @PathVariable String idForm,
			@RequestBody EaeFormStructDomainRequestMapping[] eaeFormStructDomainRequestMapping ) {
		List<EaeFormStructDomainBean> eaeDomains 	= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					// check and map domains data
					eaeDomains = eaeFormStructDomainService.mapAllDataDomains(idForm, eaeFormStructDomainRequestMapping);
					if ( eaeDomains != null ) {
						// save domains data
						eaeDomains = eaeFormStructDomainService.addNewFormStructDomains(eaeDomains);
						responseMsg	= eaeDomains.size() + " domain(s) has been created for the form with id [" + idForm + "]";
						httpStatus	= HttpStatus.OK;
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "addUserForm" + "]" + "unable to create domains for the form [" + idForm + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to create infos for the form [" + idForm + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructDomainBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructDomainBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructdomain/forms/" + idForm + "/domains",
				httpStatus.getReasonPhrase(),
				eaeDomains);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructDomainBean>>>(httpResponse, httpStatus);
	}
	
	
	/** UPDATE DOMAIN **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/domains/{idDomain}/label",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructDomainBean>> updateLabelFormStructDomain (
			@PathVariable String idDomain,
			@RequestParam String label) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructDomainService.changeLabel(idDomain, label);
					responseMsg			= "update label of the domain was successful";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateLabelQuery" + "]" + "unable to perform update label domain\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update label domain", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructDomainBean> httpResponse = new EaeHttpResponseBean<EaeFormStructDomainBean>(
				new Date(),
				responseMsg,
				"/eaeformstructdomain/domains/" + idDomain + "/label",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructDomainBean>>(httpResponse, httpStatus);
	}
}
