package com.micropole.evalsmart.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.micropole.evalsmart.component.form.service.EaeUserFormService;
import com.micropole.evalsmart.component.form.service.EaeUserService;
import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.exception.EaeHttpResponseException;
import com.micropole.evalsmart.model.EaeHttpResponseBean;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormBean;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormComponentsBean;
import com.micropole.evalsmart.model.requestmapping.EaeUserFormRequestMapping;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/eaeuserform")
public class EaeUserFormController {
	private static final Logger logger = LoggerFactory.getLogger(EaeUserFormController.class);
	
	@Autowired
	private EaeUserService eaeFormUserService;
	@Autowired
	private EaeUserFormService eaeUserFormService;
	
	/** RETRIEVE USER FORM **/
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/{idForm}/users/{idUser}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>> getUserFormByIds ( @PathVariable String idForm, @PathVariable String idUser) {
		EaeUserFormBean eaeForm		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeForm = eaeUserFormService.getUserFormByIds(idForm, idUser);
					if ( eaeForm == null ) {
						responseMsg	="no form with id [" + idForm + ", " + idUser + "]";
					}
					else {
						responseMsg	="form [" + idForm + "] recovery was successful";					
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getUserFormByIds" + "]" + "unable to retrieve user form with id [" + idForm + ", " + idUser + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve form with id [" + idForm + ", " + idUser + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserFormBean> httpResponse = new EaeHttpResponseBean<EaeUserFormBean>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/" + idForm + "/users/" + idUser,
				httpStatus.getReasonPhrase(),
				eaeForm);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/active/users/{idUser}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>> getActiveUserFormByOwner ( @PathVariable String idUser) {
		EaeUserFormBean eaeForm		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeForm = eaeUserFormService.getActiveUserFormByOwner(idUser);
					if ( eaeForm == null ) {
						responseMsg	="no active form belonging to [" + idUser + "]";
					}
					else {
						responseMsg	="form [" + eaeForm.getIdForm() + "] recovery was successful";					
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getActiveUserFormByOwner" + "]" + "unable to retrieve user form belonging to [" + idUser + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve form belonging to [" + idUser + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserFormBean> httpResponse = new EaeHttpResponseBean<EaeUserFormBean>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/active/users/" + idUser,
				httpStatus.getReasonPhrase(),
				eaeForm);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/byManager/{idManager}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeUserFormBean>>> getUserFormsByManager ( @PathVariable String idManager) {
		List<EaeUserFormBean> eaeForms		= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			// retrieve form
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeForms = eaeUserFormService.getUserFormsByManager(idManager);
					if ( eaeForms == null ) {
						responseMsg	="no user form for the manager [" + idManager + "]";
					}
					else {
						responseMsg	= eaeForms.size() + " user forms found for the manager [" + idManager + "]";
					}
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getActiveUserFormByOwner" + "]" + "unable to retrieve user form belonging to [" + idManager + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve form belonging to [" + idManager + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeUserFormBean>> httpResponse = new EaeHttpResponseBean<List<EaeUserFormBean>>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/byManager/" + idManager,
				httpStatus.getReasonPhrase(),
				eaeForms);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeUserFormBean>>>(httpResponse, httpStatus);
	}
	
	/** CREATE USER FORM **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/adduserform",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>> addUserForm ( @RequestBody EaeUserFormRequestMapping eaeUserFormRequestMapping ) {
		EaeUserFormBean eaeForm 	= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			EaeUserFormComponentsBean eaeComponents;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					// check and map form data
					eaeComponents = eaeUserFormService.mapComponentsUserForm(eaeUserFormRequestMapping);
					if ( eaeComponents != null ) {
						// save form data
						eaeForm = eaeUserFormService.addNewUserForm(
								eaeComponents.getEaeUserForm().getId(),
								eaeComponents.getEaeUserForm().getIdInterview(),
								eaeComponents.getEaeUserForm().getIdForm(), 
								eaeComponents.getEaeUserForm().getIdUser(),
								eaeComponents.getEaeUserForm().getUserRole(),
								eaeComponents.getEaeUserForm().getDtCreated(),
								eaeComponents.getEaeUserForm().getDtClosed(),
								eaeComponents.getEaeUserForm().getDtHistorized(), 
								eaeComponents.getEaeUserForm().getIdManager(), 
								eaeComponents.getEaeUserForm().getDtOwnerValidated(), 
								eaeComponents.getEaeUserForm().getDtManagerValidated(), 
								eaeComponents.getEaeUserForm().getDtOwnerSigned(), 
								eaeComponents.getEaeUserForm().getDtManagerSigned(), 
								eaeComponents.getEaeUserForm().getStatus(), 
								eaeComponents.getEaeUserForm().getCompletion());
						responseMsg	="the form with id : [" + eaeForm.getIdForm() + ", " + eaeForm.getIdUser() + "] has been created";
						httpStatus	= HttpStatus.OK;
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "addUserForm" + "]" + "unable to create form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to create form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserFormBean> httpResponse = new EaeHttpResponseBean<EaeUserFormBean>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/" + eaeForm.getIdForm() + "/users/" + eaeForm.getIdUser(),
				httpStatus.getReasonPhrase(),
				eaeForm);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>>(httpResponse, httpStatus);
	}
	
	/** UPDATE USER FORM **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/users/{idUser}/close",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>> updateDtClosedUserForm (
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@RequestBody EaeUserFormRequestMapping eaeUserFormRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserFormService.closeUserForm(idForm, idUser, eaeUserFormRequestMapping.getDtClosed());
					responseMsg			= "form closed successfully";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateDtClosedUserForm" + "]" + "unable to close form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to close form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeUserFormBean> httpResponse = new EaeHttpResponseBean<EaeUserFormBean>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/" + idForm + "/users/" + idUser + "/close",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/users/{idUser}/historize",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>> updateDtHistorizedUserForm (
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@RequestBody EaeUserFormRequestMapping eaeUserFormRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserFormService.historizeUserForm(idForm, idUser, eaeUserFormRequestMapping.getDtHistorized());
					responseMsg			= "form historized successfully";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateDtHistorizedUserForm" + "]" + "unable to historize form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to historize form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeUserFormBean> httpResponse = new EaeHttpResponseBean<EaeUserFormBean>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/" + idForm + "/users/" + idUser + "/historize",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/users/{idUser}/validate",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>> updateDtValidatedUserForm (
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@RequestBody EaeUserFormRequestMapping eaeUserFormRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserFormService.validatedUserForm(idForm, idUser, eaeUserFormRequestMapping.getUserRole(), eaeUserFormRequestMapping.getDtValidated());
					responseMsg			= "form validated successfully";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateDtValidatedUserForm" + "]" + "unable to validate form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to validate form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeUserFormBean> httpResponse = new EaeHttpResponseBean<EaeUserFormBean>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/" + idForm + "/users/" + idUser + "/validate",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/users/{idUser}/sign",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>> updateDtSignedUserForm (
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@RequestBody EaeUserFormRequestMapping eaeUserFormRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserFormService.signeUserForm(idForm, idUser, eaeUserFormRequestMapping.getUserRole(), eaeUserFormRequestMapping.getDtSigned());
					responseMsg			= "form signed successfully";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateDtSignedUserForm" + "]" + "unable to sign form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to sign form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeUserFormBean> httpResponse = new EaeHttpResponseBean<EaeUserFormBean>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/" + idForm + "/users/" + idUser + "/sign",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/users/{idUser}/userrole",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>> updateUserRoleUserForm (
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@RequestBody EaeUserFormRequestMapping eaeUserFormRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserFormService.changeUserRoleUserForm(idForm, idUser, eaeUserFormRequestMapping.getUserRole());
					responseMsg			= "modification of form user role was successful";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateUserRoleUserForm" + "]" + "unable to change user role\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to change user role", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeUserFormBean> httpResponse = new EaeHttpResponseBean<EaeUserFormBean>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/" + idForm + "/users/" + idUser + "/userrole",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/users/{idUser}/manager",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>> updateIdManagerUserForm (
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@RequestBody EaeUserFormRequestMapping eaeUserFormRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserFormService.changeManagerUserForm(idForm, idUser, eaeUserFormRequestMapping.getIdManager());
					responseMsg			= "modification of form manager was successful";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateIdManagerUserForm" + "]" + "unable to change manager\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to change manager", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeUserFormBean> httpResponse = new EaeHttpResponseBean<EaeUserFormBean>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/" + idForm + "/users/" + idUser + "/manager",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/users/{idUser}/status",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>> updateStatusUserForm (
			@PathVariable String idForm, 
			@PathVariable String idUser,
			@RequestBody EaeUserFormRequestMapping eaeUserFormRequestMapping) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUserFormService.changeStatusUserForm(idForm, idUser, eaeUserFormRequestMapping.getStatus());
					responseMsg			= "modification of form status was successful";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateStatusUserForm" + "]" + "unable to change status\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to change status", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeUserFormBean> httpResponse = new EaeHttpResponseBean<EaeUserFormBean>(
				new Date(),
				responseMsg,
				"/eaeuserform/forms/" + idForm + "/users/" + idUser + "/status",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserFormBean>>(httpResponse, httpStatus);
	}
}
