package com.micropole.evalsmart.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.micropole.evalsmart.component.form.service.EaeFormStructQueryService;
import com.micropole.evalsmart.component.form.service.EaeUserService;
import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.exception.EaeHttpResponseException;
import com.micropole.evalsmart.model.EaeHttpResponseBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructQueryBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructQueryRequestMapping;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/eaeformstructquery")
public class EaeFormStructQueryController {
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructQueryController.class);

	@Autowired
	private EaeUserService eaeFormUserService;
	@Autowired
	private EaeFormStructQueryService eaeFormStructQueryService;
	
	/** RETRIEVE QUERY DATA **/
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/forms/{idForm}/queries",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructQueryBean>>> getAllFormStructQueriesByForm ( @PathVariable String idForm ) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
		List<EaeFormStructQueryBean> eaeQueries = null;
			// retrieve queries
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeQueries = eaeFormStructQueryService.getAllQueriesByForm(idForm);
					if ( eaeQueries == null ) {
						throw new EaeHttpResponseException("An error occured retrieving queries from form : [" + idForm + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					if ( eaeQueries.isEmpty() ) {
						throw new EaeHttpResponseException("No query founded for form : [" + idForm + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					responseMsg	= eaeQueries.size() + " queries found for the form : [" + idForm + "]";
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getAllQueriesByForm" + "]" + "unable to retrieve queries\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to queries form\n" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructQueryBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructQueryBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructquery/forms/" + idForm + "/queries",
				httpStatus.getReasonPhrase(),
				eaeQueries);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructQueryBean>>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/domains/{idDomain}/queries",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructQueryBean>>> getAllFormStructQueriesByDomain ( @PathVariable String idDomain ) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
		List<EaeFormStructQueryBean> eaeQueries = null;
			// retrieve queries
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeQueries = eaeFormStructQueryService.getAllQueriesByDomain(idDomain);
					if ( eaeQueries == null ) {
						throw new EaeHttpResponseException("An error occured retrieving queries from domain : [" + idDomain + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					if ( eaeQueries.isEmpty() ) {
						throw new EaeHttpResponseException("No query founded for domain : [" + idDomain + "]", HttpStatus.INTERNAL_SERVER_ERROR);
					}
					responseMsg	= eaeQueries.size() + " queries found for the domain : [" + idDomain + "]";
					httpStatus	= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getAllFormStructQueriesByForm" + "]" + "unable to retrieve queries\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to queries form\n" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructQueryBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructQueryBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructquery/domains/" + idDomain + "/queries",
				httpStatus.getReasonPhrase(),
				eaeQueries);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructQueryBean>>>(httpResponse, httpStatus);
	}
	
	
	
	/** CREATE QUERY DATA **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/forms/{idForm}/domains/{idDomain}/addQueries",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<List<EaeFormStructQueryBean>>> addNewQueries (
			 @PathVariable String idForm,
			 @PathVariable String idDomain,
			@RequestBody EaeFormStructQueryRequestMapping[] eaeFormStructQueryRequestMapping ) {
		List<EaeFormStructQueryBean> eaeQueries 	= null;
		String responseMsg		= "user is not authorized to perform this request";
		HttpStatus httpStatus 	= HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					// check and map queries data
					eaeQueries = eaeFormStructQueryService.mapAllDataQueries(idForm, idDomain, eaeFormStructQueryRequestMapping);
					if ( eaeQueries != null ) {
						// save queries data
						eaeQueries = eaeFormStructQueryService.addNewEaeFormQueries(eaeQueries);
						responseMsg	= eaeQueries.size() + " queries have been created for the domain with id [" + idDomain + "]";
						httpStatus	= HttpStatus.OK;
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "addUserForm" + "]" + "unable to create queries for the form [" + idForm + "] and the domain [" + idDomain + "]\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to create queries for the form [" + idForm + "] and the domain [" + idDomain + "]", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<List<EaeFormStructQueryBean>> httpResponse = new EaeHttpResponseBean<List<EaeFormStructQueryBean>>(
				new Date(),
				responseMsg,
				"/eaeformstructquery/forms/" + idForm + "/domains/" + idDomain + "/queries",
				httpStatus.getReasonPhrase(),
				eaeQueries);			
		return new ResponseEntity<EaeHttpResponseBean<List<EaeFormStructQueryBean>>>(httpResponse, httpStatus);
	}
	
	
	/** UPDATE QUERY DATA **/
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/queries/{idQuery}/label",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructQueryBean>> updateLabelFormStructQuery (
			@PathVariable String idQuery,
			@RequestParam String label) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructQueryService.changeLabel(idQuery, label);
					responseMsg			= "update label property of the query was successful";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateLabelFormStructQuery" + "]" + "unable to perform update label query\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update label query", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructQueryBean> httpResponse = new EaeHttpResponseBean<EaeFormStructQueryBean>(
				new Date(),
				responseMsg,
				"/eaeformstructquery/queries/" + idQuery + "/label",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructQueryBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/queries/{idQuery}/queryType",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructQueryBean>> updateQueryTypeFormStructQuery (
			@PathVariable String idQuery,
			@RequestParam String queryType) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructQueryService.changeQueryType(idQuery, queryType);
					responseMsg			= "update query type property of the query was successful";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateQueryTypeFormStructQuery" + "]" + "unable to perform update query type\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update query type", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructQueryBean> httpResponse = new EaeHttpResponseBean<EaeFormStructQueryBean>(
				new Date(),
				responseMsg,
				"/eaeformstructquery/queries/" + idQuery + "/label",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructQueryBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/queries/{idQuery}/stateCompletion",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeFormStructQueryBean>> updateStateCompletionFormStructQuery (
			@PathVariable String idQuery,
			@RequestParam boolean isCompleted) {
		String responseMsg	= "user is not authorized to perform this request";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeFormStructQueryService.changeStateCompletion(idQuery, isCompleted);
					responseMsg			= "update state completion property of the query was successful";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateStateCompletionFormStructQuery" + "]" + "unable to perform update label query\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update label query", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		
		EaeHttpResponseBean<EaeFormStructQueryBean> httpResponse = new EaeHttpResponseBean<EaeFormStructQueryBean>(
				new Date(),
				responseMsg,
				"/eaeformstructquery/queries/" + idQuery + "/stateCompletion",
				httpStatus.getReasonPhrase(),
				null);			
		return new ResponseEntity<EaeHttpResponseBean<EaeFormStructQueryBean>>(httpResponse, httpStatus);
	}
}
