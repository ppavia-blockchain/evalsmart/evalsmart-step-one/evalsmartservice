package com.micropole.evalsmart.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.micropole.evalsmart.component.form.service.EaeUserService;
import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.exception.EaeHttpResponseException;
import com.micropole.evalsmart.model.EaeHttpResponseBean;
import com.micropole.evalsmart.model.component.form.bean.EaeUserBean;
import com.micropole.evalsmart.model.requestmapping.EaeUserRequestMapping;
import com.micropole.evalsmart.utils.EvalSmartUtils;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/users")
public class EaeUserController {
	private static final Logger logger = LoggerFactory.getLogger(EaeUserController.class);

	@Autowired
	private EaeUserService eaeFormUserService;

	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/{idUser}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserBean>> getUser (@PathVariable String idUser) {
		
		String responseMsg		= "user is not authorized";
		HttpStatus httpStatus	= HttpStatus.UNAUTHORIZED;
		EaeUserBean eaeUser	= null;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUser				= eaeFormUserService.getUserById(idUser);
					if ( eaeUser == null ) {
						responseMsg			= "no user found with id [" + idUser + "]";
						httpStatus 			= HttpStatus.OK;
					}
					else {
						responseMsg			= "get user succeed";
						httpStatus 			= HttpStatus.OK;					
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getUser" + "]" + "unable to retrieve user\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve user", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserBean> httpResponse = new EaeHttpResponseBean<EaeUserBean>(
				new Date(),
				responseMsg,
				"/users/" + idUser,
				httpStatus.getReasonPhrase(),
				eaeUser);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/email/{email}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserBean>> getUserByMail (@PathVariable String email) {
		
		String responseMsg		= "user is not authorized";
		HttpStatus httpStatus	= HttpStatus.UNAUTHORIZED;
		EaeUserBean eaeUser	= null;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUser				= eaeFormUserService.getUserByEmail(email);
					if ( eaeUser == null ) {
						responseMsg			= "no user found with email [" + email + "]";
						httpStatus 			= HttpStatus.OK;
					}
					else {
						responseMsg			= "get user succeed";
						httpStatus 			= HttpStatus.OK;					
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getUser" + "]" + "unable to retrieve user\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve user", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserBean> httpResponse = new EaeHttpResponseBean<EaeUserBean>(
				new Date(),
				responseMsg,
				"/users/email/" + email,
				httpStatus.getReasonPhrase(),
				eaeUser);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserBean>>(httpResponse, httpStatus);
	}
	
	@RequestMapping(
			method = {RequestMethod.GET},
			path = "/username/{userName}",
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserBean>> getUserByUserName (@PathVariable String userName) {
		
		String responseMsg		= "user is not authorized";
		HttpStatus httpStatus	= HttpStatus.UNAUTHORIZED;
		EaeUserBean eaeUser	= null;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					eaeUser				= eaeFormUserService.getUserByUserName(userName);
					if ( eaeUser == null ) {
						responseMsg			= "no user found with user name [" + userName + "]";
						httpStatus 			= HttpStatus.OK;
					}
					else {
						responseMsg			= "get user succeed";
						httpStatus 			= HttpStatus.OK;					
					}
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "getUser" + "]" + "unable to retrieve user\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to retrieve user", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserBean> httpResponse = new EaeHttpResponseBean<EaeUserBean>(
				new Date(),
				responseMsg,
				"/users/username/" + userName,
				httpStatus.getReasonPhrase(),
				eaeUser);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserBean>>(httpResponse, httpStatus);
	}

	/**
	 * Add new eaeUser in database
	 * @param idUser
	 * @param eaeUserRequestMapping
	 * @return
	 */
	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/add",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public @ResponseBody ResponseEntity<EaeHttpResponseBean<EaeUserBean>> addUser (@RequestBody EaeUserRequestMapping eaeUserRequestMapping) {
		String responseMsg	= "user is not authorized to perform update form";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
		EaeUserBean eaeUserBean	= null;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					EvalSmartUtils.checkUserParams(
							null,
							eaeUserRequestMapping.getProfile(),
							eaeUserRequestMapping.getStatus(),
							eaeUserRequestMapping.getFirstName(),
							eaeUserRequestMapping.getLastName(),
							eaeUserRequestMapping.getUserName(),
							eaeUserRequestMapping.getEmail(),
							eaeUserRequestMapping.getPhone(),
							EvalSmartUtils.convertArrayIntoList(eaeUserRequestMapping.getAccount())
							);
					eaeUserBean	= eaeFormUserService.addNewUser( 
							eaeUserRequestMapping.getProfile(),
							eaeUserRequestMapping.getStatus(),
							eaeUserRequestMapping.getFirstName(), 
							eaeUserRequestMapping.getLastName(),
							eaeUserRequestMapping.getUserName(),
							eaeUserRequestMapping.getEmail(), 
							eaeUserRequestMapping.getPhone(), 
							EvalSmartUtils.convertArrayIntoList(eaeUserRequestMapping.getAccount()));
					responseMsg			= "add new user succeed";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateStatusForm" + "]" + "unable to perform update status form\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update status form", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserBean> httpResponse = new EaeHttpResponseBean<EaeUserBean>(
				new Date(),
				responseMsg,
				"/users/new",
				httpStatus.getReasonPhrase(),
				eaeUserBean);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserBean>>(httpResponse, httpStatus);
	}

	@RequestMapping(
			method = {RequestMethod.POST},
			path = "/{idUser}",
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<EaeHttpResponseBean<EaeUserBean>> updateUser (
			@PathVariable String idUser,
			@RequestBody EaeUserRequestMapping eaeUserRequestMapping
			) {
		String responseMsg	= "user is not authorized to perform update form";
		HttpStatus httpStatus = HttpStatus.UNAUTHORIZED;
		EaeUserBean eaeUserBean	= null;
			try {
				if ( eaeFormUserService.isUserAuthenticated() ) {
					if ( !EvalSmartUtils.checkUserParams(
							idUser,
							eaeUserRequestMapping.getProfile(),
							eaeUserRequestMapping.getStatus(),
							eaeUserRequestMapping.getFirstName(),
							eaeUserRequestMapping.getLastName(),
							eaeUserRequestMapping.getUserName(),
							eaeUserRequestMapping.getEmail(),
							eaeUserRequestMapping.getPhone(),
							EvalSmartUtils.convertArrayIntoList(eaeUserRequestMapping.getAccount())
							) ) {
						logger.error("[" + "updateUser" + "]" + "some data are invalid\n");
						throw new EaeHttpResponseException("some data are invalid", HttpStatus.BAD_REQUEST);
					}
					eaeUserBean	= eaeFormUserService.updateUser(
							idUser,
							eaeUserRequestMapping.getProfile(),
							eaeUserRequestMapping.getStatus(),
							eaeUserRequestMapping.getFirstName(),
							eaeUserRequestMapping.getLastName(),
							eaeUserRequestMapping.getUserName(),
							eaeUserRequestMapping.getEmail(),
							eaeUserRequestMapping.getPhone(),
							EvalSmartUtils.convertArrayIntoList(eaeUserRequestMapping.getAccount()));
					responseMsg			= "update user succeed";
					httpStatus 			= HttpStatus.OK;
				}
			} catch (EaeFormComponentException e) {
				logger.error("[" + "updateUser" + "]" + "unable to perform update user\n" + e.getMessage());
				throw new EaeHttpResponseException("unable to perform update user", HttpStatus.INTERNAL_SERVER_ERROR );
			}
		EaeHttpResponseBean<EaeUserBean> httpResponse = new EaeHttpResponseBean<EaeUserBean>(
				new Date(),
				responseMsg,
				"/users/new",
				httpStatus.getReasonPhrase(),
				eaeUserBean);			
		return new ResponseEntity<EaeHttpResponseBean<EaeUserBean>>(httpResponse, httpStatus);
	}
}
