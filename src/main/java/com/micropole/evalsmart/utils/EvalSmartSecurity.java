package com.micropole.evalsmart.utils;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.EaeComponentTypeIdEnum;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructDomainRequestMapping;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructQueryRequestMapping;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructRequestMapping;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructResponseLevelRequestMapping;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructResponseRequestMapping;
import com.micropole.evalsmart.model.requestmapping.EaeUserFormRequestMapping;
import com.micropole.evalsmart.model.requestmapping.EaeUserResponseRequestMapping;

public class EvalSmartSecurity {
	private static final String nameClass			= EvalSmartSecurity.class.getSimpleName();
	private static final Logger logger 				= LoggerFactory.getLogger(EvalSmartSecurity.class);
	
	
	/** CHECK PARAMETERS **/
	
	public static boolean checkLabel(String label) {
		if ( label == null || label.isEmpty() ) {
			logger.error("[" + "checkLabel" + "]" + "label can not be empty");
			return false;
		}
		return true;
	}

	public static boolean checkDateFormat(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setLenient(false);
		cal.setTime(date);
		try {
			cal.getTime();
		}
		catch (Exception e) {
			logger.error("[" + "checkDateFormat" + "]" + "date" + " = " + date + " is invalid\n" + e.getMessage());
			return false;
		}
		return true;
	}
	
	public static boolean checkStatus(String status) {
		// TODO - implementation
		return true;
	}

	public static boolean checkQueryType(String queryType) {
		// TODO - implementation
		return true;
	}
	
	public static boolean checkIdComponents (String id) throws EaeFormComponentException {
		if ( id == null || id.isEmpty() ) {
			throw new EaeFormComponentException("[" + "checkIdComponents" + "]" + "id is empty");
		}
		
		String[] idComponents	= id.split("_");
		if ( idComponents.length != 3 ) {
			throw new EaeFormComponentException("[" + "checkIdComponents" + "]" + "id structure is invalid");
		}
		
		if ( !EaeComponentTypeIdEnum.getValues().contains(idComponents[0]) ) {
			throw new EaeFormComponentException("[" + "checkIdComponents" + "]" + "id prefix is invalid");
		}
		
		try {
			int comp2	= Integer.parseInt(idComponents[1]);
			if ( comp2 > 10000 || comp2 < 0 ) {
				throw new EaeFormComponentException("[" + "checkIdComponents" + "]" + "the second id component is out of range");
			}
		}
		catch (NumberFormatException e) {
			throw new EaeFormComponentException("[" + "checkIdComponents" + "]" + "unable to convert second component id to a whole number\n", e);
		}
		
		try {
			Integer.parseInt(idComponents[2]);
		}
		catch (NumberFormatException e) {
			throw new EaeFormComponentException("[" + "checkIdComponents" + "]" + "unable to convert third component id to a whole number\n", e);
		}
		
		return true;
	}
	
	public static boolean checkIdForm(String idForm) throws EaeFormComponentException {
		try {
			EvalSmartSecurity.checkIdComponents(idForm);
		}
		catch (EaeFormComponentException e) {
			logger.error("[" + "checkIdForm" + "]" + "idForm" + " = " + idForm + "\n" + e.getMessage());
			return false;
		}
		return true;
	}

	public static boolean checkIdDomain(String idDomain) throws EaeFormComponentException {
		try {
			EvalSmartSecurity.checkIdComponents(idDomain);
		}
		catch (EaeFormComponentException e) {
			logger.error("[" + "checkIdDomain" + "]" + "idDomain" + " = " + idDomain + "\n" + e.getMessage());
			return false;
		}
		return true;
	}

	public static boolean checkIdQuery(String idQuery) throws EaeFormComponentException {
		try {
			EvalSmartSecurity.checkIdComponents(idQuery);
		}
		catch (EaeFormComponentException e) {
			logger.error("[" + "checkIdQuery" + "]" + "idQuery" + " = " + idQuery + "\n" + e.getMessage());
			return false;
		}
		return true;
	}

	public static boolean checkIdResponses(String idResonse) throws EaeFormComponentException {
		return EvalSmartSecurity.checkIdComponents(idResonse);

	}

	public static boolean checkFormEntries(EaeFormStructRequestMapping eaeFormData) throws EaeFormComponentException {
		boolean ret	= true;
		// TODO - not implemented
		try {
			StringBuilder msg	= new StringBuilder("[").append("checkFormEntries").append("]");
		}
		catch (Exception e) {
			throw new EaeFormComponentException("An error occured checking form entries\n", e);
		}
		return ret;
	}

	public static boolean checkDomainEntries(EaeFormStructDomainRequestMapping eaeDomainData) throws EaeFormComponentException {
		// TODO Auto-generated method stub
		return true;
	}

	public static boolean checkQueryEntries(EaeFormStructQueryRequestMapping eaeQueryData) throws EaeFormComponentException {
		// TODO Auto-generated method stub
		return true;
	}

	public static boolean checkResponseEntries(EaeFormStructResponseRequestMapping eaeResponseData) throws EaeFormComponentException {
		// TODO Auto-generated method stub
		return true;
	}
	
	public static boolean checkResponseLevelEntries(EaeFormStructResponseLevelRequestMapping eaeResponseLevelData) throws EaeFormComponentException {
		// TODO Auto-generated method stub
		return true;
	}
	
	public static boolean checkUpdateFormEntries(EaeFormStructBean initialForm, EaeFormStructRequestMapping eaeFormData) throws EaeFormComponentException {
		boolean ret	= true;
//		try {
//			StringBuilder msg	= new StringBuilder("[").append("checkFormEntries").append("]");
//			msg.append("[FORM:").append(initialForm.getIdForm()).append("]").append("\n");
//			if ( initialForm.getDtCreated().equals(eaeFormData.getDtCreated()) ) {
//				msg.append("The parameter 'creation date' of the form not match with original creation date").append("\n");
//				ret = false;
//			}
//			if ( initialForm.getIdOwner().equals(eaeFormData.getIdOwner()) ) {
//				msg.append("The parameter 'idOwner' of the form not match with original idOwner").append("\n");
//				ret = false;
//			}
//			if ( ret ) {
//				throw new EaeFormComponentException(msg.toString());
//			}
//		}
//		catch (Exception e) {
//			throw new EaeFormComponentException("An error occured checking form entries\n", e);
//		}
		return ret;
	}

	public static boolean checkUpdateDomainEntries(EaeFormStructDomainRequestMapping eaeDomainData) throws EaeFormComponentException {
		// TODO Auto-generated method stub
		return true;
	}

	public static boolean checkUpdateQueryEntries(EaeFormStructQueryRequestMapping eaeQueryData) throws EaeFormComponentException {
		// TODO Auto-generated method stub
		return true;
	}

	public static boolean checkUpdateResponseEntries(EaeFormStructResponseRequestMapping eaeResponseData) throws EaeFormComponentException {
		// TODO Auto-generated method stub
		return true;
	}
	
	public static boolean checkUserFormEntries(EaeUserFormRequestMapping eaeUserFormData) throws EaeFormComponentException {
		boolean ret	= true;
		// TODO - not implemented
		try {
			StringBuilder msg	= new StringBuilder("[").append("checkUserFormEntries").append("]");
		}
		catch (Exception e) {
			throw new EaeFormComponentException("An error occured checking user form entries\n", e);
		}
		return ret;
	}
	
	public static boolean checkUserResponseEntries(EaeUserResponseRequestMapping eaeUserResponseData) throws EaeFormComponentException {
		boolean ret	= true;
		// TODO - not implemented
		try {
			StringBuilder msg	= new StringBuilder("[").append("checkUserResponseEntries").append("]");
		}
		catch (Exception e) {
			throw new EaeFormComponentException("An error occured checking user response entries\n", e);
		}
		return ret;
	}
}
