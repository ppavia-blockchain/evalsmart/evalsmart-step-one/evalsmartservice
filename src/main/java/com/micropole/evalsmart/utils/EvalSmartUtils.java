package com.micropole.evalsmart.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.micropole.evalsmart.exception.EaeFormComponentException;

public class EvalSmartUtils {
	private static final String DT_YYYY_MM_DD = "yyyy-MM-dd";
	
	public static Date convertDateStringToDate (String dateString) throws EaeFormComponentException {
		Date date	= null;
		try {
			SimpleDateFormat formatter	= new SimpleDateFormat(DT_YYYY_MM_DD);
			date	= formatter.parse(dateString);			
		}
		catch (ParseException e) {
			throw new EaeFormComponentException(e.getMessage(), e);
		}
		return date;
	}
	
	public static String convertDateToDateString (Date date) {
		// TODO - implementation
		String dateString	= null;
		return dateString;
	}
	
	public static List<String> formatParams (String... params) {
		List<String> formattedParams	= new ArrayList<String>();
		for ( String param : params ) {
			if ( "null".equals(param.toLowerCase()) ) {
				formattedParams.add(null);
			}
			else {
				formattedParams.add(param);
			}
		}
		return formattedParams;
	}
	
	public static String setFullName (String _firstName, String _lastName, String format) {
		String firstName	= _firstName.trim();
		String lastName		= _lastName.trim().toUpperCase();
		if ( firstName.length() > 1 ) {
			firstName	= firstName.substring(0, 1).toUpperCase().concat(firstName.toLowerCase().substring(1));			
		}
		if ( lastName.length() > 1 ) {
			if ( "UPPER_CASE".equals(format) ) {
				lastName		= lastName.substring(0, 1).concat(lastName.substring(1));	
			}
			else {
				lastName		= lastName.substring(0, 1).concat(lastName.toLowerCase().substring(1));			
			}
		}
		
		String fullName	= String.format("%s %s", lastName, firstName);
		return fullName;
	}
	
	public static boolean isIdValid (String id) {
		// TODO - implementation
		return true;
	}
	public static boolean isEmailValid (String email) {
		// TODO - implementation
		return true;
	}
	public static boolean isPhoneValid (String phone) {
		// TODO - implementation
		return true;
	}
	public static boolean isNameValid (String name) {
		// TODO - implementation
		return true;
	}
	public static boolean isAccountsValid (List<String> account) {
		// TODO - implementation
		return true;
	}
	public static boolean isProfileValid (String profile) {
		// TODO - implementation
		return true;
	}
	public static boolean isRoleValid (String role) {
		// TODO - implementation
		return true;
	}
	public static boolean isStatusValid (String status) {
		// TODO - implementation
		return true;
	}
	
	public static boolean checkUserParams (
			String idUser, 
			String profile,
			String status,
			String firstName, 
			String lastName,
			String userName,
			String email, 
			String phone, 
			List<String> account) throws EaeFormComponentException {
		return ( 
				isIdValid(idUser) && 
				isProfileValid(profile) &&
				isStatusValid(status) &&
				isNameValid(firstName) && 
				isNameValid(lastName) &&
				isEmailValid(email) &&
				isPhoneValid(phone) &&
				isAccountsValid(account)
		);
	}
	
	public static String generateId (String type) throws EaeFormComponentException {
		Random random = new Random();
		return type + "_" + random.nextInt(10000) + "_" + System.currentTimeMillis();
	}
 	
	public static <T> List<T> convertArrayIntoList ( T[] arrayToConvert ) {
		return Arrays.asList(arrayToConvert);
	}
	
	
}
