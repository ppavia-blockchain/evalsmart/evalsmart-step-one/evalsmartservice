package com.micropole.evalsmart.component.form.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormBean;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormComponentsBean;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormResponseBean;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomUserFormRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeUserFormRepository;
import com.micropole.evalsmart.model.requestmapping.EaeUserFormRequestMapping;
import com.micropole.evalsmart.utils.EvalSmartSecurity;

public class EaeUserFormServiceImpl implements EaeUserFormService {
	private static final Logger logger = LoggerFactory.getLogger(EaeUserFormServiceImpl.class);
	private static final String CODE_USERROLE_MANAGER			= "001";
	private static final String CODE_USERROLE_COLABORATOR		= "002";

	@Autowired
	private EaeUserFormRepository eaeUserFormRepository;
	@Autowired
	private EaeCustomUserFormRepository eaeCustomUserFormRepository;
	@Autowired
	private EaeUserFormResponseService eaeUserFormResponseService;
	
	
	/** RETRIEVE **/
	
	@Override
	public EaeUserFormBean getUserFormByIds(String idForm, String idUser) throws EaeFormComponentException {
		EaeUserFormBean eaeUserForm = null;
		try {
			eaeUserForm	= eaeCustomUserFormRepository.findUserForm(idForm, idUser);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("error getting form with id [" + idForm + "]", e);
		}
		return eaeUserForm;
	}

	@Override
	public List<EaeUserFormBean> getUserFormByStatus(List<String> status) throws EaeFormComponentException {
		List<EaeUserFormBean>	eaeUserForms = new ArrayList<EaeUserFormBean>();
		try {
			eaeUserForms	= eaeCustomUserFormRepository.findUserFormByStatus(status);
		}
		catch (EaeFormComponentException e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeUserForms;
	}
	
	@Override
	public List<EaeUserFormBean> getUserFormsByManager(String idManager) throws EaeFormComponentException {
		List<EaeUserFormBean>	eaeUserForms = new ArrayList<EaeUserFormBean>();
		try {
			eaeUserForms	= eaeCustomUserFormRepository.findUserFormsByManager(idManager);
		}
		catch (EaeFormComponentException e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeUserForms;
	}

	@Override
	public EaeUserFormBean getActiveUserFormByOwner(String idUser) throws EaeFormComponentException {
		EaeUserFormBean eaeUserForm = null;
		try {
			eaeUserForm	= eaeCustomUserFormRepository.findActiveUserFormByUser(idUser);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("error getting user form belonging to [" + idUser + "]", e);
		}
		return eaeUserForm;
	}
	
	/** MODIFY **/

	@Override
	public void closeUserForm(String idForm, String idUser, Date date) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomUserFormRepository.updateDtClosedUserForm(idForm, idUser, date);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void changeUserRoleUserForm(String idForm, String idUser, String userRole) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomUserFormRepository.updateUserRoleUserForm(idForm, idUser, userRole);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void historizeUserForm(String idForm, String idUser, Date date) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomUserFormRepository.updateDtHistorizedUserForm(idForm, idUser, date);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void changeManagerUserForm(String idForm, String idUser, String idManager) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomUserFormRepository.updateIdManagerUserForm(idForm, idUser, idManager);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void validatedUserForm(String idForm, String idUser, String userRole, Date date) throws EaeFormComponentException {
		switch (userRole) {
			case CODE_USERROLE_COLABORATOR :
				// TODO - check validation data
				try {
					eaeCustomUserFormRepository.updateDtOwnerValidatedUserForm(idForm, idUser, date);
				}
				catch (Exception e) {
					throw new EaeFormComponentException (e.getMessage(), e);
				}
				break;
			case CODE_USERROLE_MANAGER :
				// TODO - check validation data
				try {
					eaeCustomUserFormRepository.updateDtManagerValidatedUserForm(idForm, idUser, date);
				}
				catch (Exception e) {
					throw new EaeFormComponentException (e.getMessage(), e);
				}
				break;
			default :
				throw new EaeFormComponentException ("role [" + userRole + "] is not reconized");
		}

	}

	@Override
	public void signeUserForm(String idForm, String idUser, String userRole, Date date) throws EaeFormComponentException {
		switch (userRole) {
			case CODE_USERROLE_COLABORATOR :
				// TODO - check validation data
				try {
					eaeCustomUserFormRepository.updateDtOwnerSignedUserForm(idForm, idUser, date);
				}
				catch (Exception e) {
					throw new EaeFormComponentException (e.getMessage(), e);
				}
				break;
			case CODE_USERROLE_MANAGER :
				// TODO - check validation data
				try {
					eaeCustomUserFormRepository.updateDtManagerSignedUserForm(idForm, idUser, date);
				}
				catch (Exception e) {
					throw new EaeFormComponentException (e.getMessage(), e);
				}
				break;
			default :
				throw new EaeFormComponentException ("role [" + userRole + "] is not reconized");
		}
	}

	@Override
	public void changeStatusUserForm(String idForm, String idUser, String status) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomUserFormRepository.updateStatusUserForm(idForm, idUser, status);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void modifyCompletionUserForm(String idForm, String idUser, Float completion) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomUserFormRepository.updateCompletionUserForm(idForm, idUser, completion);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void modifyDataUserForm(
			String id,
			String idInterview,
			String idForm,
			String idUser,
			String userRole,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String idManager,
			Date dtOwnerValidated,
			Date dtManagerValidated,
			Date dtOwnerSigned,
			Date dtManagerSigned,
			String status,
			Float completion) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomUserFormRepository.updateDataUserForm(
					id,
					idInterview,
					idForm,
					idUser,
					userRole,
					dtCreated,
					dtClosed,
					dtHistorized,
					idManager,
					dtOwnerValidated,
					dtManagerValidated,
					dtOwnerSigned,
					dtManagerSigned,
					status,
					completion);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}

	}
	
	/** ADD **/

	@Override
	public EaeUserFormBean addNewUserForm(
			String id,
			String idInterview,
			String idForm,
			String idUser,
			String userRole,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String idManager,
			Date dtOwnerValidated,
			Date dtManagerValidated,
			Date dtOwnerSigned,
			Date dtManagerSigned,
			String status,
			Float completion) throws EaeFormComponentException {
		EaeUserFormBean eaeUserForm	= null;
		
		try {
			eaeUserForm			= new EaeUserFormBean();
			eaeUserForm.setId(id);
			eaeUserForm.setIdInterview(idInterview);
			eaeUserForm.setIdForm(idForm);
			eaeUserForm.setIdForm(idForm);
			eaeUserForm.setIdUser(idUser);
			eaeUserForm.setUserRole(userRole);
			eaeUserForm.setDtCreated(dtCreated);
			eaeUserForm.setDtClosed(dtClosed);
			eaeUserForm.setDtHistorized(dtHistorized);
			eaeUserForm.setIdManager(idManager);
			eaeUserForm.setDtOwnerValidated(dtOwnerValidated);
			eaeUserForm.setDtManagerValidated(dtManagerValidated);
			eaeUserForm.setDtOwnerSigned(dtOwnerSigned);
			eaeUserForm.setDtManagerSigned(dtManagerSigned);
			eaeUserForm.setStatus(status);
			eaeUserForm.setCompletion(completion);
			// save user form
			eaeUserForm = eaeUserFormRepository.save(eaeUserForm); 
		}
		catch (Exception e) {
			StringBuilder msg	= new StringBuilder("");
			msg.append("error creating user form with ids ")
			.append("[")
			.append(idForm).append(", ")
			.append(idUser)
			.append("]");			
			throw new EaeFormComponentException (msg.toString(), e);
		}
		
		return eaeUserForm;
	}
	
	/** MAP **/

	@Override
	public EaeUserFormBean mapDataUserForm(EaeUserFormRequestMapping eaeUserFormData) throws EaeFormComponentException {
		EaeUserFormBean eaeUserForm	= null;
		eaeUserForm	= new EaeUserFormBean();
		eaeUserForm.setId(eaeUserFormData.getId());
		eaeUserForm.setIdInterview(eaeUserFormData.getIdInterview());
		eaeUserForm.setIdForm(eaeUserFormData.getIdForm());
		eaeUserForm.setIdUser(eaeUserFormData.getIdUser());
		eaeUserForm.setUserRole(eaeUserFormData.getUserRole());
		eaeUserForm.setIdManager(eaeUserFormData.getIdManager());
		eaeUserForm.setDtCreated(eaeUserFormData.getDtCreated());
		eaeUserForm.setDtHistorized(eaeUserFormData.getDtHistorized());
		eaeUserForm.setDtClosed(eaeUserFormData.getDtClosed());
		eaeUserForm.setDtOwnerValidated(eaeUserFormData.getDtOwnerValidated());
		eaeUserForm.setDtManagerValidated(eaeUserFormData.getDtManagerValidated());
		eaeUserForm.setDtOwnerSigned(eaeUserFormData.getDtOwnerSigned());
		eaeUserForm.setDtManagerSigned(eaeUserFormData.getDtManagerSigned());
		eaeUserForm.setStatus(eaeUserFormData.getStatus());
		eaeUserForm.setCompletion(eaeUserFormData.getCompletion());
		return eaeUserForm;
	}

	@Override
	public EaeUserFormComponentsBean mapComponentsUserForm(EaeUserFormRequestMapping eaeUserFormData)	throws EaeFormComponentException {
		EaeUserFormComponentsBean eaeComponents		= new EaeUserFormComponentsBean();
		List<EaeUserFormResponseBean> eaeUserResponses	= new ArrayList<EaeUserFormResponseBean>();
		
		if ( EvalSmartSecurity.checkUserFormEntries(eaeUserFormData) ) {
			EaeUserFormBean eaeUserForm		= mapDataUserForm(eaeUserFormData);
			try {
				// mapping user response data
				for (int i=0; i<eaeUserFormData.getEaeUserFormResponses().length; i++) {					
					if ( EvalSmartSecurity.checkUserResponseEntries(eaeUserFormData.getEaeUserFormResponses()[i]) ) {
						
						EaeUserFormResponseBean eaeUserResponse	= eaeUserFormResponseService.mapDataUserResponse(eaeUserFormData.getEaeUserFormResponses()[i]);						
						eaeUserResponses.add(eaeUserResponse); // store into responses provisional list
					}
				}
				// store in a general scope object
				eaeComponents.setEaeUserForm(eaeUserForm);
				eaeComponents.setEaeUserResponses(eaeUserResponses);
				
			} catch (EaeFormComponentException e) {
				throw new EaeFormComponentException ("error mapping user form object", e);
			}
		}
		return eaeComponents;
	}

}
