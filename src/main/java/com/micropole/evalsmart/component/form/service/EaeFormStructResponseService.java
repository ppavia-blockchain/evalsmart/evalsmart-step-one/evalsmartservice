package com.micropole.evalsmart.component.form.service;

import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructResponseBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructResponseLevelBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructResponseLevelRequestMapping;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructResponseRequestMapping;

public interface EaeFormStructResponseService {
	
	/** RETRIEVE FORM STRUCT RESPONSE DATA **/
	
	public List<EaeFormStructResponseBean> getAllFormStructResponsesByForm(String idForm) throws EaeFormComponentException;
	
	public List<EaeFormStructResponseBean> getAllFormStructResponsesByDomain(String idDomain) throws EaeFormComponentException;
	
	public List<EaeFormStructResponseBean> getAllFormStructResponsesByQuery(String idQuery) throws EaeFormComponentException;
	
	
	
	/** CREATE FORM STRUCT RESPONSE DATA **/
	
	public List<EaeFormStructResponseBean> addNewEaeFormStructResponses (List<EaeFormStructResponseBean> eaeFormResponses) throws EaeFormComponentException;
	
	
	
	/** UPDATE FORM STRUCT RESPONSE DATA **/
	
	public void changeLabel(String idResponse, String label) throws EaeFormComponentException;
	
	public void changeResponseType(String idResponse, String responseType) throws EaeFormComponentException;
	
	public void changeLevel(String idResponse, String level) throws EaeFormComponentException;
	
	public void changeContent(String idResponse, String content) throws EaeFormComponentException;
	
	
	/** MAP FORM STRUCT RESPONSE DATA **/
	
	public EaeFormStructResponseBean mapDataFormStructResponse (String idForm, String idDomain, String idQuery, EaeFormStructResponseRequestMapping eaeResponseData) throws EaeFormComponentException;
	
	public List<EaeFormStructResponseBean> mapAllDataResponses (String idForm, String idDomain, String idQuery, EaeFormStructResponseRequestMapping[] eaeResponsesData) throws EaeFormComponentException;
	
	public EaeFormStructResponseLevelBean mapDataFormStructResponseLevel(String idQuery, EaeFormStructResponseLevelRequestMapping eaeResponseLevelData);

}
