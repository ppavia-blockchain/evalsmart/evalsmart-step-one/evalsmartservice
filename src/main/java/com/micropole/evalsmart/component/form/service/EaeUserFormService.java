package com.micropole.evalsmart.component.form.service;

import java.util.Date;
import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormBean;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormComponentsBean;
import com.micropole.evalsmart.model.requestmapping.EaeUserFormRequestMapping;

public interface EaeUserFormService {
	
	/** RETRIEVE **/
	
	/**
	 * @description idForm & idUser identify the user form 
	 * @param idForm
	 * @param idUser
	 * @return
	 * @throws EaeFormComponentException
	 */
	public EaeUserFormBean getUserFormByIds (String idForm, String idUser) throws EaeFormComponentException;
	
	public List<EaeUserFormBean> getUserFormByStatus (List<String> status) throws EaeFormComponentException;
	
	public List<EaeUserFormBean> getUserFormsByManager (String idManager) throws EaeFormComponentException;
	
	public EaeUserFormBean getActiveUserFormByOwner (String idUser) throws EaeFormComponentException;
	
	/** MODIFY **/
	
	public void closeUserForm (String idForm, String idUser, Date date) throws EaeFormComponentException;
	
	public void changeUserRoleUserForm (String idForm, String idUser, String userRole) throws EaeFormComponentException;
	
	public void historizeUserForm (String idForm, String idUser, Date date) throws EaeFormComponentException;
	
	public void changeManagerUserForm (String idForm, String idUser, String idManager) throws EaeFormComponentException;
	
	public void validatedUserForm (String idForm, String idUser, String userRole, Date date) throws EaeFormComponentException;

	public void signeUserForm (String idForm, String idUser, String userRole, Date date) throws EaeFormComponentException;
	
	public void changeStatusUserForm (String idForm, String idUser, String status) throws EaeFormComponentException;
	
	public void modifyCompletionUserForm (String idForm, String idUser, Float completion) throws EaeFormComponentException;
	
	public void modifyDataUserForm (
			String id,
			String idInterview,
			String idForm,
			String idUser,
			String userRole,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String idManager,
			Date dtOwnerValidated,
			Date dtManagerValidated,
			Date dtOwnerSigned,
			Date dtManagerSigned,
			String status,
			Float completion
			) throws EaeFormComponentException;
	
	/** ADD **/
	
	public EaeUserFormBean addNewUserForm (
			String id,
			String idInterview,
			String idForm,
			String idUser,
			String userRole,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String idManager,
			Date dtOwnerValidated,
			Date dtManagerValidated,
			Date dtOwnerSigned,
			Date dtManagerSigned,
			String status,
			Float completion
			) throws EaeFormComponentException;
	
	/** MAP **/
	
	public EaeUserFormBean mapDataUserForm (EaeUserFormRequestMapping eaeUserData) throws EaeFormComponentException;
	
	public EaeUserFormComponentsBean mapComponentsUserForm (EaeUserFormRequestMapping eaeUserFormData) throws EaeFormComponentException;
}