package com.micropole.evalsmart.component.form.service;

import java.util.Date;
import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructComponentsBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructRequestMapping;

public interface EaeFormStructService {
	
	/** FORM STRUCTURE **/
	
	/** RETRIEVE **/
	
	public EaeFormStructBean getFormStructById (String idForm) throws EaeFormComponentException;
	
	public EaeFormStructBean getLastActiceFormStruct (List<String> activeStatus, Date since) throws EaeFormComponentException;
	
	/** CHECK **/
	
	/** ADD **/
	
	public EaeFormStructBean addNewFormComponents (EaeFormStructComponentsBean eaeComponents) throws EaeFormComponentException; 
	
	public EaeFormStructBean addNewForm (
			String idForm,
			String label,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String status
			) throws EaeFormComponentException;
	
	/** MODIFY **/
	
	public void modifyDataFormStruct (EaeFormStructBean eaeForm) throws EaeFormComponentException;
	
	public void modifyLabelFormStruct (String idForm, String label) throws EaeFormComponentException;
	
	public void modifyStatusFormStruct (String idForm, String status) throws EaeFormComponentException;
	
	public void closeFormStruct (String idForm, Date date) throws EaeFormComponentException;
	
	public void historizeFormStruct (String idForm, Date date) throws EaeFormComponentException;
	
	/** MAP **/
	
	public EaeFormStructBean mapDataFormStruct (EaeFormStructRequestMapping eaeFormData) throws EaeFormComponentException;	
	
	public EaeFormStructComponentsBean mapComponentsFormStruct (EaeFormStructRequestMapping eaeForm) throws EaeFormComponentException;
}