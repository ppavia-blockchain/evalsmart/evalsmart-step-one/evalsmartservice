package com.micropole.evalsmart.component.form.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructResponseBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructResponseLevelBean;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructResponseRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeFormStructResponseRepository;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructResponseLevelRequestMapping;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructResponseRequestMapping;
import com.micropole.evalsmart.utils.EvalSmartUtils;

public class EaeFormStructResponseServiceImpl implements EaeFormStructResponseService {
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructResponseServiceImpl.class);
	@Autowired
	private EaeFormStructResponseRepository eaeFormResponsesRepository;
	@Autowired
	private EaeCustomFormStructResponseRepository eaeCustomFormStructResponseRepository;
	
	
	/** RETRIEVE RESPONSE DATA **/

	@Override
	public List<EaeFormStructResponseBean> getAllFormStructResponsesByForm(String idForm) throws EaeFormComponentException {
		List<EaeFormStructResponseBean>	eaeResonses = new ArrayList<EaeFormStructResponseBean>();
		try {
			eaeResonses	= eaeCustomFormStructResponseRepository.findAllResponsesFromForm(idForm);
		}
		catch (EaeFormComponentException e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeResonses;
	}

	@Override
	public List<EaeFormStructResponseBean> getAllFormStructResponsesByDomain(String idDomain) throws EaeFormComponentException {
		List<EaeFormStructResponseBean>	eaeResonses = new ArrayList<EaeFormStructResponseBean>();
		try {
			eaeResonses	= eaeCustomFormStructResponseRepository.findAllResponsesFromDomain(idDomain);
		}
		catch (EaeFormComponentException e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeResonses;
	}

	@Override
	public List<EaeFormStructResponseBean> getAllFormStructResponsesByQuery(String idQuery) throws EaeFormComponentException {
		List<EaeFormStructResponseBean>	eaeResonses = new ArrayList<EaeFormStructResponseBean>();
		try {
			eaeResonses	= eaeCustomFormStructResponseRepository.findAllResponsesFromQuery(idQuery);
		}
		catch (EaeFormComponentException e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeResonses;
	}
	
	
	
	/** CREATE RESPONSE DATA **/
	
	@Override
	public List<EaeFormStructResponseBean> addNewEaeFormStructResponses(List<EaeFormStructResponseBean> eaeFormResponses) throws EaeFormComponentException {
		try {
			eaeFormResponses	= eaeFormResponsesRepository.saveAll(eaeFormResponses);
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("error responses queries", e);
		}
		return eaeFormResponses;
	}



	/** UPDATE RESPONSE DATA **/

	@Override
	public void changeLabel(String idResponse, String label) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructResponseRepository.updateLabel(idResponse, label);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void changeResponseType(String idResponse, String responseType) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructResponseRepository.updateResponseType(idResponse, responseType);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void changeLevel(String idResponse, String level) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructResponseRepository.updateLevel(idResponse, level);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void changeContent(String idResponse, String content) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructResponseRepository.updateContent(idResponse, content);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}
	
	
	
	/** MAP RESPONSE DATA **/

	@Override
	public EaeFormStructResponseBean mapDataFormStructResponse(String idForm, String idDomain, String idQuery, EaeFormStructResponseRequestMapping eaeResponseData) throws EaeFormComponentException {
		EaeFormStructResponseBean eaeResponse	= null;
		try {
			eaeResponse	= new EaeFormStructResponseBean();
			eaeResponse.setIdForm(idForm);
			eaeResponse.setIdDomain(idDomain);
			eaeResponse.setIdQuery(idQuery);
			String idResponse	= ( eaeResponseData.getIdResponse() == null ) ? EvalSmartUtils.generateId("response") : eaeResponseData.getIdResponse();
			eaeResponse.setIdResponse(idResponse);
			eaeResponse.setLabel(eaeResponseData.getLabel());
			eaeResponse.setResponseType(eaeResponseData.getResponseType());
			eaeResponse.setLevel(eaeResponseData.getLevel());
			eaeResponse.setContent(eaeResponseData.getContent());
		} catch (EaeFormComponentException e) {
			throw new EaeFormComponentException ("An error occured during mapping response data", e);
		}		
		return eaeResponse;
	}
	
	@Override
	public List<EaeFormStructResponseBean> mapAllDataResponses(String idForm, String idDomain, String idQuery, EaeFormStructResponseRequestMapping[] eaeResponsesData) throws EaeFormComponentException {
		List<EaeFormStructResponseBean> eaeResponses	= new ArrayList<EaeFormStructResponseBean>();
		try {
			EvalSmartUtils.convertArrayIntoList(eaeResponsesData).stream()
			.forEach((eaeResponseData) -> {
				EaeFormStructResponseBean eaeResponse = null;
				try {
					eaeResponse = mapDataFormStructResponse(idForm, idDomain, idQuery, eaeResponseData);
					eaeResponses.add(eaeResponse);
				} catch (EaeFormComponentException e) {
					logger.error("An error occured during mapping responses data", e);
				}
			});
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("An error occured during mapping responses data", e);
		}
		return eaeResponses;
	}

	@Override
	public EaeFormStructResponseLevelBean mapDataFormStructResponseLevel(String idDomain, EaeFormStructResponseLevelRequestMapping eaeResponseLevelData) {
		EaeFormStructResponseLevelBean eaeResponseLevel	= null;
		eaeResponseLevel	= new EaeFormStructResponseLevelBean();
		eaeResponseLevel.setIdDomain(idDomain);
		eaeResponseLevel.setLabel(eaeResponseLevelData.getLabel());
		eaeResponseLevel.setCode(eaeResponseLevelData.getCode());		
		return eaeResponseLevel;
	}
}
