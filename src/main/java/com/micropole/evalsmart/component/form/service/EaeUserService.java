package com.micropole.evalsmart.component.form.service;

import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeUserBean;

public interface EaeUserService {
	
	public boolean isUserAuthenticated () throws EaeFormComponentException;
	
	public EaeUserBean getUserById (String idUser) throws EaeFormComponentException;
	
	public EaeUserBean getUserByUserName (String userName) throws EaeFormComponentException;
	
	public EaeUserBean getUserByEmail (String email) throws EaeFormComponentException;
	
	public List<EaeUserBean> getUserByFullName (String fullName) throws EaeFormComponentException;
	
	public EaeUserBean addNewUser (
			String profile,
			String status,
			String firstName,
			String lastName,
			String userName,
			String email,
			String phone,
			List<String> account
	) throws EaeFormComponentException;
	
	public EaeUserBean updateUser (
			String idUser, 
			String profile,
			String status,
			String firstName, 
			String lastName,
			String userName,
			String email, 
			String phone, 
			List<String> account) throws EaeFormComponentException;
}
