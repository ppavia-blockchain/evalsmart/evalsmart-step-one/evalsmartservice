package com.micropole.evalsmart.component.form.service;

import java.util.Date;
import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormResponseBean;
import com.micropole.evalsmart.model.requestmapping.EaeUserResponseRequestMapping;

public interface EaeUserFormResponseService {
	
	/** RETRIEVE **/
	
	public EaeUserFormResponseBean getUserResponseByIds (String idResponse, String idForm, String idUser, String idOwner) throws EaeFormComponentException;
	
	public List<EaeUserFormResponseBean> getUserResponsesByForm (String idForm, String idUser) throws EaeFormComponentException;
	
	public List<EaeUserFormResponseBean> getUserResponsesOwnedByUser (String idForm, String idUser, String idOwner) throws EaeFormComponentException;
	
	/** MODIFY **/
	
	public void modifyContentUserResponse (String idResponse, String idForm, String idUser, String idOwner, String content) throws EaeFormComponentException;
	
	public void modifyDtCompletionUserResponse (String idResponse, String idForm, String idUser, String idOwner, Date dtCompletion) throws EaeFormComponentException;
	
	
	/** ADD **/
	
	public List<EaeUserFormResponseBean> addUserResponses (List<EaeUserFormResponseBean> eaeResponses) throws EaeFormComponentException;
	
	public EaeUserFormResponseBean addNewUserResponse (
			String id,
			String idForm,
			String idUser,
			String idDomain,
			String idQuery,
			String idResponse,
			String idOwner,
			String content,
			Date dtCompletion
			) throws EaeFormComponentException;
	
	/** REMOVE **/
	
	public void removeUserFormResponse (String idResponse, String idForm, String idUser, String idOwner) throws EaeFormComponentException;
	
	public void removeUserFormResponses (List<EaeUserFormResponseBean> removedUserResponses) throws EaeFormComponentException;
	
	/** MAP **/
	
	public List<EaeUserFormResponseBean> mapDataUserResponses (EaeUserResponseRequestMapping[] eaeUserResponsesData) throws EaeFormComponentException;
	
	public EaeUserFormResponseBean mapDataUserResponse (EaeUserResponseRequestMapping eaeUserResponseData) throws EaeFormComponentException;
}
