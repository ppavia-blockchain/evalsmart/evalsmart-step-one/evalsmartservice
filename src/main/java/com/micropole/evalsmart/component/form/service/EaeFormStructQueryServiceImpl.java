package com.micropole.evalsmart.component.form.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructQueryBean;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructQueryRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeFormStructQueryRepository;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructQueryRequestMapping;
import com.micropole.evalsmart.utils.EvalSmartUtils;

public class EaeFormStructQueryServiceImpl implements EaeFormStructQueryService {
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructQueryServiceImpl.class);
	@Autowired
	private EaeFormStructQueryRepository eaeFormStructQueryRepository;

	@Autowired 
	private EaeCustomFormStructQueryRepository eaeCustomFormStructQueryRepository;

	/** RETRIEVE QUERY DATA **/

	@Override
	public List<EaeFormStructQueryBean> getAllQueriesByForm(String idForm) throws EaeFormComponentException {
		List<EaeFormStructQueryBean>	eaeQueries = new ArrayList<EaeFormStructQueryBean>();
		try {
			eaeQueries	= eaeCustomFormStructQueryRepository.findAllQueriesFromForm(idForm);
		}
		catch (EaeFormComponentException e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeQueries;
	}


	@Override
	public List<EaeFormStructQueryBean> getAllQueriesByDomain(String idDomain) throws EaeFormComponentException {
		List<EaeFormStructQueryBean>	eaeQueries = new ArrayList<EaeFormStructQueryBean>();
		try {
			eaeQueries	= eaeCustomFormStructQueryRepository.findAllQueriesFromDomain(idDomain);
		}
		catch (EaeFormComponentException e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeQueries;
	}


	/** CHECK QUERY DATA **/

	@Override
	public boolean checkQueryCompleted(EaeFormStructQueryBean eaeQuery) {
		// TODO Auto-generated method stub
		return true;
	}



	/** CREATE QUERY DATA **/

	@Override
	public List<EaeFormStructQueryBean> addNewEaeFormQueries(List<EaeFormStructQueryBean> eaeFormQueries) throws EaeFormComponentException {

		try {
			eaeFormQueries	= eaeFormStructQueryRepository.saveAll(eaeFormQueries);
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("error creating queries", e);
		}
		return eaeFormQueries;
	}


	/** UPDATE QUERY DATA **/

	@Override
	public void changeLabel(String idQuery, String label) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructQueryRepository.updateLabel(idQuery, label);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}


	@Override
	public void changeQueryType(String idQuery, String queryType) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructQueryRepository.updateQueryType(idQuery, queryType);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}


	@Override
	public void changeStateCompletion(String idQuery, boolean isCompleted) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructQueryRepository.updateIsCompleted(idQuery, isCompleted);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}



	/** MAP QUERY DATA **/

	@Override
	public EaeFormStructQueryBean mapDataQuery(String idForm, String idDomain, EaeFormStructQueryRequestMapping eaeQueryData) throws EaeFormComponentException {
		EaeFormStructQueryBean eaeQuery	= null;
		try {
			eaeQuery	= new EaeFormStructQueryBean();
			eaeQuery.setIdForm(idForm);
			eaeQuery.setIdDomain(idDomain);
			String idQuery	= ( eaeQueryData.getIdQuery() == null ) ? EvalSmartUtils.generateId("query") : eaeQueryData.getIdQuery();
			eaeQuery.setIdQuery(idQuery);
			eaeQuery.setLabel(eaeQueryData.getLabel());
			eaeQuery.setQueryType(eaeQueryData.getQueryType());
			eaeQuery.setOrder(eaeQueryData.getOrder());

		} catch (EaeFormComponentException e) {
			throw new EaeFormComponentException ("An error occured during mapping query data", e);
		}
		return eaeQuery;
	}


	@Override
	public List<EaeFormStructQueryBean> mapAllDataQueries(String idForm, String idDomain, EaeFormStructQueryRequestMapping[] eaeQueriesData) throws EaeFormComponentException {
		List<EaeFormStructQueryBean> eaeQueries	= new ArrayList<EaeFormStructQueryBean>();
		try {
			EvalSmartUtils.convertArrayIntoList(eaeQueriesData).stream()
			.forEach((eaeQueryData) -> {
				EaeFormStructQueryBean eaeQuery = null;
				try {
					eaeQuery = mapDataQuery(idForm, idDomain, eaeQueryData);
					eaeQueries.add(eaeQuery);
				} catch (EaeFormComponentException e) {
					logger.error("An error occured during mapping queries data", e);
				}
			});
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("An error occured during mapping queries data", e);
		}
		return eaeQueries;
	}
	
}
