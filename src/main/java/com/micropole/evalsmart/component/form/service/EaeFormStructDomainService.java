package com.micropole.evalsmart.component.form.service;

import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructDomainBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructDomainRequestMapping;

public interface EaeFormStructDomainService {
	

	/** RETRIEVE DOMAIN DATA **/
	
	public List<EaeFormStructDomainBean> getAllDomainsByForm (String idForm) throws EaeFormComponentException;
	
	
	
	/** CHECK DOMAIN DATA **/
	
	public boolean checkDomainCompleted (EaeFormStructDomainBean eaeDomain);
	
	
	
	/** MAP DOMAIN DATA **/
	
	public EaeFormStructDomainBean mapDataDomain (String idForm, EaeFormStructDomainRequestMapping eaeDomainData) throws EaeFormComponentException;
	
	public List<EaeFormStructDomainBean> mapAllDataDomains (String idForm, EaeFormStructDomainRequestMapping[] eaeDomainData) throws EaeFormComponentException;
	
	
	
	/** CREATE DOMAIN DATA **/
	
	public List<EaeFormStructDomainBean> addNewFormStructDomains (List<EaeFormStructDomainBean> eaeFormDomains) throws EaeFormComponentException;
	
	
	
	/** UPDATE DOMAIN DATA **/
	
	public void changeLabel(String idDomain, String label) throws EaeFormComponentException;
}
