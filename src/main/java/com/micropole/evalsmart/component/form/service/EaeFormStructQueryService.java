package com.micropole.evalsmart.component.form.service;

import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructQueryBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructQueryRequestMapping;

public interface EaeFormStructQueryService {
	
	/** RETRIEVE QUERY DATA **/
	
	public List<EaeFormStructQueryBean> getAllQueriesByForm (String idForm) throws EaeFormComponentException;

	public List<EaeFormStructQueryBean> getAllQueriesByDomain (String idDomain) throws EaeFormComponentException;
	
	
	
	/** CHECK QUERY DATA **/

	public boolean checkQueryCompleted (EaeFormStructQueryBean eaeQuery);
	
	
	
	/** CREATE QUERY DATA **/
	
	public List<EaeFormStructQueryBean> addNewEaeFormQueries(List<EaeFormStructQueryBean> eaeFormQueries) throws EaeFormComponentException;
	
	
	/** UPDATE QUERY DATA **/
	
	public void changeLabel(String idQuery, String label) throws EaeFormComponentException;
	
	public void changeQueryType(String idQuery, String queryType) throws EaeFormComponentException;
	
	public void changeStateCompletion(String idQuery, boolean isCompleted) throws EaeFormComponentException;
	
	
	
	/** MAP QUERY DATA **/
	
	public EaeFormStructQueryBean mapDataQuery (String idForm, String idDomain, EaeFormStructQueryRequestMapping eaeQueryData) throws EaeFormComponentException;
	
	public List<EaeFormStructQueryBean> mapAllDataQueries (String idForm, String idDomain, EaeFormStructQueryRequestMapping[] eaeQueriesData) throws EaeFormComponentException;
}
