package com.micropole.evalsmart.component.form.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructComponentsBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructDomainBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructInfoBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructQueryBean;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructResponseBean;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeFormStructRepository;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructDomainRequestMapping;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructRequestMapping;
import com.micropole.evalsmart.utils.EvalSmartSecurity;
import com.micropole.evalsmart.utils.EvalSmartUtils;

public class EaeFormStructServiceImpl implements EaeFormStructService {
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructServiceImpl.class);

	@Autowired
	private EaeFormStructRepository eaeFormStructRepository;
	@Autowired
	private EaeCustomFormStructRepository eaeCustomFormStructRepository;
	
	@Autowired
	private EaeFormStructDomainService eaeFormStructDomainService;
	@Autowired
	private EaeFormStructInfoService eaeFormStructInfoService;
	@Autowired
	private EaeFormStructQueryService eaeFormStructQueryService;
	@Autowired
	private EaeFormStructResponseService eaeFormStructResponseService;


	public void doSerializeObjectToJson () {
	}
	
	/** FORM STRUCTURE **/
	
	/** RETRIEVE FORM DATA **/	

	@Override
	public EaeFormStructBean getFormStructById(String idForm) throws EaeFormComponentException {
		EaeFormStructBean eaeForm = null;
		try {
			eaeForm	= eaeFormStructRepository.findByIdForm(idForm);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("error getting form with id [" + idForm + "]", e);
		}
		return eaeForm;
	}
	
	/** CHECK FORM DATA **/
	
	@Override
	public EaeFormStructBean getLastActiceFormStruct(List<String> activeStatus, Date since) throws EaeFormComponentException {
		EaeFormStructBean eaeForm = null;
		try {
			eaeForm	= eaeCustomFormStructRepository.findLastActiveFormStruct(activeStatus, since);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("error getting last active form", e);
		}
		return eaeForm;
	}

	/** CREATE NEW FORM COMPONENTS **/

	@Override
	public EaeFormStructBean addNewFormComponents(EaeFormStructComponentsBean eaeComponents) throws EaeFormComponentException {
		EaeFormStructBean eaeForm = null;
		// save form			
		eaeForm	= addNewForm(
				eaeComponents.getEaeFormStruct().getIdForm(),
				eaeComponents.getEaeFormStruct().getLabel(),
				eaeComponents.getEaeFormStruct().getDtCreated(),
				eaeComponents.getEaeFormStruct().getDtClosed(),
				eaeComponents.getEaeFormStruct().getDtHistorized(),
				eaeComponents.getEaeFormStruct().getStatus());
		if ( eaeForm == null ) {
			// TODO - make the return db status
			throw new EaeFormComponentException ("error adding new form [" + eaeComponents.getEaeFormStruct().getIdForm() + "] - The store process was canceled");
		}
		// save domains form
		List<EaeFormStructDomainBean> eaeDomains = eaeFormStructDomainService.addNewFormStructDomains (eaeComponents.getEaeDomains());
		if ( eaeDomains == null ) {
			// TODO - make the return db status
			throw new EaeFormComponentException ("error adding domains from new form [" + eaeComponents.getEaeFormStruct().getIdForm() + "] - The store process was canceled");
		}
		// save queries form
		List<EaeFormStructQueryBean> eaeQueries = eaeFormStructQueryService.addNewEaeFormQueries(eaeComponents.getEaeQueries());
		if ( eaeQueries == null ) {
			// TODO - make the return db status
			throw new EaeFormComponentException ("error adding queries from new form [" + eaeComponents.getEaeFormStruct().getIdForm() + "] - The store process was canceled");
		}
		// save responses form
		List<EaeFormStructResponseBean> eaeResponses = eaeFormStructResponseService.addNewEaeFormStructResponses(eaeComponents.getEaeResponses());
		if ( eaeResponses == null ) {
			// TODO - make the return db status
			throw new EaeFormComponentException ("error adding responses from new form [" + eaeComponents.getEaeFormStruct().getIdForm() + "] - The store process was canceled");
		}

		return eaeForm;
	}
	
	@Override
	public EaeFormStructBean addNewForm(
			String idForm,
			String label, 
			Date dtCreated, 
			Date dtClosed,
			Date dtHitorized,
			String status) throws EaeFormComponentException {
		EaeFormStructBean eaeForm = null;
		try {
			eaeForm			= new EaeFormStructBean();
			eaeForm.setIdForm(idForm);
			eaeForm.setLabel(label);
			eaeForm.setDtCreated(dtCreated);
			eaeForm.setDtClosed(dtClosed);
			eaeForm.setDtHistorized(dtHitorized);
			eaeForm.setStatus(status);

			// save form
			eaeForm = eaeFormStructRepository.save(eaeForm);
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("error adding responses from new form [" + idForm + "]", e);
		}

		return eaeForm;
	}
	
	/** UPDATE FORM DATA **/
	
	@Override
	public void modifyDataFormStruct(EaeFormStructBean eaeForm) throws EaeFormComponentException {
		// TODO - check validation data
		eaeCustomFormStructRepository.updateDataFormStruct(
				eaeForm.getIdForm(),
				eaeForm.getLabel(),
				eaeForm.getDtCreated(),
				eaeForm.getDtClosed(),
				eaeForm.getDtHistorized(),
				eaeForm.getStatus());
	}

	@Override
	public void modifyLabelFormStruct(String idForm, String label) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructRepository.updateLabelFormStruct(idForm, label);			
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void modifyStatusFormStruct(String idForm, String status) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructRepository.updateStatusFormStruct(idForm, status);			
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void closeFormStruct(String idForm, Date date) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructRepository.updateDtClosedFormStruct(idForm, date);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void historizeFormStruct(String idForm, Date date) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructRepository.updateDtHistorizedFormStruct(idForm, date);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}
	
	/** MAP COMPONENTS DATA OF THE FORM **/

	@Override
	public EaeFormStructComponentsBean mapComponentsFormStruct (EaeFormStructRequestMapping eaeFormData) throws EaeFormComponentException {
		EaeFormStructComponentsBean eaeComponents		= new EaeFormStructComponentsBean();
		List<EaeFormStructDomainBean> eaeFormDomains		= new ArrayList<EaeFormStructDomainBean>();
		List<EaeFormStructInfoBean> eaeFormInfos			= new ArrayList<EaeFormStructInfoBean>();
		List<EaeFormStructQueryBean> eaeFormQueries			= new ArrayList<EaeFormStructQueryBean>();
		List<EaeFormStructResponseBean> eaeFormResponses	= new ArrayList<EaeFormStructResponseBean>();
		EaeFormStructDomainRequestMapping[] domains			= eaeFormData.getEaeFormDomains();

		if ( EvalSmartSecurity.checkFormEntries(eaeFormData) ) {
			EaeFormStructBean eaeForm	= mapDataFormStruct(eaeFormData);
			String idForm	= eaeForm.getIdForm();

			try {
				// mapping domains data
				for (int i=0; i<domains.length; i++) {
					if ( EvalSmartSecurity.checkDomainEntries(domains[i]) ) {
						EaeFormStructDomainBean eaeDomain	= eaeFormStructDomainService.mapDataDomain(idForm, domains[i]);
						// set levels iddomain
						eaeDomain.setLevels(eaeDomain.getLevels().stream()
								.map(level -> {
									level.setIdDomain(eaeDomain.getIdDomain());
									return level;
								})
								.collect(Collectors.toList()));
						// mapping infos data
						for (int j=0; j<domains[i].getEaeFormInfos().length; j++) {
							if ( EvalSmartSecurity.checkQueryEntries(domains[i].getEaeFormQueries()[j]) ) {
								EaeFormStructInfoBean eaeInfo	= eaeFormStructInfoService.mapDataInfo(idForm, eaeDomain.getIdDomain(), domains[i].getEaeFormInfos()[j]);
								eaeFormInfos.add(eaeInfo); // store into a infos provisional list
							}
						}
						// mapping queries and responses data
						for (int k=0; k<domains[i].getEaeFormQueries().length; k++) {
							if ( EvalSmartSecurity.checkQueryEntries(domains[i].getEaeFormQueries()[k]) ) {
								EaeFormStructQueryBean eaeQuery	= eaeFormStructQueryService.mapDataQuery(idForm, eaeDomain.getIdDomain(), domains[i].getEaeFormQueries()[k]);

								// mapping response data
								for (int l=0; k<domains[i].getEaeFormQueries()[k].getEaeFormResponses().length; l++) {
									if ( EvalSmartSecurity.checkResponseEntries(domains[i].getEaeFormQueries()[k].getEaeFormResponses()[l]) ) {
										EaeFormStructResponseBean eaeResponse	= eaeFormStructResponseService.mapDataFormStructResponse(idForm, eaeDomain.getIdDomain(), eaeQuery.getIdQuery(), domains[i].getEaeFormQueries()[k].getEaeFormResponses()[l]);
										eaeFormResponses.add(eaeResponse); // store into responses provisional list
									}
								}
								eaeFormQueries.add(eaeQuery); // store into a queries provisional list
							}
						}
						eaeFormDomains.add(eaeDomain); // store into domains provisional list

						// store in a general scope object
						eaeComponents.setEaeFormStruct(eaeForm);
						eaeComponents.setEaeDomains(eaeFormDomains);
						eaeComponents.setEaeInfos(eaeFormInfos);
						eaeComponents.setEaeQueries(eaeFormQueries);
						eaeComponents.setEaeResponses(eaeFormResponses);
					}
				}
			} catch (EaeFormComponentException e) {
				throw new EaeFormComponentException ("error generating id", e);
			}

		}
		return eaeComponents;
	}

	@Override
	public EaeFormStructBean mapDataFormStruct(EaeFormStructRequestMapping eaeFormData) throws EaeFormComponentException {
		EaeFormStructBean eaeForm	= null;
		try {
			eaeForm	= new EaeFormStructBean();
			String idForm	= ( eaeFormData.getIdForm() == null ) ? EvalSmartUtils.generateId("form") : eaeFormData.getIdForm();
			eaeForm.setIdForm(idForm);
			eaeForm.setLabel(eaeFormData.getLabel());
			eaeForm.setDtCreated(eaeFormData.getDtCreated());
			eaeForm.setDtClosed(eaeFormData.getDtClosed());
			eaeForm.setDtHistorized(eaeFormData.getDtHistorized());
			eaeForm.setStatus(eaeFormData.getStatus());
		} catch (EaeFormComponentException e) {
			throw new EaeFormComponentException ("An error occured during mapping form data", e);
		}
		return eaeForm;
	}
}
