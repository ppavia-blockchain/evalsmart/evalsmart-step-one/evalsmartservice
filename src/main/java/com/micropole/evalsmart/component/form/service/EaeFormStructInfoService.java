package com.micropole.evalsmart.component.form.service;

import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructInfoBean;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructInfoRequestMapping;

public interface EaeFormStructInfoService {
	
	/** RETRIEVE INFO **/
	public List<EaeFormStructInfoBean> getAllInfosByForm (String idForm) throws EaeFormComponentException;
	
	public List<EaeFormStructInfoBean> getAllInfosByDomain (String idDomain) throws EaeFormComponentException;
	
	public EaeFormStructInfoBean getInfoById (String idInfo) throws EaeFormComponentException;
	
	
	/** MODIFY INFO **/
	
	public void changeContentInfo (String idInfo, String content) throws EaeFormComponentException;
	
	public void changeOrderInfo (String idInfo, int order) throws EaeFormComponentException;
	
	
	/** ADD INFO **/
	
	public List<EaeFormStructInfoBean> addNewFormStructInfos (List<EaeFormStructInfoBean> eaeFormDomains) throws EaeFormComponentException;
	
	/** MAP INFO **/
	
	public EaeFormStructInfoBean mapDataInfo (String idForm, String idDomain, EaeFormStructInfoRequestMapping eaeInfoData) throws EaeFormComponentException;
	
	public List<EaeFormStructInfoBean> mapAllDataInfos (String idForm, String idDomain, EaeFormStructInfoRequestMapping[] eaeInfosData) throws EaeFormComponentException;

}
