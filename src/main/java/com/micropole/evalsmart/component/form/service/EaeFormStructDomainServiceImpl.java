package com.micropole.evalsmart.component.form.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructDomainBean;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructDomainRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeFormStructDomainRepository;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructDomainRequestMapping;
import com.micropole.evalsmart.utils.EvalSmartUtils;

public class EaeFormStructDomainServiceImpl implements EaeFormStructDomainService {
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructDomainServiceImpl.class);
	@Autowired
	private EaeFormStructDomainRepository eaeFormStructDomainRepository;
	@Autowired
	private EaeCustomFormStructDomainRepository eaeCustomFormStructDomainRepository;

	/** RETRIEVE DOMAIN DATA **/

	@Override
	public List<EaeFormStructDomainBean> getAllDomainsByForm(String idForm) throws EaeFormComponentException {
		List<EaeFormStructDomainBean>	eaeDomains = new ArrayList<EaeFormStructDomainBean>();
		try {
			eaeDomains	= eaeCustomFormStructDomainRepository.findAllDomainsFromForm(idForm);
		}
		catch (EaeFormComponentException e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeDomains;
	}	


	/** CHECK DOMAIN DATA **/

	@Override
	public boolean checkDomainCompleted(EaeFormStructDomainBean eaeDomain) {
		// TODO Auto-generated method stub
		return true;
	}



	/** MAP DOMAIN DATA **/

	@Override
	public EaeFormStructDomainBean mapDataDomain(String idForm, EaeFormStructDomainRequestMapping eaeDomainData) throws EaeFormComponentException {
		EaeFormStructDomainBean eaeDomain	= null;
		try {
			eaeDomain	= new EaeFormStructDomainBean ();
			eaeDomain.setIdForm(idForm);
			String idDomain	= ( eaeDomainData.getIdDomain() == null ) ? EvalSmartUtils.generateId("domain") : eaeDomainData.getIdDomain();
			eaeDomain.setIdDomain(idDomain);
			eaeDomain.setLabel(eaeDomainData.getLabel());
			eaeDomain.setOrder(eaeDomainData.getOrder());
			eaeDomain.setLevels(EvalSmartUtils.convertArrayIntoList(eaeDomainData.getLevels()));
		} catch (EaeFormComponentException e) {
			throw new EaeFormComponentException ("An error occured during mapping domain data", e);
		}
		return eaeDomain;
	}

	@Override
	public List<EaeFormStructDomainBean> mapAllDataDomains(String idForm, EaeFormStructDomainRequestMapping[] eaeDomainsData) throws EaeFormComponentException {
		List<EaeFormStructDomainBean> eaeDomains	= new ArrayList<EaeFormStructDomainBean>();
		try {
			EvalSmartUtils.convertArrayIntoList(eaeDomainsData).stream()
			.forEach((eaeDomainData) -> {
				EaeFormStructDomainBean eaeDomain;
				try {
					eaeDomain = mapDataDomain(idForm, eaeDomainData);
					eaeDomains.add(eaeDomain);
				} catch (EaeFormComponentException e) {
					logger.error("An error occured during mapping domain data", e);
				}
			});
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("An error occured during mapping domains data", e);
		}
		return eaeDomains;
	}


	/** CREATE DOMAIN DATA **/

	@Override
	public List<EaeFormStructDomainBean> addNewFormStructDomains(List<EaeFormStructDomainBean> eaeFormDomains) throws EaeFormComponentException {
		try {
			eaeFormDomains	= eaeFormStructDomainRepository.saveAll(eaeFormDomains);
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("error creating domains", e);
		}
		return eaeFormDomains;
	}



	/** UPDATE DOMAIN DATA **/

	@Override
	public void changeLabel(String idDomain, String label) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomFormStructDomainRepository.updateLabel(idDomain, label);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}
}
