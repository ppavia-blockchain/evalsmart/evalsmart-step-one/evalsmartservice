package com.micropole.evalsmart.component.form.service;

import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeStatusFormBean;

public interface EaeEnumService {
	
	public EaeStatusFormBean getStatusFormByCode (String statusCode) throws EaeFormComponentException;
	
	public List<EaeStatusFormBean> getAllStatusForm () throws EaeFormComponentException;
}
