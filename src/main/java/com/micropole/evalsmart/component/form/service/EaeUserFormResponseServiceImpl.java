package com.micropole.evalsmart.component.form.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormResponseBean;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomUserResponseRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeUserResponseRepository;
import com.micropole.evalsmart.model.requestmapping.EaeUserResponseRequestMapping;

public class EaeUserFormResponseServiceImpl implements EaeUserFormResponseService {
	private static final Logger logger = LoggerFactory.getLogger(EaeUserFormResponseServiceImpl.class);
	@Autowired
	private EaeUserResponseRepository eaeUserResponseRepository;
	@Autowired
	private EaeCustomUserResponseRepository eaeCustomUserResponseRepository;
	
	/** RETRIEVE **/
	
	@Override
	public EaeUserFormResponseBean getUserResponseByIds(String idResponse, String idForm, String idUser, String idOwner) throws EaeFormComponentException {
		EaeUserFormResponseBean eaeUserResponse = null;
		try {
			eaeUserResponse = eaeCustomUserResponseRepository.findUserResponseByIds(idResponse, idForm, idUser, idOwner);
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("error finding response [" + idResponse + ", " + idOwner + "]", e);
		}
		return eaeUserResponse; // can be null : not an error... just no user response found
	}

	@Override
	public List<EaeUserFormResponseBean> getUserResponsesByForm(String idForm, String idUser) throws EaeFormComponentException {
		List<EaeUserFormResponseBean> eaeUserResponses = null;
		try {
			eaeUserResponses = eaeCustomUserResponseRepository.findUserResponsesByForm(idForm, idUser);
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("error finding responses from form [" + idForm + "]", e);
		}
		return eaeUserResponses;
	}
	
	@Override
	public List<EaeUserFormResponseBean> getUserResponsesOwnedByUser(String idForm, String idUser, String idOwner) throws EaeFormComponentException {
		List<EaeUserFormResponseBean> eaeUserResponses = null;
		try {
			eaeUserResponses = eaeCustomUserResponseRepository.findUserResponsesOwnedByUser(idForm, idUser, idOwner);
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("error finding responses from form [" + idForm + "]", e);
		}
		return eaeUserResponses;
	}
	
	
	
	/** MODIFY **/

	@Override
	public void modifyContentUserResponse(String idResponse, String idForm, String idUser, String idOwner, String content) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomUserResponseRepository.updateContentUserResponseForm(idResponse, idForm, idUser, idOwner, content);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}
	
	@Override
	public void modifyDtCompletionUserResponse(String idResponse, String idForm, String idUser, String idOwner, Date dtCompletion) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeCustomUserResponseRepository.updateDtCompletionUserResponseForm(idResponse, idForm, idUser, idOwner, dtCompletion);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	/** ADD **/
	
	@Override
	public List<EaeUserFormResponseBean> addUserResponses(List<EaeUserFormResponseBean> userResponses) throws EaeFormComponentException {
		// TODO - check validation data
		try {
			eaeUserResponseRepository.saveAll(userResponses);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return userResponses;
	}

	@Override
	public EaeUserFormResponseBean addNewUserResponse(
			String id,
			String idForm,
			String idUser,
			String idDomain,
			String idQuery,
			String idResponse,
			String idOwner,
			String content,
			Date dtCompletion) throws EaeFormComponentException {
		EaeUserFormResponseBean eaeUserResponse	= null;
		
		try {
			eaeUserResponse			= new EaeUserFormResponseBean();
			eaeUserResponse.setId(id);
			eaeUserResponse.setIdForm(idForm);
			eaeUserResponse.setIdUser(idUser);
			eaeUserResponse.setIdDomain(idDomain);
			eaeUserResponse.setIdQuery(idQuery);
			eaeUserResponse.setIdResponse(idResponse);
			eaeUserResponse.setIdOwner(idOwner);
			eaeUserResponse.setContent(content);
			eaeUserResponse.setDtCompletion(dtCompletion);
			// save user response
			eaeUserResponse = eaeUserResponseRepository.save(eaeUserResponse); 
		}
		catch (Exception e) {
			StringBuilder msg	= new StringBuilder("");
			msg.append("[");
			msg.append("idResponse = ").append(idResponse).append(", ");
			msg.append("idForm = ").append(idForm).append(", ");
			msg.append("idUser = ").append(idUser).append(", ");
			msg.append("idOwner = ").append(idOwner);
			msg.append("]");
			throw new EaeFormComponentException ("error adding responses to user form " + msg, e);
		}
		
		return eaeUserResponse;
	}
	
	/** REMOVE **/
	@Override
	public void removeUserFormResponse (String idResponse, String idForm, String idUser, String idOwner) throws EaeFormComponentException {
		try {
			this.eaeCustomUserResponseRepository.removeUserFormResponse(idResponse, idForm, idUser, idOwner);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}
	

	@Override
	public void removeUserFormResponses(List<EaeUserFormResponseBean> removedUserResponses) throws EaeFormComponentException {
		try {
			this.eaeCustomUserResponseRepository.removeUserFormResponses(removedUserResponses);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	/** MAP **/
	
	@Override
	public List<EaeUserFormResponseBean> mapDataUserResponses(EaeUserResponseRequestMapping[] eaeUserResponsesData) throws EaeFormComponentException {
		List<EaeUserFormResponseBean> eaeUserResponses	= new ArrayList<EaeUserFormResponseBean>();
		try {
			for (int i=0; i<eaeUserResponsesData.length; i++) {
				EaeUserFormResponseBean eaeUserResponse	= mapDataUserResponse(eaeUserResponsesData[i]);
				eaeUserResponses.add(eaeUserResponse);
			}
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeUserResponses;
	}

	@Override
	public EaeUserFormResponseBean mapDataUserResponse(EaeUserResponseRequestMapping eaeUserResponseData) throws EaeFormComponentException {
		EaeUserFormResponseBean eaeUserResponse	= null;
		eaeUserResponse	= new EaeUserFormResponseBean();
		eaeUserResponse.setId(eaeUserResponseData.getId());
		eaeUserResponse.setIdForm(eaeUserResponseData.getIdForm());
		eaeUserResponse.setIdUser(eaeUserResponseData.getIdUser());
		eaeUserResponse.setIdDomain(eaeUserResponseData.getIdDomain());
		eaeUserResponse.setIdQuery(eaeUserResponseData.getIdQuery());
		eaeUserResponse.setIdResponse(eaeUserResponseData.getIdResponse());
		eaeUserResponse.setIdOwner(eaeUserResponseData.getIdOwner());
		eaeUserResponse.setDtCompletion(eaeUserResponseData.getDtCompletion());
		eaeUserResponse.setContent(eaeUserResponseData.getContent());
		return eaeUserResponse;
	}

}
