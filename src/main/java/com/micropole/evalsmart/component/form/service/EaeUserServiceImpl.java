package com.micropole.evalsmart.component.form.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeUserBean;
import com.micropole.evalsmart.model.component.form.repository.EaeFormUserRepository;
import com.micropole.evalsmart.utils.EvalSmartUtils;

public class EaeUserServiceImpl implements EaeUserService {

	@Autowired
	private EaeFormUserRepository eaeFormUserRepository;

	@Override
	public boolean isUserAuthenticated() throws EaeFormComponentException {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public EaeUserBean getUserById(String idUser) throws EaeFormComponentException {
		EaeUserBean eaeUser = null;
		try {
			eaeUser  = eaeFormUserRepository.findByIdUser(idUser);			
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeUser;
	}

	@Override
	public EaeUserBean getUserByEmail(String email) throws EaeFormComponentException {
		EaeUserBean eaeUser = null;
		try {
			email = email.replace(";", ".");
			eaeUser  = eaeFormUserRepository.findByEmail(email);			
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeUser;
	}

	@Override
	public EaeUserBean getUserByUserName(String userName) throws EaeFormComponentException {
		EaeUserBean eaeUser = null;
		try {
			eaeUser  = eaeFormUserRepository.findByUserName(userName);			
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeUser;
	}

	@Override
	public List<EaeUserBean> getUserByFullName(String fullName) throws EaeFormComponentException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EaeUserBean addNewUser(
			String profile,
			String status,
			String firstName, 
			String lastName, 
			String userName,
			String email, 
			String phone, 
			List<String> account) throws EaeFormComponentException {
		EaeUserBean eaeUser = null;
		try {
			// create user and store it in db
			eaeUser	= eaeFormUserRepository.save(new EaeUserBean(
					EvalSmartUtils.generateId("user"),
					profile,
					status,
					firstName, 
					lastName,
					userName,
					email,
					phone, 
					account	
					));			
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeUser;
	}

	@Override
	public EaeUserBean updateUser(
			String idUser, 
			String profile,
			String status,
			String firstName, 
			String lastName,
			String userName,
			String email, 
			String phone, 
			List<String> account) throws EaeFormComponentException {
		EaeUserBean eaeUser	= null;
		try {
			eaeUser	= getUserById(idUser);
			// update properties
			eaeUser.setProfile(profile);
			eaeUser.setStatus(status);
			eaeUser.setFirstName(firstName);
			eaeUser.setLastName(lastName);
			eaeUser.setUserName(userName);
			eaeUser.setEmail(email);
			eaeUser.setPhone(phone);
			eaeUser.setAccount(account);
			// save in database
			eaeUser	= eaeFormUserRepository.save(eaeUser);			
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeUser;
	}

}
