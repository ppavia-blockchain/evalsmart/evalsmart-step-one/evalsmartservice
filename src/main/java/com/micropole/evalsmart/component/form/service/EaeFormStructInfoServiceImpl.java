package com.micropole.evalsmart.component.form.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructInfoBean;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructInfoRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeFormStructInfoRepository;
import com.micropole.evalsmart.model.requestmapping.EaeFormStructInfoRequestMapping;
import com.micropole.evalsmart.utils.EvalSmartUtils;

public class EaeFormStructInfoServiceImpl implements EaeFormStructInfoService {
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructInfoServiceImpl.class);
	@Autowired
	private EaeFormStructInfoRepository eaeFormStructInfoRepository;
	@Autowired
	private EaeCustomFormStructInfoRepository eaeCustomFormStructInfoRepository;

	@Override
	public List<EaeFormStructInfoBean> getAllInfosByForm(String idForm) throws EaeFormComponentException {		
		List<EaeFormStructInfoBean>	eaeInfos = new ArrayList<EaeFormStructInfoBean>();
		try {
			eaeInfos	= eaeFormStructInfoRepository.findByIdForm(idForm);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeInfos;
	}

	@Override
	public List<EaeFormStructInfoBean> getAllInfosByDomain(String idDomain) throws EaeFormComponentException {
		List<EaeFormStructInfoBean>	eaeInfos = new ArrayList<EaeFormStructInfoBean>();
		try {
			eaeInfos	= eaeFormStructInfoRepository.findByIdDomain(idDomain);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeInfos;
	}

	@Override
	public EaeFormStructInfoBean getInfoById(String idInfo) throws EaeFormComponentException {
		EaeFormStructInfoBean	eaeInfo = null;
		try {
			eaeInfo	= eaeFormStructInfoRepository.findByIdInfo(idInfo);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return eaeInfo;
	}

	@Override
	public void changeContentInfo(String idInfo, String content) throws EaeFormComponentException {
		try {
			eaeCustomFormStructInfoRepository.updateContent(idInfo, content);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public void changeOrderInfo(String idInfo, int order) throws EaeFormComponentException {
		try {
			eaeCustomFormStructInfoRepository.updateOrder(idInfo, order);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
	}

	@Override
	public List<EaeFormStructInfoBean> addNewFormStructInfos(List<EaeFormStructInfoBean> eaeFormInfos) throws EaeFormComponentException {
		try {
			eaeFormInfos	= eaeFormStructInfoRepository.saveAll(eaeFormInfos);
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("error creating infos", e);
		}
		return eaeFormInfos;
	}

	@Override
	public EaeFormStructInfoBean mapDataInfo(String idForm, String idDomain, EaeFormStructInfoRequestMapping eaeInfoData) throws EaeFormComponentException {
		EaeFormStructInfoBean eaeInfo	= null;
		try {
			eaeInfo			= new EaeFormStructInfoBean();
			eaeInfo.setIdForm(idForm);
			eaeInfo.setIdDomain(idDomain);
			String idInfo	= ( eaeInfoData.getIdInfo() == null ) ? EvalSmartUtils.generateId("info") : eaeInfoData.getIdInfo();
			eaeInfo.setIdInfo(idInfo);
			eaeInfo.setLabel(eaeInfoData.getLabel());
			eaeInfo.setContent(eaeInfoData.getContent());
			eaeInfo.setOrder(eaeInfoData.getOrder());
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("An error occured during mapping info data", e);
		}
		return eaeInfo;
	}

	@Override
	public List<EaeFormStructInfoBean> mapAllDataInfos(String idForm, String idDomain, EaeFormStructInfoRequestMapping[] eaeInfosData) throws EaeFormComponentException {
		List<EaeFormStructInfoBean> eaeInfos	= new ArrayList<EaeFormStructInfoBean>();
		try {
			EvalSmartUtils.convertArrayIntoList(eaeInfosData).stream()
			.forEach((eaeInfoData) -> {
				EaeFormStructInfoBean eaeInfo = null;
				try {
					eaeInfo = mapDataInfo(idForm, idDomain, eaeInfoData);
					eaeInfos.add(eaeInfo);
				} catch (EaeFormComponentException e) {
					logger.error("An error occured during mapping infos data", e);
				}
			});
		}
		catch (Exception e) {
			throw new EaeFormComponentException ("An error occured during mapping infos data", e);
		}
		return eaeInfos;
	}

}
