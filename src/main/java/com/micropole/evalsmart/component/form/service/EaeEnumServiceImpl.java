package com.micropole.evalsmart.component.form.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeStatusFormBean;
import com.micropole.evalsmart.model.component.form.repository.EaeStatusFormRepository;

public class EaeEnumServiceImpl implements EaeEnumService {

	@Autowired
	private EaeStatusFormRepository eaeStatusFormRepository;

	@Override
	public EaeStatusFormBean getStatusFormByCode(String statusCode) throws EaeFormComponentException {
		EaeStatusFormBean statusForm = null;
		try {
			statusForm  = eaeStatusFormRepository.findByCode(statusCode);
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return statusForm;
	}

	@Override
	public List<EaeStatusFormBean> getAllStatusForm() throws EaeFormComponentException {
		List<EaeStatusFormBean> statusForm = null;
		try {
			statusForm  = eaeStatusFormRepository.findAll();
		}
		catch (Exception e) {
			throw new EaeFormComponentException (e.getMessage(), e);
		}
		return statusForm;
	}
}
