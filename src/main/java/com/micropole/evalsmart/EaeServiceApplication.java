package com.micropole.evalsmart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EaeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EaeServiceApplication.class, args);
	}
}
