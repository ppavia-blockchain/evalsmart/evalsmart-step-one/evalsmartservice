package com.micropole.evalsmart.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.micropole.evalsmart.component.form.service.EaeEnumService;
import com.micropole.evalsmart.component.form.service.EaeEnumServiceImpl;
import com.micropole.evalsmart.component.form.service.EaeFormStructDomainService;
import com.micropole.evalsmart.component.form.service.EaeFormStructDomainServiceImpl;
import com.micropole.evalsmart.component.form.service.EaeFormStructInfoService;
import com.micropole.evalsmart.component.form.service.EaeFormStructInfoServiceImpl;
import com.micropole.evalsmart.component.form.service.EaeFormStructQueryService;
import com.micropole.evalsmart.component.form.service.EaeFormStructQueryServiceImpl;
import com.micropole.evalsmart.component.form.service.EaeFormStructResponseService;
import com.micropole.evalsmart.component.form.service.EaeFormStructResponseServiceImpl;
import com.micropole.evalsmart.component.form.service.EaeFormStructService;
import com.micropole.evalsmart.component.form.service.EaeFormStructServiceImpl;
import com.micropole.evalsmart.component.form.service.EaeUserFormResponseService;
import com.micropole.evalsmart.component.form.service.EaeUserFormResponseServiceImpl;
import com.micropole.evalsmart.component.form.service.EaeUserFormService;
import com.micropole.evalsmart.component.form.service.EaeUserFormServiceImpl;
import com.micropole.evalsmart.component.form.service.EaeUserService;
import com.micropole.evalsmart.component.form.service.EaeUserServiceImpl;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructDomainRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructDomainRepositoryImpl;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructInfoRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructInfoRepositoryImpl;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructQueryRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructQueryRepositoryImpl;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructRepositoryImpl;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructResponseRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomFormStructResponseRepositoryImpl;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomUserFormRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomUserFormRepositoryImpl;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomUserResponseRepository;
import com.micropole.evalsmart.model.component.form.repository.EaeCustomUserResponseRepositoryImpl;
import com.mongodb.MongoClient;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class EaeMainConfiguration {
	private static final Logger logger = LoggerFactory.getLogger(EaeMainConfiguration.class);
	
	@Autowired
	private Environment env;
	
	@Bean
	public EaeFormStructService eaeFormStructService () {
		return new EaeFormStructServiceImpl();
	}
	
	@Bean
	public EaeFormStructDomainService eaeFormStructDomainService () {
		return new EaeFormStructDomainServiceImpl();
	}
	
	@Bean
	public EaeFormStructInfoService eaeFormStructInfoService () {
		return new EaeFormStructInfoServiceImpl();
	}
	
	@Bean
	public EaeFormStructQueryService eaeFormStructQueryService () {
		return new EaeFormStructQueryServiceImpl();
	}
	
	@Bean
	public EaeFormStructResponseService eaeFormStructResponseService () {
		return new EaeFormStructResponseServiceImpl();
	}
	
	@Bean
	public EaeUserFormService eaeUserFormService () {
		return new EaeUserFormServiceImpl();
	}
	
	@Bean
	public EaeUserFormResponseService eaeUserFormResponseService () {
		return new EaeUserFormResponseServiceImpl();
	}
	
	@Bean
	public EaeUserService eaeFormUserService () {
		return new EaeUserServiceImpl();
	}
	
	@Bean
	public EaeEnumService eaeEnumService() {
		return new EaeEnumServiceImpl();
	}

	@Bean
	public MongoClient mongoClient() {
		if ( logger.isDebugEnabled() ) {
			logger.debug("mongodb.host" + " = " + env.getProperty("mongodb.host"));
			logger.debug("mongodb.port" + " = " + env.getProperty("mongodb.port"));
			logger.debug("mongodb.name" + " = " + env.getProperty("mongodb.name"));
			logger.debug("mongodb.address" + " = " + env.getProperty("mongodb.address"));
		}
		return new MongoClient(env.getProperty("mongodb.host"), Integer.parseInt(env.getProperty("mongodb.port")));
	}

	@Bean
	public MongoTemplate mongoTemplate() {
		return new MongoTemplate(mongoClient(), env.getProperty("mongodb.name"));
	}
	
	@Bean
	public EaeCustomFormStructRepository eaeCustomRepository() {
		return new EaeCustomFormStructRepositoryImpl();
	}
	
	@Bean
	public EaeCustomFormStructDomainRepository eaeCustomFormStructDomainRepository() {
		return new EaeCustomFormStructDomainRepositoryImpl();
	}
	
	@Bean
	public EaeCustomFormStructInfoRepository eaeCustomFormStructInfoRepository() {
		return new EaeCustomFormStructInfoRepositoryImpl();
	}
	
	@Bean
	public EaeCustomFormStructQueryRepository eaeCustomFormStructQueryRepository() {
		return new EaeCustomFormStructQueryRepositoryImpl();
	}
	
	@Bean
	public EaeCustomFormStructResponseRepository eaeCustomFormStructResponseRepository() {
		return new EaeCustomFormStructResponseRepositoryImpl();
	}
	
	@Bean
	public EaeCustomUserFormRepository eaeCustomUserFormRepository () {
		return new EaeCustomUserFormRepositoryImpl();
	}
	
	@Bean
	public EaeCustomUserResponseRepository eaeCustomUserResponseRepository () {
		return new EaeCustomUserResponseRepositoryImpl();
	}

	/*
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any())
				.build();
	}
	*/
}
