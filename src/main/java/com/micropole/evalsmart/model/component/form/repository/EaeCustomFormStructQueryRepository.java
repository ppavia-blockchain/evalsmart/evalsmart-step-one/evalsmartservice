package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructQueryBean;

public interface EaeCustomFormStructQueryRepository {
	
	/** RETRIEVE DATA QUERY **/
	
	public List<EaeFormStructQueryBean> findAllQueriesFromForm (String idForm) throws EaeFormComponentException;
	
	public List<EaeFormStructQueryBean> findAllQueriesFromDomain (String idDomain) throws EaeFormComponentException;
	
	
	
	/** UPDATE DATA QUERY **/
	
	public void updateLabel (String idQuery, String label) throws EaeFormComponentException;
	
	public void updateQueryType (String idQuery, String queryType) throws EaeFormComponentException;
	
	public void updateIsCompleted (String idQuery, boolean isCompleted) throws EaeFormComponentException;
}
