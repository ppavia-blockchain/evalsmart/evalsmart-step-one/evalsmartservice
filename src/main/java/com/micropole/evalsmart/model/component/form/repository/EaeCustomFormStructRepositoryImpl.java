package com.micropole.evalsmart.model.component.form.repository;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructBean;
import com.mongodb.client.result.UpdateResult;

public class EaeCustomFormStructRepositoryImpl implements EaeCustomFormStructRepository {
	private static final Logger logger = LoggerFactory.getLogger(EaeCustomFormStructRepositoryImpl.class);
	@Autowired
	private MongoTemplate mongoTemplate;

	/** FORM STRUCTURE **/
	
	@Override
	public EaeFormStructBean findLastActiveFormStruct(List<String> activeStatus, Date since) throws EaeFormComponentException {
		Query query 					= new Query();
		Criteria criteria				= new Criteria();
		List<EaeFormStructBean> eaeForms	= null;
		
		criteria.andOperator(
				Criteria.where("status").in(activeStatus),
				Criteria.where("dtCreated").gte(since)
				);
		query.addCriteria(criteria);
		try {
			eaeForms	= mongoTemplate.find(query, EaeFormStructBean.class);			
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding last active form\n", e);
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findLastActiveFormStruct" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeForms.get(0);
	}
	
	@Override
	public void updateDataFormStruct (
			String idForm,
			String label,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String status
			) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		EaeFormStructBean	eaeForm				= null;
		// find form
		query.addCriteria(Criteria.where("idForm").is(idForm));
		eaeForm		= mongoTemplate.findOne(query, EaeFormStructBean.class);
		if ( eaeForm == null ) {
			throw new EaeFormComponentException ("Error in retrieving form with id : [" + idForm + "]");
		}
		// update only different values
		if ( !label.equals(eaeForm.getLabel()) ) {
			update.set("label", label);
		}
		if ( dtCreated != eaeForm.getDtCreated() ) {
			update.set("dtCreated", dtCreated);		
		}
		if ( dtClosed != eaeForm.getDtClosed() ) {
			update.set("dtClosed", dtClosed);		
		}
		if ( dtHistorized != eaeForm.getDtHistorized() ) {
			update.set("dtHistorized", dtHistorized);		
		}
		if ( !status.equals(eaeForm.getStatus()) ) {
			update.set("status", status);		
		}
		try {
			mongoTemplate.updateFirst(query, update, EaeFormStructBean.class);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating form with id : [" + idForm + "]\n", e);
		}
	}

	@Override
	public void updateLabelFormStruct(String idForm, String label) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idForm").is(idForm));
		update.set("label", label);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateLabelFormStruct" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateLabelFormStruct" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated label");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating label form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating label form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateStatusFormStruct(String idForm, String status) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idForm").is(idForm));
		update.set("status", status);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateStatusForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateStatusForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated status");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating status form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating status form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateDtClosedFormStruct(String idForm, Date date) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idForm").is(idForm));
		update.set("dtClosed", date);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateDtClosedForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateDtClosedForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated dtClosed");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating dtClosed form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating dtClosed form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateDtHistorizedFormStruct(String idForm, Date date) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idForm").is(idForm));
		update.set("dtHistorized", date);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateDtHistorizedForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateDtHistorizedForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated dtHistorized");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating dtHistorized form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating dtHistorized form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}	
}
