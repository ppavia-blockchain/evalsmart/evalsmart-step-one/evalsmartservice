package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.micropole.evalsmart.model.component.form.bean.EaeUserFormResponseBean;

public interface EaeUserResponseRepository extends MongoRepository<EaeUserFormResponseBean, String> {
	public List<EaeUserFormResponseBean> findByIdOwner (String idOwner);
	
	public List<EaeUserFormResponseBean> findByIdForm (String idForm);
}