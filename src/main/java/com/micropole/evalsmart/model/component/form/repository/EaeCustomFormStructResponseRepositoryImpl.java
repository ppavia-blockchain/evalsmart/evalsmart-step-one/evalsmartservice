package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructResponseBean;
import com.mongodb.client.result.UpdateResult;

public class EaeCustomFormStructResponseRepositoryImpl implements EaeCustomFormStructResponseRepository {
	private static final Logger logger = LoggerFactory.getLogger(EaeCustomFormStructResponseRepositoryImpl.class);
	@Autowired
	private MongoTemplate mongoTemplate;

	/** RETRIEVE DATA RESPONSE **/

	@Override
	public List<EaeFormStructResponseBean> findAllResponsesFromForm(String idForm) throws EaeFormComponentException {
		Query query 								= new Query();
		List<EaeFormStructResponseBean> eaeResponses		= null;
		query.with(new Sort(Sort.Direction.ASC, "order"));
		
		query.addCriteria(Criteria.where("idForm").is(idForm));
		try {
			eaeResponses	= mongoTemplate.find(query, EaeFormStructResponseBean.class);			
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding responses from form : [" + idForm + "]\n", e);
		}
		
		if ( eaeResponses.size() == 0 ) {
			throw new EaeFormComponentException ("Error : No response was founded from the form : [" + idForm + "]\n");
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findAllResponsesFromForm" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeResponses;
	}

	@Override
	public List<EaeFormStructResponseBean> findAllResponsesFromDomain(String idDomain) throws EaeFormComponentException {
		Query query 								= new Query();
		List<EaeFormStructResponseBean> eaeResponses		= null;
		query.with(new Sort(Sort.Direction.ASC, "order"));
		
		query.addCriteria(Criteria.where("idDomain").is(idDomain));
		try {
			eaeResponses	= mongoTemplate.find(query, EaeFormStructResponseBean.class);			
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding responses from domain : [" + idDomain + "]\n", e);
		}
		
		if ( eaeResponses.size() == 0 ) {
			throw new EaeFormComponentException ("Error : No response was founded from the domain : [" + idDomain + "]\n");
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findAllResponsesFromForm" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeResponses;
	}

	@Override
	public List<EaeFormStructResponseBean> findAllResponsesFromQuery(String idQuery) throws EaeFormComponentException {
		Query query 								= new Query();
		List<EaeFormStructResponseBean> eaeResponses		= null;
		query.with(new Sort(Sort.Direction.ASC, "order"));
		
		query.addCriteria(Criteria.where("idQuery").is(idQuery));
		try {
			eaeResponses	= mongoTemplate.find(query, EaeFormStructResponseBean.class);			
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding responses from query : [" + idQuery + "]\n", e);
		}
		
		if ( eaeResponses.size() == 0 ) {
			throw new EaeFormComponentException ("Error : No response was founded from the query : [" + idQuery + "]\n");
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findAllResponsesFromForm" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeResponses;
	}



	/** UPDATE RESPONSE DATA **/

	@Override
	public void updateLabel(String idResponse, String label) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idResponse").is(idResponse));
		update.set("label", label);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructResponseBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching response with id [" + idResponse + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated label");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating label response :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating label response : [" + idResponse + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateResponseType(String idResponse, String responseType) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idResponse").is(idResponse));
		update.set("responseType", responseType);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructResponseBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateResponseType" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateResponseType" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching response with id [" + idResponse + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated responseType");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating responseType :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating responseType : [" + idResponse + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateLevel(String idResponse, String level) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idResponse").is(idResponse));
		update.set("level", level);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructResponseBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateResponseType" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateResponseType" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching response with id [" + idResponse + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated level");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating level for the response :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating level for the response : [" + idResponse + "]\n" + e.getMessage(), e);
		}
	}
	
	@Override
	public void updateContent(String idResponse, String content) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idResponse").is(idResponse));
		update.set("content", content);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructResponseBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateResponseType" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateResponseType" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching response with id [" + idResponse + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated content");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating content for the response :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating content for the response : [" + idResponse + "]\n" + e.getMessage(), e);
		}
	}
}
