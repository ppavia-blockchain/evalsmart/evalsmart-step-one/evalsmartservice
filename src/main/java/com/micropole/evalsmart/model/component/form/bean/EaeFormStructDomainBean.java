package com.micropole.evalsmart.model.component.form.bean;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "eaeFormStructDomain")
public class EaeFormStructDomainBean implements Serializable {
	/**
	 * 
	 */
	@Transient
	private static final long serialVersionUID = 611083596981530997L;
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructDomainBean.class);
	@Transient
	private static String nameClass	= EaeFormStructDomainBean.class.getSimpleName();
	
	@Id
	private String id;
	private String idDomain;
	private String label;
	private String idForm;
	private int order;
	private List<EaeFormStructResponseLevelBean> levels;

	public EaeFormStructDomainBean () {}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdDomain() {
		return idDomain;
	}

	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public List<EaeFormStructResponseLevelBean> getLevels() {
		return levels;
	}

	public void setLevels(List<EaeFormStructResponseLevelBean> levels) {
		this.levels = levels;
	}


	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "id" + "=" + "%s" + "\n"
				+ "idDomain" + "=" + "%s" + "\n"
				+ "label" + "=" + "%s" + "\n"
				+ "idForm" + "=" + "%s" + "\n"
				+ "order" + "=" + "%s" + "\n"
				+ "levels" + "=" + "%s" + "\n",
				this.id,
				this.idDomain,
				this.label,
				this.idForm,
				this.order,
				this.levels.toString()
		);
	}
}
