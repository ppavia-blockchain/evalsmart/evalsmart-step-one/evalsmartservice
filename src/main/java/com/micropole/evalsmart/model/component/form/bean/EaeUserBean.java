package com.micropole.evalsmart.model.component.form.bean;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.micropole.evalsmart.utils.EvalSmartUtils;

/**
 * @author PPAVIA
 *
 */
@Document(collection = "eaeUser")
public class EaeUserBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID 		= 8439715666419074953L;
	private static final String nameClass			= EaeFormStructBean.class.getSimpleName();
	private static final String FORMAT_FULLNAME		= "UPPER_CASE";
	@Id
	private String id;
	private String idUser;
	private String status;
	private String profile;
	private String firstName;
	private String lastName;
	private String fullName;
	private String userName;
	private String email;
	private String phone;
	private List<String> account;
	
	public EaeUserBean () {};
	
	public EaeUserBean (
			String idUser,
			String profile,
			String status,
			String firstName, 
			String lastName,
			String userName,
			String email, 
			String phone, 
			List<String> account
	) {
		this.idUser		= idUser;
		this.status		= status;
		this.profile	= profile;
		this.firstName	= firstName;
		this.lastName	= lastName;
		this.fullName	= EvalSmartUtils.setFullName (firstName, lastName, FORMAT_FULLNAME);
		this.userName	= userName;
		this.email		= email;
		this.phone		= phone;
		this.account	= account;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public List<String> getAccount() {
		return account;
	}
	public void setAccount(List<String> account) {
		this.account = account;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "id" + "=" + "%s" + "\n"
				+ "idUser" + "=" + "%s" + "\n"
				+ "profile" + "=" + "%s" + "\n"
				+ "status" + "=" + "%s" + "\n"
				+ "firstName" + "=" + "%s" + "\n"
				+ "lastName" + "=" + "%s" + "\n"
				+ "fullName" + "=" + "%s" + "\n"
				+ "userName" + "=" + "%s" + "\n"
				+ "email" + "=" + "%s" + "\n"
				+ "phone" + "=" + "%s" + "\n"
				+ "account" + "=" + "%s" + "\n",
				this.id,
				this.idUser,
				this.profile,
				this.status,
				this.firstName,
				this.lastName,
				this.fullName,
				this.userName,
				this.email,
				this.phone,
				this.account
				);
	}
}
