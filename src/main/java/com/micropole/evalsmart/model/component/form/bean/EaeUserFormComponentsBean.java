package com.micropole.evalsmart.model.component.form.bean;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Transient;

public class EaeUserFormComponentsBean {
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeUserFormComponentsBean.class);
	@Transient
	private static String nameClass	= EaeUserFormComponentsBean.class.getSimpleName();
	
	private EaeUserFormBean eaeUserForm						= null;
	private List<EaeUserFormResponseBean> eaeUserResponses		= new ArrayList<EaeUserFormResponseBean>();
	
	public EaeUserFormBean getEaeUserForm() {
		return eaeUserForm;
	}
	public void setEaeUserForm(EaeUserFormBean eaeUserForm) {
		this.eaeUserForm = eaeUserForm;
	}
	public List<EaeUserFormResponseBean> getEaeUserResponses() {
		return eaeUserResponses;
	}
	public void setEaeUserResponses(List<EaeUserFormResponseBean> eaeUserResponses) {
		this.eaeUserResponses = eaeUserResponses;
	}
	
	@Override
	public String toString() {
		StringBuilder responses	= new StringBuilder("");
		this.eaeUserResponses.stream().forEach(userResponse -> {
			responses.append(userResponse.toString()).append(",");
		});
		responses.append("-------------------\n");
		
		return String.format(
				nameClass + "\n"
				+ "eaeUserForm" + "=" + "%s" + "\n"
				+ "eaeUserResponses" + "=" + "%s" + "\n",
				this.eaeUserForm.toString(),
				responses.toString()
				);
	}
}
