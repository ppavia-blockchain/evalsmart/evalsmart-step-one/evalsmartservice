package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.micropole.evalsmart.model.component.form.bean.EaeFormStructQueryBean;

public interface EaeFormStructQueryRepository extends MongoRepository<EaeFormStructQueryBean, String> {
	public List<EaeFormStructQueryBean> findByIdDomain (String idDomain);
	
	public List<EaeFormStructQueryBean> findByIdForm (String idForm);
}
