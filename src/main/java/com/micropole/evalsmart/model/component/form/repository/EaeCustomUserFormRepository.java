package com.micropole.evalsmart.model.component.form.repository;

import java.util.Date;
import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormBean;

public interface EaeCustomUserFormRepository {
	
	/** GET **/
	
	/**
	 * Find user form by ids (idForm = idForm of the form struct, idUser = id of the eaeUser)
	 * @param idForm
	 * @param idUser
	 * @return
	 * @throws EaeFormComponentException
	 */
	public EaeUserFormBean findUserForm (String idForm, String idUser) throws EaeFormComponentException;
	
	/**
	 * Find user forms with the status specified by list status 
	 * @param status
	 * @return
	 * @throws EaeFormComponentException
	 */
	public List<EaeUserFormBean> findUserFormByStatus (List<String> status) throws EaeFormComponentException;
	
	/**
	 * Find user forms by manager
	 * @param idManager
	 * @return
	 * @throws EaeFormComponentException
	 */
	public List<EaeUserFormBean> findUserFormsByManager(String idManager) throws EaeFormComponentException;
	
	/**
	 * Find user forms owned by user
	 * @param idUser
	 * @return
	 * @throws EaeFormComponentException
	 */
	public EaeUserFormBean findActiveUserFormByUser (String idUser) throws EaeFormComponentException;
	
	/** UPDATE **/
	
	/**
	 * Update the closing date of a user form
	 * @param idForm
	 * @param idUser
	 * @param date
	 * @throws EaeFormComponentException
	 */
	public void updateDtClosedUserForm (String idForm, String idUser, Date date) throws EaeFormComponentException;
	
	/**
	 * Update the user role of a user form
	 * @param idForm
	 * @param idUser
	 * @param userRole
	 * @throws EaeFormComponentException
	 */
	public void updateUserRoleUserForm (String idForm, String idUser, String userRole) throws EaeFormComponentException;
	
	/**
	 * Update the historizing date of a user form
	 * @param idForm
	 * @param idUser
	 * @param date
	 * @throws EaeFormComponentException
	 */
	public void updateDtHistorizedUserForm (String idForm, String idUser, Date date) throws EaeFormComponentException;
	
	/**
	 * Update the manager of a user form
	 * @param idForm
	 * @param idUser
	 * @param idManager
	 * @throws EaeFormComponentException
	 */
	public void updateIdManagerUserForm (String idForm, String idUser, String idManager) throws EaeFormComponentException;
	
	/**
	 * Update the validating user date of a user form
	 * @param idForm
	 * @param idUser
	 * @param date
	 * @throws EaeFormComponentException
	 */
	public void updateDtOwnerValidatedUserForm (String idForm, String idUser, Date date) throws EaeFormComponentException;

	/**
	 * Update the validating manager date of a user form
	 * @param idForm
	 * @param idUser
	 * @param date
	 * @throws EaeFormComponentException
	 */
	public void updateDtManagerValidatedUserForm (String idForm, String idUser, Date date) throws EaeFormComponentException;

	/**
	 * Update the signing user date of a user form
	 * @param idForm
	 * @param idUser
	 * @param date
	 * @throws EaeFormComponentException
	 */
	public void updateDtOwnerSignedUserForm (String idForm, String idUser, Date date) throws EaeFormComponentException;

	/**
	 * Update the signing manager date of a user form
	 * @param idForm
	 * @param idUser
	 * @param date
	 * @throws EaeFormComponentException
	 */
	public void updateDtManagerSignedUserForm (String idForm, String idUser, Date date) throws EaeFormComponentException;
	
	/**
	 * Update the status of a user form
	 * @param idForm
	 * @param idUser
	 * @param status
	 * @throws EaeFormComponentException
	 */
	public void updateStatusUserForm (String idForm, String idUser, String status) throws EaeFormComponentException;
	
	/**
	 * Update the completion date of a user form
	 * @param idForm
	 * @param idUser
	 * @param completion
	 * @throws EaeFormComponentException
	 */
	public void updateCompletionUserForm (String idForm, String idUser, Float completion) throws EaeFormComponentException;
	
	/**
	 * Update all data of a user form
	 * @param idForm
	 * @param idUser
	 * @param userRole
	 * @param dtCreated
	 * @param dtClosed
	 * @param dtHistorized
	 * @param idManager
	 * @param dtOwnerValidated
	 * @param dtManagerValidated
	 * @param dtOwnerSigned
	 * @param dtManagerSigned
	 * @param status
	 * @param completion
	 * @throws EaeFormComponentException
	 */
	public void updateDataUserForm (
			String id,
			String idInterview,
			String idForm,
			String idUser,
			String userRole,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String idManager,
			Date dtOwnerValidated,
			Date dtManagerValidated,
			Date dtOwnerSigned,
			Date dtManagerSigned,
			String status,
			Float completion
			) throws EaeFormComponentException;
	
	/** CREATE **/
	
	// use EaeUserFormRepository instead
	
}
