package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructDomainBean;
import com.mongodb.client.result.UpdateResult;

public class EaeCustomFormStructDomainRepositoryImpl implements EaeCustomFormStructDomainRepository {
	private static final Logger logger = LoggerFactory.getLogger(EaeCustomFormStructDomainRepositoryImpl.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;
	@Override
	public List<EaeFormStructDomainBean> findAllDomainsFromForm(String idForm) throws EaeFormComponentException {
		Query query 								= new Query();
		List<EaeFormStructDomainBean> eaeDomains			= null;
		query.with(new Sort(Sort.Direction.ASC, "order"));
		query.addCriteria(Criteria.where("idForm").is(idForm));
		try {
			eaeDomains	= mongoTemplate.find(query, EaeFormStructDomainBean.class);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding domains from form : [" + idForm + "]\n", e);
		}
		
		if ( eaeDomains.size() == 0 ) {
			throw new EaeFormComponentException ("Error : No domains was founded from the form : [" + idForm + "]\n");
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findAllResponsesFromForm" + "]" + "query" + " : " + query.toString());
		}
		return eaeDomains;
	}

	@Override
	public void updateLabel(String idDomain, String label) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idDomain").is(idDomain));
		update.set("label", label);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructDomainBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching domain with id [" + idDomain + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated label");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating label for the domain :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating label for the domain : [" + idDomain + "]\n" + e.getMessage(), e);
		}
	}

}
