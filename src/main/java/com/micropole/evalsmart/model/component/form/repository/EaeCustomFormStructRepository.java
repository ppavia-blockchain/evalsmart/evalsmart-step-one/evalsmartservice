package com.micropole.evalsmart.model.component.form.repository;

import java.util.Date;
import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructBean;

public interface EaeCustomFormStructRepository {
	
	/** FORM STRUCTURE **/
	
	public EaeFormStructBean findLastActiveFormStruct (List<String> activeStatus, Date since) throws EaeFormComponentException;
	
	/**
	 * Update label of a form struct
	 * @param idForm
	 * @param label
	 * @throws EaeFormComponentException
	 */
	public void updateLabelFormStruct (String idForm, String label) throws EaeFormComponentException;
	
	/**
	 * Update status of a form struct
	 * @param idForm
	 * @param status
	 * @throws EaeFormComponentException
	 */
	public void updateStatusFormStruct (String idForm, String status) throws EaeFormComponentException;
	
	/**
	 * Update the closing date of a form struct
	 * @param idForm
	 * @param date
	 * @throws EaeFormComponentException
	 */
	public void updateDtClosedFormStruct (String idForm, Date date) throws EaeFormComponentException;
	
	/**
	 * Update historizing date of a form struct
	 * @param idForm
	 * @param date
	 * @throws EaeFormComponentException
	 */
	public void updateDtHistorizedFormStruct (String idForm, Date date) throws EaeFormComponentException;
	
	/**
	 * Update all data of a form struct
	 * @param idForm
	 * @param label
	 * @param dtCreated
	 * @param dtClosed
	 * @param dtHistorized
	 * @param status
	 * @throws EaeFormComponentException
	 */
	public void updateDataFormStruct (
			String idForm,
			String label,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String status
			) throws EaeFormComponentException;
	
	
	/** USER FORM **/
}
