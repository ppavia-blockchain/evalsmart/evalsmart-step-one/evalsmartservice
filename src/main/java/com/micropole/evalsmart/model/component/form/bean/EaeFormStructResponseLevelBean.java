package com.micropole.evalsmart.model.component.form.bean;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Transient;

public class EaeFormStructResponseLevelBean implements Serializable {
	/**
	 * 
	 */
	@Transient
	private static final long serialVersionUID = 9147250129249847474L;
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructResponseLevelBean.class);
	@Transient
	private static String nameClass	= EaeFormStructResponseLevelBean.class.getSimpleName();
	
	private String idDomain;
	private String label;
	private String code;
	
	public String getIdDomain() {
		return idDomain;
	}
	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "idDomain" + "=" + "%s" + "\n"
				+ "label" + "=" + "%s" + "\n"
				+ "code" + "=" + "%s" + "\n",
				this.idDomain,
				this.label,
				this.code
				);
	}
	
}
