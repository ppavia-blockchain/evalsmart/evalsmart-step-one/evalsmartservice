package com.micropole.evalsmart.model.component.form.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormResponseBean;
import com.mongodb.client.result.UpdateResult;

public class EaeCustomUserResponseRepositoryImpl implements EaeCustomUserResponseRepository {
	private static final Logger logger = LoggerFactory.getLogger(EaeCustomUserResponseRepositoryImpl.class);
	@Autowired
	private MongoTemplate mongoTemplate;
	

	@Override
	public List<EaeUserFormResponseBean> findUserResponsesByForm(String idForm, String idOwner) throws EaeFormComponentException {
		Query query 					= new Query();
		Criteria criteria				= new Criteria();
		List<EaeUserFormResponseBean> eaeResponses	= null;
		criteria.andOperator(
				Criteria.where("idForm").in(idForm),
				Criteria.where("idOwner").is(idOwner)
				);
		query.addCriteria(criteria);
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idOwner = ").append(idOwner);
		msg.append("]");
		try {
			eaeResponses	= mongoTemplate.find(query, EaeUserFormResponseBean.class);			
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding responses with ids " + msg + "\n", e);
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findUserResponseByIds" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeResponses;
	}
	
	@Override
	public EaeUserFormResponseBean findUserResponseByIds(String idResponse, String idForm, String idUser, String idOwner) throws EaeFormComponentException {
		Query query 					= new Query();
		Criteria criteria				= new Criteria();
		List<EaeUserFormResponseBean> eaeResponses	= new ArrayList<EaeUserFormResponseBean>();
		criteria.andOperator(
				Criteria.where("idResponse").is(idResponse),
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser),
				Criteria.where("idOwner").is(idOwner)
				);
		query.addCriteria(criteria);
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idResponse = ").append(idResponse).append(", ");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idUser = ").append(idUser).append(", ");
		msg.append("idOwner = ").append(idOwner);
		msg.append("]");
		try {
			eaeResponses	= mongoTemplate.find(query, EaeUserFormResponseBean.class);
			if ( eaeResponses.size() == 0 ) { // no user response found... is not an error
				return null;
			}
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding response with ids " + msg + "\n", e);
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findUserResponseByIds" + "]" + "query" + " : " + query.toString());
		}
		return eaeResponses.get(0);
	}

	@Override
	public List<EaeUserFormResponseBean> findUserResponsesOwnedByUser(String idForm, String idUser, String idOwner) throws EaeFormComponentException {
		Query query 					= new Query();
		Criteria criteria				= new Criteria();
		List<EaeUserFormResponseBean> eaeResponses	= null;
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser),
				Criteria.where("idOwner").is(idOwner)
				);
		query.addCriteria(criteria);
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idUser = ").append(idUser).append(idForm).append(", ");
		msg.append("idOwner = ").append(idOwner);
		msg.append("]");
		try {
			eaeResponses	= mongoTemplate.find(query, EaeUserFormResponseBean.class);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding response with ids " + msg + "\n", e);
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findUserResponseByIds" + "]" + "query" + " : " + query.toString());
		}
		return eaeResponses;
	}

	/** REMOVE **/
	@Override
	public void removeUserFormResponse(String idResponse, String idForm, String idUser, String idOwner) throws EaeFormComponentException {
		EaeUserFormResponseBean eaeUserFormResponse	= this.findUserResponseByIds(idResponse, idForm, idUser, idOwner);
		if ( eaeUserFormResponse != null ) { // something has been returned
			mongoTemplate.remove(eaeUserFormResponse);
		}
	}
	
	@Override
	public void removeUserFormResponses(List<EaeUserFormResponseBean> removedUserResponses) throws EaeFormComponentException {
		removedUserResponses.forEach(removedResponse -> {
			try {
				this.removeUserFormResponse(removedResponse.getIdResponse(), removedResponse.getIdForm(), removedResponse.getIdUser(), removedResponse.getIdOwner());
			} catch (EaeFormComponentException e) {
				StringBuilder msg	= new StringBuilder("");
				msg.append("[");
				msg.append("idResponse = ").append(removedResponse.getIdResponse()).append(", ");
				msg.append("idForm = ").append(removedResponse.getIdForm()).append(", ");
				msg.append("idUser = ").append(removedResponse.getIdUser()).append(", ");
				msg.append("idOwner = ").append(removedResponse.getIdOwner());
				msg.append("]");
				logger.error("unable to remove response with id " + msg.toString());
			}
		});
		
	}
	
	@Override
	public void updateContentUserResponseForm(String idResponse, String idForm, String idUser, String idOwner, String content) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idResponse").is(idResponse),
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser),
				Criteria.where("idOwner").is(idOwner)
				);
		query.addCriteria(criteria);
		update.set("content", content);
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idResponse = ").append(idResponse).append(", ");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idUser = ").append(idUser).append(", ");
		msg.append("idOwner = ").append(idOwner);
		msg.append("]");
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormResponseBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateContentUserResponse" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateContentUserResponse" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching user response with id " + msg + "");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("content has not been updated");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating content response :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating content response : " + msg + "\n" + e.getMessage(), e);
		}
	}
	
	


	@Override
	public void updateDtCompletionUserResponseForm(String idResponse, String idForm, String idUser, String idOwner, Date dtCompletion) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idResponse").is(idResponse),
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser),
				Criteria.where("idOwner").is(idOwner)
				);
		query.addCriteria(criteria);
		update.set("dtCompletion", dtCompletion);
		StringBuilder msg	= new StringBuilder("");
		msg.append("[");
		msg.append("idResponse = ").append(idResponse).append(", ");
		msg.append("idForm = ").append(idForm).append(", ");
		msg.append("idUser = ").append(idUser).append(", ");
		msg.append("idOwner = ").append(idOwner);
		msg.append("]");
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormResponseBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateDtCompletionUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateDtCompletionUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching user response with ids " + msg + "");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("dtCompletion has not been updated");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating dtCompletion response :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating dtCompletion response : " + msg + "\n" + e.getMessage(), e);
		}
	}
}
