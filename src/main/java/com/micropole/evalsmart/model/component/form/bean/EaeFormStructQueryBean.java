package com.micropole.evalsmart.model.component.form.bean;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "eaeFormStructQuery")
public class EaeFormStructQueryBean implements Serializable {
	/**
	 * 
	 */
	@Transient
	private static final long serialVersionUID = -452276796203001673L;
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructQueryBean.class);
	@Transient
	private static String nameClass	= EaeFormStructQueryBean.class.getSimpleName();
	
	@Id
	private String id;
	private String idQuery;
	private String idDomain;
	private String idForm;
	private String label;
	private String queryType;
	private int order;
	
	public EaeFormStructQueryBean () {}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getIdQuery() {
		return idQuery;
	}

	public void setIdQuery(String idQuery) {
		this.idQuery = idQuery;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public String getIdDomain() {
		return idDomain;
	}

	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "id" + "=" + "%s" + "\n"
				+ "idQuery" + "=" + "%s" + "\n"
				+ "idDomain" + "=" + "%s" + "\n"
				+ "idForm" + "=" + "%s" + "\n"
				+ "label" + "=" + "%s" + "\n"
				+ "queryType" + "=" + "%s" + "\n"
				+ "order" + "=" + "%d" + "\n",
				this.id,
				this.idQuery,
				this.idDomain,
				this.idForm,
		        this.label,
		        this.queryType,
		        this.order
				);
	}
}
