package com.micropole.evalsmart.model.component.form.bean;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "eaeFormStructResponse")
public class EaeFormStructResponseBean implements Serializable {
	/**
	 * 
	 */
	@Transient
	private static final long serialVersionUID = 5955301935937418098L;
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructResponseBean.class);
	@Transient
	private static String nameClass	= EaeFormStructResponseBean.class.getSimpleName();
	
	@Id
	private String id;
	private String idResponse;
	private String label;
	private String idQuery;
	private String idDomain;
	private String idForm;
	private String responseType;
	private String content;
	private String level;
	
	public EaeFormStructResponseBean () {}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getIdResponse() {
		return idResponse;
	}

	public void setIdResponse(String idResponse) {
		this.idResponse = idResponse;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getIdQuery() {
		return idQuery;
	}

	public void setIdQuery(String idQuery) {
		this.idQuery = idQuery;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getIdDomain() {
		return idDomain;
	}

	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "id" + "=" + "%s" + "\n"
				+ "idResponse" + "=" + "%s" + "\n"
				+ "idQuery" + "=" + "%s" + "\n"
				+ "idDomain" + "=" + "%s" + "\n"
				+ "idForm" + "=" + "%s" + "\n"
				+ "label" + "=" + "%s" + "\n"
				+ "responseType" + "=" + "%s" + "\n"
				+ "content" + "=" + "%s" + "\n"
				+ "level" + "=" + "%s" + "\n",
				this.id,
				this.idResponse,
				this.idQuery,
				this.idDomain,
				this.idForm,
				this.label,
				this.responseType,
				this.content,
				this.level
				);
	}
}
