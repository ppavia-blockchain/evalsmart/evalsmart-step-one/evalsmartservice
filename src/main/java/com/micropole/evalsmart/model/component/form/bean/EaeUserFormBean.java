package com.micropole.evalsmart.model.component.form.bean;

import java.io.Serializable;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author PPAVIA
 *
 */
@Document(collection = "eaeUserForm")
public class EaeUserFormBean implements Serializable {
	/**
	 * 
	 */
	@Transient
	private static final long serialVersionUID = -921894353942946954L;
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeUserFormBean.class);
	@Transient
	private static String nameClass	= EaeUserFormBean.class.getSimpleName();
	@Id
	private String id;
	private String idInterview;
	private String idForm;
	private String idUser;
	private String userRole;
	private Date dtCreated;
	private Date dtClosed;
	private Date dtHistorized;
	private String idManager;
	private Date dtOwnerValidated;
	private Date dtManagerValidated;
	private Date dtOwnerSigned;
	private Date dtManagerSigned;
	private String status;
	private float completion;
	
	public EaeUserFormBean () {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdInterview() {
		return idInterview;
	}

	public void setIdInterview(String idInterview) {
		this.idInterview = idInterview;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public Date getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(Date dtCreated) {
		this.dtCreated = dtCreated;
	}

	public Date getDtClosed() {
		return dtClosed;
	}

	public void setDtClosed(Date dtClosed) {
		this.dtClosed = dtClosed;
	}

	public Date getDtHistorized() {
		return dtHistorized;
	}

	public void setDtHistorized(Date dtHistorized) {
		this.dtHistorized = dtHistorized;
	}

	public String getIdManager() {
		return idManager;
	}

	public void setIdManager(String idManager) {
		this.idManager = idManager;
	}

	public Date getDtOwnerValidated() {
		return dtOwnerValidated;
	}

	public void setDtOwnerValidated(Date dtOwnerValidated) {
		this.dtOwnerValidated = dtOwnerValidated;
	}

	public Date getDtManagerValidated() {
		return dtManagerValidated;
	}

	public void setDtManagerValidated(Date dtManagerValidated) {
		this.dtManagerValidated = dtManagerValidated;
	}

	public Date getDtOwnerSigned() {
		return dtOwnerSigned;
	}

	public void setDtOwnerSigned(Date dtOwnerSigned) {
		this.dtOwnerSigned = dtOwnerSigned;
	}

	public Date getDtManagerSigned() {
		return dtManagerSigned;
	}

	public void setDtManagerSigned(Date dtManagerSigned) {
		this.dtManagerSigned = dtManagerSigned;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public float getCompletion() {
		return completion;
	}

	public void setCompletion(float completion) {
		this.completion = completion;
	}

	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "id" + "=" + "%s" + "\n"
				+ "idForm" + "=" + "%s" + "\n"
				+ "idUser" + "=" + "%s" + "\n"
				+ "userRole" + "=" + "%s" + "\n"
				+ "dtCreated" + "=" + "%s" + "\n"
				+ "dtClosed" + "=" + "%s" + "\n"
				+ "dtHistorized" + "=" + "%s" + "\n"
				+ "idManager" + "=" + "%s" + "\n"
				+ "dtOwnerValidated" + "=" + "%s" + "\n"
				+ "dtManagerValidated" + "=" + "%s" + "\n"
				+ "dtOwnerSigned" + "=" + "%s" + "\n"
				+ "dtManagerSigned" + "=" + "%s" + "\n"
				+ "status" + "=" + "%s" + "\n"
				+ "completion" + "=" + "%d" + "\n",
				this.id,
				this.idForm,
				this.idUser,
				this.userRole,
				this.dtCreated,
				this.dtClosed,
				this.dtHistorized,
				this.idManager,
				this.dtOwnerValidated,
				this.dtManagerValidated,
				this.dtOwnerSigned,
				this.dtManagerSigned,
				this.status,
				this.completion
				);
	}
}
