package com.micropole.evalsmart.model.component.form.bean;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "eaeFormStatus")
public class EaeStatusFormBean implements Serializable {

	@Transient
	private static final long serialVersionUID = -4549210443515238206L;
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeStatusFormBean.class);
	@Transient
	private static String nameClass	= EaeStatusFormBean.class.getSimpleName();
	
	private String label;
	private String code;
	

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "label" + "=" + "%s" + "\n"
				+ "code" + "=" + "%s" + "\n",
				this.label,
				this.code
				);
	}
	
}
