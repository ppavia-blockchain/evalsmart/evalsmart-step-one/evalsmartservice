package com.micropole.evalsmart.model.component.form.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructInfoBean;
import com.mongodb.client.result.UpdateResult;

public class EaeCustomFormStructInfoRepositoryImpl implements EaeCustomFormStructInfoRepository {
	private static final Logger logger = LoggerFactory.getLogger(EaeCustomFormStructInfoRepositoryImpl.class);
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public void updateLabel(String idInfo, String label) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.with(new Sort(Sort.Direction.ASC, "order"));
		query.addCriteria(Criteria.where("idInfo").is(idInfo));
		update.set("label", label);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructInfoBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching info with id [" + idInfo + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated label");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating label for the info :[" + idInfo + "]\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating label for the info : [" + idInfo + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateContent(String idInfo, String content) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idInfo").is(idInfo));
		update.set("content", content);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructInfoBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching info with id [" + idInfo + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated content");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating content for the info :[" + idInfo + "]\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating content for the info : [" + idInfo + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateOrder(String idInfo, int order) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idInfo").is(idInfo));
		update.set("order", order);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructInfoBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching info with id [" + idInfo + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated order");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating order for the info :[" + idInfo + "]\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating order for the info : [" + idInfo + "]\n" + e.getMessage(), e);
		}
	}

}
