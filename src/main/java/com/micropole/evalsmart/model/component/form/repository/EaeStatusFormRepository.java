package com.micropole.evalsmart.model.component.form.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.micropole.evalsmart.model.component.form.bean.EaeStatusFormBean;

public interface EaeStatusFormRepository extends MongoRepository<EaeStatusFormBean, String>  {
	/** RETRIEVE DATA STATUS FORM **/
	public EaeStatusFormBean findByCode(String statusCode);
}
