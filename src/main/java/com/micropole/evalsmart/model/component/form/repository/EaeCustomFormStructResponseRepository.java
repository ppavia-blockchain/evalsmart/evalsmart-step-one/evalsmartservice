package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructResponseBean;

public interface EaeCustomFormStructResponseRepository {
	
	/** RETRIEVE DATA RESPONSE **/
	
	/**
	 * Find all form stuct responses of a form
	 * @param idForm
	 * @return
	 * @throws EaeFormComponentException
	 */
	public List<EaeFormStructResponseBean> findAllResponsesFromForm (String idForm) throws EaeFormComponentException;
	
	/**
	 * Find all form stuct responses of a domain
	 * @param idDomain
	 * @return
	 * @throws EaeFormComponentException
	 */
	public List<EaeFormStructResponseBean> findAllResponsesFromDomain (String idDomain) throws EaeFormComponentException;
	
	/**
	 * Find all form stuct responses of a query
	 * @param idQuery
	 * @return
	 * @throws EaeFormComponentException
	 */
	public List<EaeFormStructResponseBean> findAllResponsesFromQuery (String idQuery) throws EaeFormComponentException;
	
	
	
	/** UPDATE DATA RESPONSE **/
	
	/**
	 * Update label of a form stuct response
	 * @param idResponse
	 * @param label
	 * @throws EaeFormComponentException
	 */
	public void updateLabel (String idResponse, String label) throws EaeFormComponentException;
	
	/**
	 * Update response type of a form stuct response
	 * @param idResponse
	 * @param responseType
	 * @throws EaeFormComponentException
	 */
	public void updateResponseType (String idResponse, String responseType) throws EaeFormComponentException;
	
	/**
	 * Update level of a form stuct response
	 * @param idResponse
	 * @param level
	 * @throws EaeFormComponentException
	 */
	public void updateLevel (String idResponse, String level) throws EaeFormComponentException;
	
	/**
	 * Update content of a form stuct response
	 * @param idResponse
	 * @param content
	 * @throws EaeFormComponentException
	 */
	public void updateContent(String idResponse, String content) throws EaeFormComponentException;
	
}
