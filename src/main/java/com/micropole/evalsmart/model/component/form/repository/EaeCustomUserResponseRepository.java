package com.micropole.evalsmart.model.component.form.repository;

import java.util.Date;
import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormResponseBean;

public interface EaeCustomUserResponseRepository {
	
	/** GET **/
	
	/**
	 * Find the response from a specific user form owned by a user
	 * @param idResponse
	 * @param idForm
	 * @param idUser
	 * @param idOwner
	 * @return
	 * @throws EaeFormComponentException
	 */
	public EaeUserFormResponseBean findUserResponseByIds (String idResponse, String idForm, String idUser, String idOwner) throws EaeFormComponentException;
	
	/**
	 * Find all responses from a specific user form
	 * @param idForm
	 * @param idUser
	 * @return
	 * @throws EaeFormComponentException
	 */
	public List<EaeUserFormResponseBean> findUserResponsesByForm (String idForm, String idUser) throws EaeFormComponentException;
	
	/**
	 * Find all responses from a specific user form owned by a user
	 * @param idForm
	 * @param idUser
	 * @param idOwner
	 * @return
	 * @throws EaeFormComponentException
	 */
	public List<EaeUserFormResponseBean> findUserResponsesOwnedByUser (String idForm, String idUser, String idOwner) throws EaeFormComponentException;
	
	/** UPDATE **/
	
	/**
	 * Update the content of a user form response
	 * @param idResponse
	 * @param content
	 * @throws EaeFormComponentException
	 */
	public void updateContentUserResponseForm (String idResponse, String idForm, String idUser, String idOwner, String content) throws EaeFormComponentException;
	
	/**
	 * Set the completion date of a user form response
	 * @param idResponse
	 * @throws EaeFormComponentException
	 */
	public void updateDtCompletionUserResponseForm (String idResponse, String idForm, String idUser, String idOwner, Date dtCompletion) throws EaeFormComponentException;
	
	
	/** CREATE **/
	
	// use EaeUserResponseRepository instead
	
	/** REMOVE **/
	
	public void removeUserFormResponse (String idResponse, String idForm, String idUser, String idOwner) throws EaeFormComponentException;
	
	public void removeUserFormResponses (List<EaeUserFormResponseBean> removedUserResponses) throws EaeFormComponentException;
}
