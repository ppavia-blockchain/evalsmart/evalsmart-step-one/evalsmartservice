package com.micropole.evalsmart.model.component.form.bean;

import java.io.Serializable;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "eaeUserFormResponse")
public class EaeUserFormResponseBean implements Serializable {
	/**
	 * 
	 */
	@Transient
	private static final long serialVersionUID = -7018042541284530905L;	
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeUserFormBean.class);
	@Transient
	private static String nameClass	= EaeUserFormBean.class.getSimpleName();
	@Id
	private String id;
	private String idForm;
	private String idUser; // the user of the form
	private String idDomain;
	private String idQuery;
	private String idResponse;
	private String idOwner; // the owner of the response
	private String content;
	private Date dtCompletion;
	
	public EaeUserFormResponseBean() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getIdDomain() {
		return idDomain;
	}

	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}

	public String getIdQuery() {
		return idQuery;
	}

	public void setIdQuery(String idQuery) {
		this.idQuery = idQuery;
	}

	public String getIdResponse() {
		return idResponse;
	}

	public void setIdResponse(String idResponse) {
		this.idResponse = idResponse;
	}

	public String getIdOwner() {
		return idOwner;
	}

	public void setIdOwner(String idOwner) {
		this.idOwner = idOwner;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDtCompletion() {
		return dtCompletion;
	}

	public void setDtCompletion(Date dtCompletion) {
		this.dtCompletion = dtCompletion;
	}
}
