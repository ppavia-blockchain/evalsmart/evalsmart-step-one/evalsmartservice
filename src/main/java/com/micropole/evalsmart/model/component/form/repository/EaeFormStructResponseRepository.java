package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.micropole.evalsmart.model.component.form.bean.EaeFormStructResponseBean;

public interface EaeFormStructResponseRepository extends MongoRepository<EaeFormStructResponseBean, String> {
	public List<EaeFormStructResponseBean> findByIdQuery (String idQuery);
}
