package com.micropole.evalsmart.model.component.form.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.EaeStatusFormEnum;
import com.micropole.evalsmart.model.component.form.bean.EaeUserFormBean;
import com.mongodb.client.result.UpdateResult;

public class EaeCustomUserFormRepositoryImpl implements EaeCustomUserFormRepository {
	private static final Logger logger = LoggerFactory.getLogger(EaeCustomUserFormRepositoryImpl.class);
	@Autowired
	private MongoTemplate mongoTemplate;

	/** GET **/
	
	@Override
	public EaeUserFormBean findUserForm(String idForm, String idUser) throws EaeFormComponentException {
		Query query 					= new Query();
		Criteria criteria				= new Criteria();
		List<EaeUserFormBean> eaeForms	= null;
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		try {
			eaeForms	= mongoTemplate.find(query, EaeUserFormBean.class);			
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding form belonging to : [" + idUser + "]\n", e);
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findUserForm" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeForms.get(0);
	}
	
	@Override
	public List<EaeUserFormBean> findUserFormsByManager(String idManager) throws EaeFormComponentException {
		Query query 					= new Query();
		Criteria criteria				= new Criteria();
		List<EaeUserFormBean> eaeForms	= null;
		criteria.andOperator(
				Criteria.where("idManager").in(idManager)
				);
		query.addCriteria(criteria);
		try {
			eaeForms	= mongoTemplate.find(query, EaeUserFormBean.class);			
		}
		catch ( Exception e ) {
			StringBuilder sb	= new StringBuilder("");
			sb.append("An error occured finding forms for the manager with id : [");
			sb.append(idManager);
			sb.append("]\n");			
			throw new EaeFormComponentException (sb.toString(), e);
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findUserFormByManager" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeForms;
	}

	@Override
	public List<EaeUserFormBean> findUserFormByStatus(List<String> status) throws EaeFormComponentException {
		Query query 					= new Query();
		Criteria criteria				= new Criteria();
		List<EaeUserFormBean> eaeForms	= null;
		criteria.andOperator(
				Criteria.where("status").in(status)
				);
		query.addCriteria(criteria);
		try {
			eaeForms	= mongoTemplate.find(query, EaeUserFormBean.class);			
		}
		catch ( Exception e ) {
			StringBuilder sb	= new StringBuilder("");
			sb.append("An error occured finding forms with status : [");
			status.stream().forEach(st -> {
				sb.append(st).append(",");
			});
			sb.append("]\n");			
			throw new EaeFormComponentException (sb.toString(), e);
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findUserFormByStatus" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeForms;
	}

	@Override
	public EaeUserFormBean findActiveUserFormByUser(String idUser) throws EaeFormComponentException {
		Query query 					= new Query();
		Criteria criteria				= new Criteria();
		List<EaeUserFormBean> eaeForms		= null;
		List<String> status	= new ArrayList<String>();
		status.add(EaeStatusFormEnum.getCode("created"));
		status.add(EaeStatusFormEnum.getCode("validated"));
		status.add(EaeStatusFormEnum.getCode("signed"));
		
		criteria.andOperator(
				Criteria.where("idUser").is(idUser),
				Criteria.where("status").in(status)
				);
		query.addCriteria(criteria);
		try {
			// TODO - EaeUserFormBean instead EaeFormStructBean
			eaeForms	= mongoTemplate.find(query, EaeUserFormBean.class);			
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding form belonging to : [" + idUser + "]\n", e);
		}
		
		if ( eaeForms.size() > 1 ) {
			throw new EaeFormComponentException ("Error : there are more than one form with active status belonging to [" + idUser + "]");
		}
		if ( eaeForms.size() == 0 ) {
			return null;
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findActiveUserFormByOwner" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeForms.get(0);
	}
	
	/** UPDATE **/

	@Override
	public void updateDtClosedUserForm(String idForm, String idUser, Date date) throws EaeFormComponentException {
		Query query 					= new Query();
		Criteria criteria				= new Criteria();
		Update update					= new Update();
		// find form
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		update.set("dtClosed", date);
		try {
			// TODO - EaeUserFormBean instead EaeFormStructBean
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateDtClosedUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateDtClosedUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated dtClosed");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating dtClosed form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating dtClosed form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateUserRoleUserForm(String idForm, String idUser, String userRole) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		update.set("userRole", userRole);
		try {
			// TODO - EaeUserFormBean instead EaeFormStructBean
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateUserRoleUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateUserRoleUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated userRole");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating userRole :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating userRole of form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateDtHistorizedUserForm(String idForm, String idUser, Date date) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		update.set("dtHistorized", date);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateDtHistorizedUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateDtHistorizedUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated dtHistorized");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating dtHistorized form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating dtHistorized form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateIdManagerUserForm(String idForm, String idUser, String idManager) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		update.set("idManager", idManager);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateIdManagerUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateIdManagerUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated idManager");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating idManager form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating idManager form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateDtOwnerValidatedUserForm(String idForm, String idUser, Date date) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		update.set("dtOwnerValidated", date);
		try {
			// TODO - EaeUserFormBean instead EaeFormStructBean
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateDtOwnerValidatedUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateDtOwnerValidatedUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated dtOwnerValidated");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating dtOwnerValidated form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating dtOwnerValidated form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateDtManagerValidatedUserForm(String idForm, String idUser, Date date) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		update.set("dtManagerValidated", date);
		try {
			// TODO - EaeUserFormBean instead EaeFormStructBean
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateDtManagerValidatedUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateDtManagerValidatedUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated dtManagerValidated");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating dtManagerValidated form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating dtManagerValidated form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateDtOwnerSignedUserForm(String idForm, String idUser, Date date) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		update.set("dtOwnerSigned", date);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateDtOwnerSignedUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateDtOwnerSignedUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated dtOwnerSigned");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating dtOwnerSigned form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating dtOwnerSigned form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateDtManagerSignedUserForm(String idForm, String idUser, Date date) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		update.set("dtManagerSigned", date);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateDtManagerSignedUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateDtManagerSignedUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated dtManagerSigned");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating dtManagerSigned form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating dtManagerSigned form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateStatusUserForm(String idForm, String idUser, String status) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		update.set("status", status);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateStatusUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateStatusUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated status");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating status form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating status form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateCompletionUserForm(String idForm, String idUser, Float completion) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		Criteria criteria				= new Criteria();
		// find form
		criteria.andOperator(
				Criteria.where("idForm").is(idForm),
				Criteria.where("idUser").is(idUser)
				);
		query.addCriteria(criteria);
		update.set("completion", completion);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeUserFormBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateCompletionUserForm" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateCompletionUserForm" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching form with id [" + idForm + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated completion");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating completion form :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating completion form : [" + idForm + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateDataUserForm(
			String id,
			String idInterview,
			String idForm,
			String idUser,
			String userRole,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String idManager,
			Date dtOwnerValidated,
			Date dtManagerValidated,
			Date dtOwnerSigned,
			Date dtManagerSigned,
			String status,
			Float completion) throws EaeFormComponentException {
		// TODO Auto-generated method stub

	}

}
