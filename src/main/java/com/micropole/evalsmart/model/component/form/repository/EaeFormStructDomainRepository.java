package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.micropole.evalsmart.model.component.form.bean.EaeFormStructDomainBean;

public interface EaeFormStructDomainRepository extends MongoRepository<EaeFormStructDomainBean, String> {
	public List<EaeFormStructDomainBean> findByIdForm (String idForm);
}
