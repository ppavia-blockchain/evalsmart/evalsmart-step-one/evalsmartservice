package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructDomainBean;

public interface EaeCustomFormStructDomainRepository {
	
	/** RETRIEVE DATA DOMAIN **/
	
	public List<EaeFormStructDomainBean> findAllDomainsFromForm (String idForm) throws EaeFormComponentException;
	
	
	
	/** UPDATE DATA DOMAIN **/
	
	public void updateLabel (String idDomain, String label) throws EaeFormComponentException;
}
