package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.micropole.evalsmart.model.component.form.bean.EaeFormStructBean;

public interface EaeFormStructRepository extends MongoRepository<EaeFormStructBean, String> {
	public EaeFormStructBean findByIdForm (String idForm);
	
	public List<EaeFormStructBean> findByStatus (String status);
}