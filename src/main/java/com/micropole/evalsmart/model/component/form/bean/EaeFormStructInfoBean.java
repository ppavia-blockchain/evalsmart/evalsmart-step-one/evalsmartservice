package com.micropole.evalsmart.model.component.form.bean;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "eaeFormStructInfo")
public class EaeFormStructInfoBean implements Serializable {
	
	/**
	 * 
	 */
	@Transient
	private static final long serialVersionUID = 1798907168244746217L;
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructInfoBean.class);
	@Transient
	private static String nameClass	= EaeFormStructInfoBean.class.getSimpleName();
	
	private String id;
	private String idInfo;
	private String idDomain;
	private String idForm;
	private String label;
	private String content;
	private int order;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdInfo() {
		return idInfo;
	}
	public void setIdInfo(String idInfo) {
		this.idInfo = idInfo;
	}
	public String getIdDomain() {
		return idDomain;
	}
	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}
	public String getIdForm() {
		return idForm;
	}
	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	
	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "id" + "=" + "%s" + "\n"
				+ "idInfo" + "=" + "%s" + "\n"
				+ "idDomain" + "=" + "%s" + "\n"
				+ "idForm" + "=" + "%s" + "\n"
				+ "label" + "=" + "%s" + "\n"
				+ "content" + "=" + "%s" + "\n"
				+ "order" + "=" + "%d" + "\n",
				this.id,
				this.idInfo,
				this.idDomain,
				this.idForm,
		        this.label,
		        this.content,
		        this.order
				);
	}
}
