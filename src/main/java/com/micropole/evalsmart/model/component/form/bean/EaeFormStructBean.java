package com.micropole.evalsmart.model.component.form.bean;

import java.io.Serializable;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author PPAVIA
 *
 */
@Document(collection = "eaeFormStruct")
public class EaeFormStructBean implements Serializable {
	/**
	 * 
	 */
	@Transient
	private static final long serialVersionUID = -921894353942946954L;
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructBean.class);
	@Transient
	private static String nameClass	= EaeFormStructBean.class.getSimpleName();
	@Id
	private String id;
	private String idForm;
	private String label;
	private Date dtCreated;
	private Date dtClosed;
	private Date dtHistorized;
	private String status;
	
	public EaeFormStructBean () {}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Date getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(Date dtCreated) {
		this.dtCreated = dtCreated;
	}

	public Date getDtClosed() {
		return dtClosed;
	}

	public void setDtClosed(Date dtClosed) {
		this.dtClosed = dtClosed;
	}

	public Date getDtHistorized() {
		return dtHistorized;
	}

	public void setDtHistorized(Date dtHistorized) {
		this.dtHistorized = dtHistorized;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "id" + "=" + "%s" + "\n"
				+ "idForm" + "=" + "%s" + "\n"
				+ "label" + "=" + "%s" + "\n"
				+ "dtCreated" + "=" + "%s" + "\n"
				+ "dtClosed" + "=" + "%s" + "\n"
				+ "dtHistorized" + "=" + "%s" + "\n"
				+ "status" + "=" + "%s" + "\n",
				this.id,
				this.idForm,
				this.label,
				this.dtCreated,
				this.dtClosed,
				this.dtHistorized,
				this.status
				);
	}
}
