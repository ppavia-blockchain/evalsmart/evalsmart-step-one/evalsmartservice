package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.micropole.evalsmart.model.component.form.bean.EaeUserBean;

public interface EaeFormUserRepository extends MongoRepository<EaeUserBean, String> {
	public EaeUserBean findByIdUser (String idUser);
	public EaeUserBean findByEmail (String email);
	public EaeUserBean findByUserName (String userName);
	public List<EaeUserBean> findByFullName (String fullName);
}
