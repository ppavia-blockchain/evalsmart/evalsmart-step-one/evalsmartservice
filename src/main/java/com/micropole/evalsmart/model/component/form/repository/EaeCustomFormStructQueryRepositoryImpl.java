package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.micropole.evalsmart.exception.EaeFormComponentException;
import com.micropole.evalsmart.model.component.form.bean.EaeFormStructQueryBean;
import com.mongodb.client.result.UpdateResult;

public class EaeCustomFormStructQueryRepositoryImpl implements EaeCustomFormStructQueryRepository {
	private static final Logger logger = LoggerFactory.getLogger(EaeCustomFormStructQueryRepositoryImpl.class);
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<EaeFormStructQueryBean> findAllQueriesFromForm(String idForm) throws EaeFormComponentException {
		Query query 								= new Query();
		List<EaeFormStructQueryBean> eaeQueries			= null;
		query.with(new Sort(Sort.Direction.ASC, "order"));
		query.addCriteria(Criteria.where("idForm").is(idForm));
		try {
			eaeQueries	= mongoTemplate.find(query, EaeFormStructQueryBean.class);			
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding queries from form : [" + idForm + "]\n", e);
		}
		
		if ( eaeQueries.size() == 0 ) {
			throw new EaeFormComponentException ("Error : No queries was founded for the form : [" + idForm + "]\n");
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findAllQueriesFromForm" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeQueries;
	}

	@Override
	public List<EaeFormStructQueryBean> findAllQueriesFromDomain(String idDomain) throws EaeFormComponentException {
		Query query 								= new Query();
		List<EaeFormStructQueryBean> eaeQueries			= null;
		query.with(new Sort(Sort.Direction.ASC, "order"));
		query.addCriteria(Criteria.where("idDomain").is(idDomain));
		try {
			eaeQueries	= mongoTemplate.find(query, EaeFormStructQueryBean.class);			
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured finding queries from domain : [" + idDomain + "]\n", e);
		}
		
		if ( eaeQueries.size() == 0 ) {
			throw new EaeFormComponentException ("Error : No queries was founded for the domain : [" + idDomain + "]\n");
		}
		
		if ( logger.isDebugEnabled() ) {
			logger.debug("[" + "findAllQueriesFromForm" + "]" + "query" + " : " + query.toString());
		}
		
		return eaeQueries;
	}

	@Override
	public void updateLabel(String idQuery, String label) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idQuery").is(idQuery));
		update.set("label", label);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructQueryBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateLabel" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching query with id [" + idQuery + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated label");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating label for the query :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating label for the query : [" + idQuery + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateQueryType(String idQuery, String queryType) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idQuery").is(idQuery));
		update.set("queryType", queryType);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructQueryBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateQueryType" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateQueryType" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching query with id [" + idQuery + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated queryType");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating label for the query :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating label for the query : [" + idQuery + "]\n" + e.getMessage(), e);
		}
	}

	@Override
	public void updateIsCompleted(String idQuery, boolean isCompleted) throws EaeFormComponentException {
		Query query 					= new Query();
		Update update					= new Update();
		// find form
		query.addCriteria(Criteria.where("idQuery").is(idQuery));
		update.set("isCompleted", isCompleted);
		try {
			UpdateResult ur	= mongoTemplate.updateFirst(query, update, EaeFormStructQueryBean.class);
			if ( logger.isDebugEnabled() ) {
				logger.debug("[" + "updateIsCompleted" + "]" + "UpdateResult getModifiedCount" + " : " + ur.getModifiedCount());
				logger.debug("[" + "updateIsCompleted" + "]" + "UpdateResult getMatchedCount" + " : " + ur.getMatchedCount());
			}
			if ( ur.getMatchedCount() == 0 ) {
				throw new EaeFormComponentException ("No matching query with id [" + idQuery + "]");
			}
			if ( ur.getModifiedCount() == 0 ) {
				throw new EaeFormComponentException ("No updated isCompleted");
			}
		}
		catch ( EaeFormComponentException e ) {
			throw new EaeFormComponentException ("An error occured updating isCompleted for the query :\n" + e.getMessage(), e);
		}
		catch ( Exception e ) {
			throw new EaeFormComponentException ("An error occured updating isCompleted for the query : [" + idQuery + "]\n" + e.getMessage(), e);
		}
	}

}
