package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.micropole.evalsmart.model.component.form.bean.EaeUserFormBean;

public interface EaeUserFormRepository extends MongoRepository<EaeUserFormBean, String> {
	public EaeUserFormBean findByIdForm (String idForm);
	
	public List<EaeUserFormBean> findByStatus (String status);
}