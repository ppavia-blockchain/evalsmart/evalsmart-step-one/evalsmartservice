package com.micropole.evalsmart.model.component.form.repository;

import com.micropole.evalsmart.exception.EaeFormComponentException;

public interface EaeCustomFormStructInfoRepository {
	
	public void updateLabel (String idInfo, String label) throws EaeFormComponentException;
	
	public void updateContent (String idInfo, String content) throws EaeFormComponentException;
	
	public void updateOrder (String idInfo, int order) throws EaeFormComponentException;

}
