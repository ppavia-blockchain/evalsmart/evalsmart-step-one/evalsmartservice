package com.micropole.evalsmart.model.component.form.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.micropole.evalsmart.model.component.form.bean.EaeFormStructInfoBean;

public interface EaeFormStructInfoRepository extends MongoRepository<EaeFormStructInfoBean, String> {
	public List<EaeFormStructInfoBean> findByIdForm (String idForm);
	public List<EaeFormStructInfoBean> findByIdDomain (String idDomain);
	public EaeFormStructInfoBean findByIdInfo (String idInfo);
	
}