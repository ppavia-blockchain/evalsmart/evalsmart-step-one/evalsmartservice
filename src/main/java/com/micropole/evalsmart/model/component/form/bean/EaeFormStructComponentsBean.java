package com.micropole.evalsmart.model.component.form.bean;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Transient;

/**
 * This class represent the data structure in db for a specific form
 * @author PPAVIA
 *
 */
public class EaeFormStructComponentsBean {
	@Transient
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructComponentsBean.class);
	@Transient
	private static String nameClass	= EaeFormStructComponentsBean.class.getSimpleName();
	private EaeFormStructBean eaeFormStruct						= null;
	private List<EaeFormStructDomainBean> eaeDomains			= new ArrayList<EaeFormStructDomainBean>();
	private List<EaeFormStructInfoBean> eaeInfos				= new ArrayList<EaeFormStructInfoBean>();
	private List<EaeFormStructQueryBean> eaeQueries				= new ArrayList<EaeFormStructQueryBean>();
	private List<EaeFormStructResponseBean> eaeResponses		= new ArrayList<EaeFormStructResponseBean>();

	public EaeFormStructComponentsBean () {}
	
	public EaeFormStructComponentsBean (
			EaeFormStructBean eaeForm,
			List<EaeFormStructDomainBean> eaeFormDomains,
			List<EaeFormStructInfoBean> eaeInfos,
			List<EaeFormStructQueryBean> eaeFormQueries,
			List<EaeFormStructResponseBean> eaeFormResponses
			) {
		this.eaeFormStruct	= eaeForm;
		this.eaeDomains		= eaeFormDomains;
		this.eaeInfos		= eaeInfos;
		this.eaeQueries		= eaeFormQueries;
		this.eaeResponses	= eaeFormResponses;
	}

	
	public EaeFormStructBean getEaeFormStruct() {
		return eaeFormStruct;
	}

	public void setEaeFormStruct(EaeFormStructBean eaeFormStruct) {
		this.eaeFormStruct = eaeFormStruct;
	}

	public List<EaeFormStructDomainBean> getEaeDomains() {
		return eaeDomains;
	}

	public void setEaeDomains(List<EaeFormStructDomainBean> eaeDomains) {
		this.eaeDomains = eaeDomains;
	}

	public List<EaeFormStructInfoBean> getEaeInfos() {
		return eaeInfos;
	}

	public void setEaeInfos(List<EaeFormStructInfoBean> eaeInfos) {
		this.eaeInfos = eaeInfos;
	}

	public List<EaeFormStructQueryBean> getEaeQueries() {
		return eaeQueries;
	}

	public void setEaeQueries(List<EaeFormStructQueryBean> eaeQueries) {
		this.eaeQueries = eaeQueries;
	}

	public List<EaeFormStructResponseBean> getEaeResponses() {
		return eaeResponses;
	}

	public void setEaeResponses(List<EaeFormStructResponseBean> eaeResponses) {
		this.eaeResponses = eaeResponses;
	}

	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "eaeFormStruct" + "=" + "%s" + "\n"
				+ "eaeDomains" + "=" + "%s" + "\n"
				+ "eaeInfos" + "=" + "%s" + "\n"
				+ "eaeQueries" + "=" + "%s" + "\n"
				+ "eaeResponses" + "=" + "%s" + "\n",
				this.eaeFormStruct.toString(),
				this.eaeDomains.toString(),
				this.eaeInfos.toString(),
				this.eaeQueries.toString(),
				this.eaeResponses.toString()
				);
	}

}
