package com.micropole.evalsmart.model;

import java.util.ArrayList;
import java.util.List;

public enum EaeComponentTypeIdEnum {
	USER("user"), FORM("form"), DOMAIN("domain"), QUERY("query"), RESPONSE("response");
	private String name;
	private static final List<String> values	= new ArrayList<String>();
	
	static {
		for ( EaeComponentTypeIdEnum componentType : EaeComponentTypeIdEnum.values() ) {
			values.add(componentType.getName());
		}
	}

	private EaeComponentTypeIdEnum(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public static List<String> getValues () {		
		return values;
	}
}
