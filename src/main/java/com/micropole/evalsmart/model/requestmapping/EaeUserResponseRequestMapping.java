package com.micropole.evalsmart.model.requestmapping;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;

public class EaeUserResponseRequestMapping {
	private static final String nameClass			= EaeUserResponseRequestMapping.class.getSimpleName();
	private static final Logger logger = LoggerFactory.getLogger(EaeUserResponseRequestMapping.class);

	/*
	 * idForm and idUser identify the user form
	 * idOwner tell us who owns the answer
	 */
	@Id
	private String id;
	private String idForm;
	private String idUser;
	private String idDomain;
	private String idQuery;
	private String idResponse;
	private String idOwner;
	private String content;
	private Date dtCompletion;
	
	public EaeUserResponseRequestMapping () {
		super();
	}
	
	public EaeUserResponseRequestMapping (
			String id,
			String idForm,
			String idUser,
			String idDomain,
			String idQuery,
			String idResponse,
			String idOwner,
			String content,
			Date dtCompletion
			) {
		this.id						= id;
		this.idForm					= idForm;
		this.idUser					= idUser;
		this.idDomain				= idDomain;
		this.idQuery				= idQuery;
		this.idResponse				= idResponse;
		this.idOwner				= idOwner;
		this.content				= content;
		this.dtCompletion			= dtCompletion;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getIdDomain() {
		return idDomain;
	}

	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}

	public String getIdQuery() {
		return idQuery;
	}

	public void setIdQuery(String idQuery) {
		this.idQuery = idQuery;
	}

	public String getIdResponse() {
		return idResponse;
	}

	public void setIdResponse(String idResponse) {
		this.idResponse = idResponse;
	}

	public String getIdOwner() {
		return idOwner;
	}

	public void setIdOwner(String idOwner) {
		this.idOwner = idOwner;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDtCompletion() {
		return dtCompletion;
	}

	public void setDtCompletion(Date dtCompletion) {
		this.dtCompletion = dtCompletion;
	}

	@Override
	public String toString() {
		return String.format(
			nameClass + "\n"
			+ "id" + "=" + "%s" + "\n"
			+ "idForm" + "=" + "%s" + "\n"
			+ "idUser" + "=" + "%s" + "\n"
			+ "idDomain" + "=" + "%s" + "\n"
			+ "idQuery" + "=" + "%s" + "\n"
			+ "idResponse" + "=" + "%s" + "\n"
			+ "idOwner" + "=" + "%s" + "\n"
			+ "content" + "=" + "%s" + "\n"
			+ "dtCompletion" + "=" + "%s" + "\n",
			this.id,
			this.idForm,
			this.idUser,
			this.idDomain,
			this.idQuery,
			this.idResponse,
			this.idOwner,
			this.content,
			this.dtCompletion
		);
	}
}
