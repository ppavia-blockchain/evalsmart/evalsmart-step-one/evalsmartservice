package com.micropole.evalsmart.model.requestmapping;

import com.micropole.evalsmart.model.component.form.bean.EaeFormStructBean;

public class EaeUserRequestMapping {
	private static final String nameClass			= EaeFormStructBean.class.getSimpleName();
	private String id;
	private String idUser;
	private String profile;
	private String status;
	private String firstName;
	private String lastName;
	private String userName;
	private String email;
	private String phone;
	private String[] account;
	
	public EaeUserRequestMapping () {};
	
	public EaeUserRequestMapping (
			String id,
			String idUser,
			String profile,
			String status,
			String firstName,
			String lastName,
			String userName,
			String email, 
			String phone, 
			String[] account
	) {
		this.id			= id;
		this.idUser		= idUser;
		this.profile	= profile;
		this.status		= status;
		this.firstName	= firstName;
		this.lastName	= lastName;
		this.userName	= userName;
		this.email		= email;
		this.phone		= phone;
		this.account	= account;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String[] getAccount() {
		return account;
	}

	public void setAccount(String[] account) {
		this.account = account;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "id" + "=" + "%s" + "\n"
				+ "idUser" + "=" + "%s" + "\n"
				+ "profile" + "=" + "%s" + "\n"
				+ "status" + "=" + "%s" + "\n"
				+ "firstName" + "=" + "%s" + "\n"
				+ "lastName" + "=" + "%s" + "\n"
				+ "userName" + "=" + "%s" + "\n"
				+ "email" + "=" + "%s" + "\n"
				+ "phone" + "=" + "%s" + "\n"
				+ "account" + "=" + "%s" + "\n",
				this.id,
				this.idUser,
				this.profile,
				this.status,
				this.firstName,
				this.lastName,
				this.userName,
				this.email,
				this.phone,
				this.account
				);
	}
}
