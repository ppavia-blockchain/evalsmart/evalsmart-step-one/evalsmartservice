package com.micropole.evalsmart.model.requestmapping;

public class EaeFormStructResponseLevelRequestMapping {
	private String idDomain;
	private String label;
	private String code;
	
	public String getIdDomain() {
		return idDomain;
	}
	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
