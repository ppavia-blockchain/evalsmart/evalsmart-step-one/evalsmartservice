package com.micropole.evalsmart.model.requestmapping;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EaeFormStructRequestMapping {
	private static final String nameClass			= EaeFormStructRequestMapping.class.getSimpleName();
	private static final Logger logger = LoggerFactory.getLogger(EaeFormStructRequestMapping.class);

	private String idForm;
	private String label;
	private Date dtCreated;
	private Date dtClosed;
	private Date dtHistorized;
	private String status;
	private EaeFormStructDomainRequestMapping[] eaeFormDomains;
	
	public EaeFormStructRequestMapping () {
		super();
	}
	
	public EaeFormStructRequestMapping (
			String idForm,
			String label,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String status,
			EaeFormStructDomainRequestMapping[] eaeFormDomains
			) {
		this.idForm					= idForm;
		this.label					= label;
		this.dtCreated				= dtCreated;
		this.dtClosed				= dtClosed;
		this.dtHistorized			= dtHistorized;
		this.status     			= status;
		this.eaeFormDomains     	= eaeFormDomains;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Date getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(Date dtCreated) {
		this.dtCreated = dtCreated;
	}

	public Date getDtClosed() {
		return dtClosed;
	}

	public void setDtClosed(Date dtClosed) {
		this.dtClosed = dtClosed;
	}

	public Date getDtHistorized() {
		return dtHistorized;
	}

	public void setDtHistorized(Date dtHistorized) {
		this.dtHistorized = dtHistorized;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public EaeFormStructDomainRequestMapping[] getEaeFormDomains() {
		return eaeFormDomains;
	}

	public void setEaeFormDomains(EaeFormStructDomainRequestMapping[] eaeFormDomains) {
		this.eaeFormDomains = eaeFormDomains;
	}

	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
						+ "idForm" + "=" + "%s" + "\n"
						+ "label" + "=" + "%s" + "\n"
						+ "dtCreated" + "=" + "%s" + "\n"
						+ "dtClosed" + "=" + "%s" + "\n"
						+ "dtHistorized" + "=" + "%s" + "\n"
						+ "status" + "=" + "%s" + "\n"
						+ "eaeFormDomains" + "=" + "%s" + "\n",
						this.idForm,
						this.label,
						this.dtCreated,
						this.dtClosed,
						this.dtHistorized,
						this.status,
						this.eaeFormDomains.toString()
				);
	}
}
