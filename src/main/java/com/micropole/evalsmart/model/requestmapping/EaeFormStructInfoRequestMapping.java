package com.micropole.evalsmart.model.requestmapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EaeFormStructInfoRequestMapping {
	private static final String nameClass			= EaeFormStructQueryRequestMapping.class.getSimpleName();
	private static final Logger logger 				= LoggerFactory.getLogger(EaeFormStructInfoRequestMapping.class);
	
	private String id;
	private String idInfo;
	private String idDomain;
	private String idForm;
	private String label;
	private String content;
	private int order;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdInfo() {
		return idInfo;
	}
	public void setIdInfo(String idInfo) {
		this.idInfo = idInfo;
	}
	public String getIdDomain() {
		return idDomain;
	}
	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}
	public String getIdForm() {
		return idForm;
	}
	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
				+ "id" + "=" + "%s" + "\n"
				+ "idInfo" + "=" + "%s" + "\n"
				+ "idDomain" + "=" + "%s" + "\n"
				+ "idForm" + "=" + "%s" + "\n"
				+ "label" + "=" + "%s" + "\n"
				+ "content" + "=" + "%s" + "\n"
				+ "order" + "=" + "%d" + "\n",
				this.id,
				this.idInfo,
				this.idDomain,
				this.idForm,
		        this.label,
		        this.content,
		        this.order
				);
	}
}
