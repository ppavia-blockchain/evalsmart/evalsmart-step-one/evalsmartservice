package com.micropole.evalsmart.model.requestmapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.micropole.evalsmart.model.component.form.bean.EaeFormStructResponseLevelBean;

public class EaeFormStructDomainRequestMapping {
	private static final String nameClass			= EaeFormStructDomainRequestMapping.class.getSimpleName();
	private static final Logger logger 				= LoggerFactory.getLogger(EaeFormStructDomainRequestMapping.class);
	private String idForm;
	private String idDomain;
	private String label;
	private int order;
	private EaeFormStructResponseLevelBean[] levels;
	private EaeFormStructQueryRequestMapping[] eaeFormQueries;
	private EaeFormStructInfoRequestMapping[] eaeFormInfos;
	
	public EaeFormStructDomainRequestMapping () {
		super();
	}
	
	public EaeFormStructDomainRequestMapping (
			String idForm,
			String idDomain,
			String label,
			int order,
			EaeFormStructResponseLevelBean[] levels,
			EaeFormStructQueryRequestMapping[] eaeFormQueries
			) {
		this.idForm			= idForm;
		this.idDomain		= idDomain;
		this.label			= label;
		this.order			= order;
		this.levels			= levels;
		this.eaeFormQueries	= eaeFormQueries;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public String getIdDomain() {
		return idDomain;
	}

	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public EaeFormStructQueryRequestMapping[] getEaeFormQueries() {
		return eaeFormQueries;
	}

	public void setEaeFormQueries(EaeFormStructQueryRequestMapping[] eaeFormQueries) {
		this.eaeFormQueries = eaeFormQueries;
	}

	public EaeFormStructResponseLevelBean[] getLevels() {
		return levels;
	}

	public void setLevels(EaeFormStructResponseLevelBean[] levels) {
		this.levels = levels;
	}
	
	public EaeFormStructInfoRequestMapping[] getEaeFormInfos() {
		return eaeFormInfos;
	}

	public void setEaeFormInfos(EaeFormStructInfoRequestMapping[] eaeFormInfos) {
		this.eaeFormInfos = eaeFormInfos;
	}

	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
						+ "idForm" + "=" + "%s" + "\n"
						+ "idDomain" + "=" + "%s" + "\n"
						+ "label" + "=" + "%s" + "\n"
						+ "order" + "=" + "%d" + "\n"
						+ "levels" + "=" + "%s" + "\n"
						+ "eaeFormQueries" + "=" + "%s" + "\n"
						+ "eaeFormInfos" + "=" + "%s" + "\n",
						this.idForm,
						this.idDomain,
						this.label,
						this.order,
						this.levels.toString(),
						this.eaeFormQueries.toString(),
						this.eaeFormInfos.toString()
				);
	}
}
