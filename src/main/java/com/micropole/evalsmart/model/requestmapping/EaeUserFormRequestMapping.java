package com.micropole.evalsmart.model.requestmapping;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;

public class EaeUserFormRequestMapping {
	private static final String nameClass			= EaeUserFormRequestMapping.class.getSimpleName();
	private static final Logger logger = LoggerFactory.getLogger(EaeUserFormRequestMapping.class);
	
	@Id
	private String id;
	private String idInterview;
	private String idForm;
	private String idUser;
	private String userRole;
	private Date dtCreated;
	private Date dtClosed;
	private Date dtHistorized;
	private String idManager;
	private Date dtOwnerValidated;
	private Date dtManagerValidated;
	private Date dtOwnerSigned;
	private Date dtManagerSigned;
	private String status;
	private Float completion;
	private Date dtSigned;
	private Date dtValidated;
	
	private EaeUserResponseRequestMapping[] eaeUserFormResponses;
	
	public EaeUserFormRequestMapping () {
		super();
	}
	
	public EaeUserFormRequestMapping (
			String id,
			String idInterview,
			String idForm,
			String idUser,
			String userRole,
			Date dtCreated,
			Date dtClosed,
			Date dtHistorized,
			String idManager,
			Date dtOwnerValidated,
			Date dtManagerValidated,
			Date dtOwnerSigned,
			Date dtManagerSigned,
			String status,
			Float completion,
			Date dtSigned,
			Date dtValidated,
			EaeUserResponseRequestMapping[] eaeUserFormResponses
			) {
		this.id						= id;
		this.idInterview			= idInterview;
		this.idForm					= idForm;
		this.idUser					= idUser;
		this.userRole				= userRole;
		this.dtCreated				= dtCreated;
		this.dtClosed				= dtClosed;
		this.dtHistorized			= dtHistorized;
		this.idManager				= idManager;
		this.dtOwnerValidated		= dtOwnerValidated;
		this.dtManagerValidated		= dtManagerValidated;
		this.dtOwnerSigned			= dtOwnerSigned;
		this.dtManagerSigned		= dtManagerSigned;
		this.status     			= status;
		this.completion     		= completion;
		this.dtSigned     			= dtSigned;
		this.dtValidated     		= dtValidated;
		this.eaeUserFormResponses     	= eaeUserFormResponses;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdInterview() {
		return idInterview;
	}

	public void setIdInterview(String idInterview) {
		this.idInterview = idInterview;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public Date getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(Date dtCreated) {
		this.dtCreated = dtCreated;
	}

	public Date getDtClosed() {
		return dtClosed;
	}

	public void setDtClosed(Date dtClosed) {
		this.dtClosed = dtClosed;
	}

	public Date getDtHistorized() {
		return dtHistorized;
	}

	public void setDtHistorized(Date dtHistorized) {
		this.dtHistorized = dtHistorized;
	}

	public String getIdManager() {
		return idManager;
	}

	public void setIdManager(String idManager) {
		this.idManager = idManager;
	}

	public Date getDtOwnerValidated() {
		return dtOwnerValidated;
	}

	public void setDtOwnerValidated(Date dtOwnerValidated) {
		this.dtOwnerValidated = dtOwnerValidated;
	}

	public Date getDtManagerValidated() {
		return dtManagerValidated;
	}

	public void setDtManagerValidated(Date dtManagerValidated) {
		this.dtManagerValidated = dtManagerValidated;
	}

	public Date getDtOwnerSigned() {
		return dtOwnerSigned;
	}

	public void setDtOwnerSigned(Date dtOwnerSigned) {
		this.dtOwnerSigned = dtOwnerSigned;
	}

	public Date getDtManagerSigned() {
		return dtManagerSigned;
	}

	public void setDtManagerSigned(Date dtManagerSigned) {
		this.dtManagerSigned = dtManagerSigned;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Float getCompletion() {
		return completion;
	}

	public void setCompletion(Float completion) {
		this.completion = completion;
	}

	public Date getDtSigned() {
		return dtSigned;
	}

	public void setDtSigned(Date dtSigned) {
		this.dtSigned = dtSigned;
	}

	public Date getDtValidated() {
		return dtValidated;
	}

	public void setDtValidated(Date dtValidated) {
		this.dtValidated = dtValidated;
	}

	public EaeUserResponseRequestMapping[] getEaeUserFormResponses() {
		return eaeUserFormResponses;
	}

	public void setEaeUserFormResponses(EaeUserResponseRequestMapping[] eaeUserFormResponses) {
		this.eaeUserFormResponses = eaeUserFormResponses;
	}

	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
					+ "id" + "=" + "%s" + "\n"
					+ "idInterview" + "=" + "%s" + "\n"
					+ "idForm" + "=" + "%s" + "\n"
					+ "idUser" + "=" + "%s" + "\n"
					+ "userRole" + "=" + "%s" + "\n"
					+ "dtCreated" + "=" + "%s" + "\n"
					+ "dtClosed" + "=" + "%s" + "\n"
					+ "dtHistorized" + "=" + "%s" + "\n"
					+ "idManager" + "=" + "%s" + "\n"
					+ "dtOwnerValidated" + "=" + "%s" + "\n"
					+ "dtManagerValidated" + "=" + "%s" + "\n"
					+ "dtOwnerSigned" + "=" + "%s" + "\n"
					+ "dtManagerSigned" + "=" + "%s" + "\n"
					+ "status" + "=" + "%s" + "\n"
					+ "completion" + "=" + "%d" + "\n"
					+ "dtSigned" + "=" + "%s" + "\n"
					+ "dtValidated" + "=" + "%s" + "\n"
					+ "eaeUserFormResponses" + "=" + "%s" + "\n",
					this.id,
					this.idInterview,
					this.idForm,
					this.idUser,
					this.userRole,
					this.dtCreated,
					this.dtClosed,
					this.dtHistorized,
					this.idManager,
					this.dtOwnerValidated,
					this.dtManagerValidated,
					this.dtOwnerSigned,
					this.dtManagerSigned,
					this.status,
					this.completion,
					this.dtSigned,
					this.dtValidated,
					this.eaeUserFormResponses.toString()
				);
	}
}
