package com.micropole.evalsmart.model.requestmapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EaeFormStructQueryRequestMapping {
	private static final String nameClass			= EaeFormStructQueryRequestMapping.class.getSimpleName();
	private static final Logger logger 				= LoggerFactory.getLogger(EaeFormStructQueryRequestMapping.class);

	private String idQuery;
	private String idDomain;
	private String idForm;
	private String label;
	private String idOwner;
	private String queryType;
	private int order;
	private EaeFormStructResponseRequestMapping[] eaeFormResponses;
	
	public EaeFormStructQueryRequestMapping () {
		super();
	}

	public EaeFormStructQueryRequestMapping (
			String idQuery,
			String idDomain,
			String idForm,
			String label,
			String idOwner,
			String queryType,
			int order,
			EaeFormStructResponseRequestMapping[] eaeFormResponses) {
		this.idQuery				= idQuery;
		this.idDomain				= idDomain;
		this.idForm					= idForm;
		this.label					= label;
		this.idOwner				= idOwner;
		this.queryType				= queryType;
		this.order					= order;
		this.eaeFormResponses		= eaeFormResponses;
	}

	public String getIdQuery() {
		return idQuery;
	}

	public void setIdQuery(String idQuery) {
		this.idQuery = idQuery;
	}

	public String getIdDomain() {
		return idDomain;
	}

	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getIdOwner() {
		return idOwner;
	}

	public void setIdOwner(String idOwner) {
		this.idOwner = idOwner;
	}

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public EaeFormStructResponseRequestMapping[] getEaeFormResponses() {
		return eaeFormResponses;
	}

	public void setEaeFormResponses(EaeFormStructResponseRequestMapping[] eaeFormResponses) {
		this.eaeFormResponses = eaeFormResponses;
	}
}
