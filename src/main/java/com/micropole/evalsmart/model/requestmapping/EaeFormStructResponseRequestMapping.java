package com.micropole.evalsmart.model.requestmapping;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EaeFormStructResponseRequestMapping {
	private static final Logger logger	= LoggerFactory.getLogger(EaeFormStructResponseRequestMapping.class);
	private static String nameClass		= EaeFormStructResponseRequestMapping.class.getSimpleName();
	private String idResponse;
	private String idQuery;
	private String idDomain;
	private String idForm;
	private String label;
	private String responseType;
	private String level;
	private String content;
	
	public EaeFormStructResponseRequestMapping () {
		super();
	}
	
	public EaeFormStructResponseRequestMapping (
			String idResponse,
			String idQuery,
			String idDomain,
			String idForm,
			String label,
			String responseType,
			String level,
			String content
			) {
		this.idResponse			= idResponse;
        this.idQuery			= idQuery;
        this.idDomain			= idDomain;
        this.idForm				= idForm;
        this.label				= label;
        this.responseType		= responseType;
        this.level				= level;
        this.content			= content;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getIdResponse() {
		return idResponse;
	}

	public void setIdResponse(String idResponse) {
		this.idResponse = idResponse;
	}

	public String getIdQuery() {
		return idQuery;
	}

	public void setIdQuery(String idQuery) {
		this.idQuery = idQuery;
	}

	public String getIdDomain() {
		return idDomain;
	}

	public void setIdDomain(String idDomain) {
		this.idDomain = idDomain;
	}

	public String getIdForm() {
		return idForm;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public String getResponseType() {
		return responseType;
	}

	public void setResponseType(String responseType) {
		this.responseType = responseType;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Override
	public String toString() {
		return String.format(
				nameClass + "\n"
						+ "idForm" + "=" + "%s" + "\n"
						+ "idDomain" + "=" + "%s" + "\n"
						+ "label" + "=" + "%s" + "\n"
						+ "order" + "=" + "%d" + "\n"
						+ "levels" + "=" + "%s" + "\n"
						+ "eaeFormQueries" + "=" + "%s" + "\n",
						this.idResponse,
				        this.idQuery,
				        this.idDomain,
				        this.idForm,
				        this.label,
				        this.responseType,
				        this.level,
				        this.content
				);
	}
}
