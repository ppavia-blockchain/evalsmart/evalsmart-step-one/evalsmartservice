package com.micropole.evalsmart.model;

import java.util.ArrayList;
import java.util.List;

public enum EaeStatusFormEnum {
	CREATED("001"), ACTIVE("010"), VALIDATED("002"), SIGNED("003"), ABORDED("004"), HISTORISIZED("005"), EXPIRED("006");
	private String statusCode;
	private static final List<String> codes	= new ArrayList<String>();
	
	static {
		for ( EaeStatusFormEnum componentType : EaeStatusFormEnum.values() ) {
			codes.add(componentType.getStatusCode());
		}
	}

	private EaeStatusFormEnum(String status) {
		this.statusCode = status;
	}
	
	public String getStatusCode() {
		return this.statusCode;
	}
	
	public static List<String> getValues () {
		return codes;
	}
	
	public static String getCode(String value) {
		switch (value.trim().toUpperCase()) {
			case  "CREATED":
				return EaeStatusFormEnum.CREATED.getStatusCode();
			case  "ACTIVE":
				return EaeStatusFormEnum.ACTIVE.getStatusCode();
			case  "VALIDATED":
				return EaeStatusFormEnum.VALIDATED.getStatusCode();
			case  "SIGNED":
				return EaeStatusFormEnum.SIGNED.getStatusCode();
			case  "ABORDED":
				return EaeStatusFormEnum.ABORDED.getStatusCode();
			case  "HISTORISIZED":
				return EaeStatusFormEnum.HISTORISIZED.getStatusCode();
			case  "EXPIRED":
				return EaeStatusFormEnum.EXPIRED.getStatusCode();
			default:
				return "";			
		}
	}
}
