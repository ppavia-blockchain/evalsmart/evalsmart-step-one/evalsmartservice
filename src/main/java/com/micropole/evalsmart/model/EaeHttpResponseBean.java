package com.micropole.evalsmart.model;

import java.util.Date;

public class EaeHttpResponseBean <T> {
	  private Date timestamp;
	  private String message;
	  private String location;
	  private String httpCodeMessage;
	  private T data;
	  // TODO - implementation - what about _link ?
	  

	  public EaeHttpResponseBean(Date timestamp, String message, String location,String httpCodeMessage) {
	    super();
	    this.timestamp = timestamp;
	    this.message = message;
	    this.location = location;
	    this.httpCodeMessage=httpCodeMessage;
	  }
	  public EaeHttpResponseBean(Date timestamp, String message, String location, String httpCodeMessage, T data) {
		    super();
		    this.timestamp 			= timestamp;
		    this.message 			= message;
		    this.location 			= location;
		    this.httpCodeMessage	= httpCodeMessage;
		    this.data				= data;
		  }

	  public String getHttpCodeMessage() {
	    return httpCodeMessage;
	  }
	  
	  public Date getTimestamp() {
	    return timestamp;
	  }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public void setHttpCodeMessage(String httpCodeMessage) {
		this.httpCodeMessage = httpCodeMessage;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
}
